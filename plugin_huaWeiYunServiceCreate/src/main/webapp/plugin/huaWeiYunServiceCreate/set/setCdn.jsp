<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="参数设置"/>
</jsp:include>

<style>
	
	.content{
		width: 600px;
		min-height:80%;
   margin: 0 auto;
   box-shadow: rgba(0, 0, 0, 0.06) 0px 1px 10px 2px;
   padding: 30px;
   margin-top: 50px;
	}
	.title{
		border-bottom: 1px solid #eee;
   padding-top: 20px;
   padding-left: 10px;
   padding-bottom: 20px;
   font-size: 28px;
   margin-bottom: 20px;
   text-align:center;
	}
	.content ul{
		padding-left: 20px;
	}
	.content ul li{
		list-style-type: decimal;
		padding-left:10px;
		padding-bottom:4px;
	}
	.content ul li img{
		max-width:250px;
		padding:4px;
		padding-left:40px;
	}
	.info{
		font-size:14px;
		line-height: 22px;
	}
	.info h2,h3,h4,h5{
	border-bottom: 1px solid #eee;
   padding-top: 23px;
   margin-bottom: 10px;
   padding-bottom: 5px;
	}
	
	@media only screen and (max-width: 700px) {
		.content{
			width:auto;
			margin-top: 0px;
			box-shadow: rgba(0, 0, 0, 0.06) 0px 0px 0px 0px;
		}
		
	}
	
	a{
		color: blue;
	}
</style>

<div class="content">
	<div class="title">
		配置图片等附件访问所用的域名
		<div style="color:gray; font-size: 10px;">（一般为配置CDN加速使用）</div>
	</div>
	
	<div class="info" style="font-size:16px;">
	
		<div style="float:left; width:500px;">
			<input type="text" id="ATTACHMENT_FILE_URL" disabled="disabled" value="<%=SystemUtil.get("ATTACHMENT_FILE_URL") %>" class="layui-input">
		</div>
		<div style="float:left">
			<button onclick="setCdn();" class="layui-btn layui-btn-primary">修改</button>
		</div>
		<div style="clear: both;"></div>
	</div>
	
	<div style="color:red; font-size: 14px;     padding-top: 3rem;">
		请注意：此您完全可以不必填写，直接跳过，使用OBS自动分配的域名，完全可正常使用！<br/>
		配置CDN域名后，可以更节省成本，提升图片等加载速度。但CDN需要您自行在华为云后台进行操作，这方面没有教程，不懂的请直接跳过<br/><br/>
		如果您要配置CDN域名，填写格式如 http://cdn.xxxx.com/ 或 //cdn.xxxx.com/  <br/>
		1. 请注意前面要带上协议如 http:// 或者 https:// 或者 //<br/>
		2. 域名后以 / 结尾，一定记的要带上 /
	</div>
	
	<div style="text-align:center; padding-top:4rem;"> 
		<a href="/install/domainSet.do" class="layui-btn" style="width:180px;">进行下一步</a>
	</div>
</div>



<script type="text/javascript">
function setCdn(){
	msg.textarea('设置图片等附件访问域名', function(v){
		
		msg.loading("更改中");	//显示“更改中”的等待提示
		post('variableSave.json', {name:'ATTACHMENT_FILE_URL', value:v}, function(data){
			msg.close();
			if(data.result == '0'){
				msg.alert(data.info);
				return;
			}
			
			msg.info('保存成功',function(){
				window.location.reload();
			});
		});
		
	}, document.getElementById('ATTACHMENT_FILE_URL').value);
}
</script>

<jsp:include page="/wm/common/foot.jsp"></jsp:include> 