package com.xnx3.wangmarket.plugin.huaWeiYunServiceCreate.controller;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.DateUtil;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.entity.System;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.SystemService;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.AttachmentUtil;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.j2ee.util.AttachmentMode.HuaweiyunOBSMode;
import com.xnx3.j2ee.vo.BaseVO;

/**
 * 华为云环境设置基本变量信息
 * @author 李鑫
 */
@Controller
@RequestMapping("/plugin/huaWeiYunServiceCreate/set/")
public class SetHuaWeiYunServiceCreatePluginController extends BasePluginController {
	
	@Resource
	private SqlService sqlService;
	@Resource
	private SystemService systemService;
	
	/**
	 * 初始化-设置accesskey
	 */
	@RequestMapping("setAccessKey${url.suffix}")
	public String setAccessKey(Model model){
		/*** 权限检查 ***/
		if(SystemUtil.get("IW_AUTO_INSTALL_USE").equalsIgnoreCase("true")) {
			//正在安装界面安装
		}else if(!haveSuperAdminAuth()) {
			// 是否有管理员权限
			return error(model, "无权使用");
		}
		
		/*
		 * 初始化数据库需要字段信息
		 */
		//检查HUAWEIYUN_ACCESSKEYID字段
		initializationSystem("HUAWEIYUN_ACCESSKEYID", "华为云平台的accessKeyId", null);
		// 检查HUAWEIYUN_ACCESSKEYSECRET字段
		initializationSystem("HUAWEIYUN_ACCESSKEYSECRET", "华为云平台的accessKeySecret", null);
		// 检查桶名称字段是否存在，默认值为 "auto"
		initializationSystem("HUAWEIYUN_OBS_BUCKETNAME", "华为云对象存储桶的名字。若值为auto，则会自动创建。建议值不必修改，默认即可。它可自动给你赋值。", "auto");
		
		
		//判断是否设置了accesskey等，如果没有设置，需要先进性设置
		if((SystemUtil.get("HUAWEIYUN_ACCESSKEYID") == null || SystemUtil.get("HUAWEIYUN_ACCESSKEYID").length() < 5) || SystemUtil.get("HUAWEIYUN_ACCESSKEYSECRET") == null || SystemUtil.get("HUAWEIYUN_ACCESSKEYSECRET").length() < 5){
			return redirect("plugin/huaWeiYunServiceCreate/set/setAccessKey.jsp"); 
		}else{
			//如果已经设置了，那么就重定向，跳转到下一个设置项
			return redirect("plugin/huaWeiYunServiceCreate/set/setArea.do");
		}
	}
	
	/**
	 * 初始化-选择区域,比如香港、上海、北京、深圳
	 */
	@RequestMapping("setArea${url.suffix}")
	public String setArea(Model model){
		/*** 权限检查 ***/
		if(SystemUtil.get("IW_AUTO_INSTALL_USE").equalsIgnoreCase("true")) {
			//正在安装界面安装
		}else if(!haveSuperAdminAuth()) {
			// 是否有管理员权限
			return error(model, "无权使用");
		}
		
		//判断是否设置了 endpoint ，如果没有设置，需要先进性设置
		if(SystemUtil.get("HUAWEIYUN_COMMON_ENDPOINT") == null || SystemUtil.get("HUAWEIYUN_COMMON_ENDPOINT").length() < 5){
			return redirect("plugin/huaWeiYunServiceCreate/set/setArea.jsp");
		}else{
			//如果已经设置了，那么就重定向，跳转到下一个设置项
			return redirect("plugin/huaWeiYunServiceCreate/index.do");
		}
	}
	
	/**
	 * 设置区域后的保存
	 */
	@RequestMapping("setAreaSave.json")
	@ResponseBody
	public BaseVO setAreaSave(
			@RequestParam(value = "area", required = false, defaultValue="") String area){
		/*** 权限检查 ***/
		if(SystemUtil.get("IW_AUTO_INSTALL_USE").equalsIgnoreCase("true")) {
			//正在安装界面安装
		}else if(!haveSuperAdminAuth()) {
			// 是否有管理员权限
			return error("无权使用");
		}
		
		area = area.trim();
		// 保存及检查地域信息
		if(area.length() == 0){
			return error("请选择区域");
		}else{
			System areaSys = sqlService.findAloneByProperty(System.class, "name", "HUAWEIYUN_COMMON_ENDPOINT");
			if(areaSys == null){
				areaSys = new System();
				areaSys.setDescription("华为云OBS的Endpoint设置。如香港，则此处的值为 ap-southeast-1；上海一，则是 cn-east-3");
				areaSys.setName("HUAWEIYUN_COMMON_ENDPOINT");
			}
			areaSys.setLasttime(DateUtil.timeForUnix10());
			areaSys.setValue(area);
			sqlService.save(areaSys);
			//刷新缓存
//			systemService.refreshSystemCache();
			
			//手动刷新缓存
			Global.system.put("HUAWEIYUN_COMMON_ENDPOINT", area);
		}
		return success();
	}
	

	/**
	 * 修改全局变量后的保存。根据 name 来进行改
	 */
	@RequestMapping(value="variableSave.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO variableSave(System sys, Model model, HttpServletRequest request){
		/*** 权限检查 ***/
		if(SystemUtil.get("IW_AUTO_INSTALL_USE").equalsIgnoreCase("true")) {
			//正在安装界面安装
		}else if(!haveSuperAdminAuth()) {
			// 是否有管理员权限
			return error("无权使用");
		}
		
		
		System system = sqlService.findAloneByProperty(System.class, "name", sys.getName());
		if(system == null){
			//新增
			system = new System();
			system.setName(sys.getName());
		}else{
			//编辑
		}
		system.setDescription(sys.getDescription());
		system.setLasttime(DateUtil.timeForUnix10());
		system.setValue(sys.getValue());
		sqlService.save(system);
		
		/***更新内存数据****/
		systemService.refreshSystemCache();
		
		if(system.getName().equalsIgnoreCase("ATTACHMENT_FILE_URL")) {
			//设置CDN域名，那么刷新域名
			Global.system.put("ATTACHMENT_FILE_URL", sys.getValue());
			HuaweiyunOBSMode.obsHandler.setUrlForCDN(sys.getValue());
			AttachmentUtil.setNetUrl(sys.getValue());
		}
		
		ActionLogUtil.insertUpdateDatabase(request, system.getId(), "保存系统变量", system.getName()+"="+system.getValue());
		return success();
	}
	
	
	/**
	 * 初始化数据库中与华为云相关的信息,如果没有该字段将会进行创建
	 * @author 李鑫
	 * @param name 需要初始化的信息的名称 例 “HUAWEIYUN_ACCESSKEYSECRET”
	 * @param desc 该信息的描述信息 例："华为云秘钥accessKeySecret"
	 * @param defaultValue 字段的默认值，如果没有默认值请传入 对象 null
	 */
	private void initializationSystem(String name, String desc, String defaultValue) {
		// 检查HUAWEIYUN_ACCESSKEYSECRET字段
		System system = sqlService.findAloneByProperty(System.class, "name", name);
		if(system == null) {
			system = new System();
			// 设置字段描述
			system.setDescription(desc);
			// 设置字段名称
			system.setName(name);
			// 如果有默认值，设为默认值
			if(defaultValue != null) {
				system.setValue(defaultValue);
			}
			// 设置字段的最后修改的时间
			system.setLasttime(DateUtil.timeForUnix10());
			sqlService.save(system);
		}
	}
}