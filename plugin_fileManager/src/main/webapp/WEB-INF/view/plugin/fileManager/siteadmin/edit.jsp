<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="../../../iw/common/head.jsp">
	<jsp:param name="title" value="编辑"/>
</jsp:include>
<!-- 代码编辑模式所需资源 -->
<link rel="stylesheet" href="${STATIC_RESOURCE_PATH}module/editor/css/editormd.css" />
<script src="${STATIC_RESOURCE_PATH}module/editor/editormd.js"></script>

<form id="form" class="layui-form layui-form-pane" action="save.do" method="post" style="padding:5px;">
	<input type="hidden" name="path" value="${path }" />
  <div class="layui-form-item layui-form-text">
    <div class="layui-input-block">
    	<div id="htmlMode" style="width:100%;height:90%;">
			<div id="editormd" style="width:100%; min-height:400px;">
				<textarea name="text" id="html_textarea" lay-verify="text" class="layui-textarea"></textarea>
			</div>
        </div>
    </div>
  </div>
  <div class="layui-form-item" style="text-align:center;">
  	<button class="layui-btn" lay-submit="" lay-filter="demo1">保存</button>
  </div>
</form>

<script>
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引

layui.use(['element', 'form', 'layedit', 'laydate'], function(){
  var form = layui.form;
  var element = layui.element;
  
  //监听提交
  form.on('submit(demo1)', function(data){
		parent.msg.loading('保存中');
		var d=$("form").serialize();
        $.post("save.do", d, function (result) { 
        	parent.msg.close();
        	var obj = JSON.parse(result);
        	if(obj.result == '1'){
        		parent.msg.success("保存成功");
        		parent.layer.close(index);
        	}else if(obj.result == '0'){
        		msg.failure(obj.info);
        	}else{
        		msg.failure(result);
        	}
         }, "text");
		
    return false;
  });
  
});

//在加载完输入模型内容后，进行加载编辑器
function loadEditor(){
	//代码编辑器
	testEditor = editormd("editormd", {
	    width            : "100%",
	    height            : "650px",
	    watch            : false,
	    toolbar          : false,
	    codeFold         : true,
	    searchReplace    : true,
	    placeholder      : "请输入",
	    value            : document.getElementById("html_textarea").value,
	    theme            : "default",
	    mode             : "text/html",
	    path             : '${STATIC_RESOURCE_PATH}module/editor/lib/'
	});
}

//加载输入模型的主要数据
function load(){
	parent.msg.loading("加载中");
	$.post("getFileText.do",{"path":"${path}" },  function(data){
		parent.msg.close();
		if(data.result == '1'){
			//编辑模式，获取模型主要内容成功，加载到textarea
			document.getElementById("html_textarea").innerHTML = data.info;
			loadEditor();
	 	}else{
	 		parent.msg.failure(data.info);
	 		parent.layer.close(index);
	 	}
	});

}
load();


</script>

<jsp:include page="../../../iw/common/foot.jsp"></jsp:include>