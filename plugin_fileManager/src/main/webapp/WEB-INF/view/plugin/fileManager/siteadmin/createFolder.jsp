<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="../../../iw/common/head.jsp">
	<jsp:param name="title" value="新建文件夹"/>
</jsp:include>

<form id="form" class="layui-form" method="post" style="padding:5px; padding-top:30px; padding-right:30px;">
	<input type="hidden" name="path" value="${path }" />
	
	<div class="layui-form-item">
		<label class="layui-form-label" id="label_columnName">文件夹名：</label>
		<div class="layui-input-block">
			<input type="text" name="name" lay-verify="name" autocomplete="off" placeholder="只限英文、数字、下划线_" class="layui-input" value="">
		</div>
	</div>
	
  <div class="layui-form-item" style="text-align:center; padding-top:20px;">
  	<button class="layui-btn" lay-submit="" lay-filter="demo1">建立文件夹</button>
  </div>
</form>

<script>
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引

layui.use(['element', 'form', 'layedit', 'laydate'], function(){
  var form = layui.form;
  var element = layui.element;
  
  
  //自定义验证规则
  form.verify({
    name: function(value){
      if(value.length == 0){
        return '请输入新建文件夹的名字';
      }
    },
  });
  
  //监听提交
  form.on('submit(demo1)', function(data){
		parent.msg.loading('新建中');
		var d=$("form").serialize();
        $.post("createFolderSave.do", d, function (result) { 
        	parent.msg.close();
        	var obj = JSON.parse(result);
        	if(obj.result == '1'){
        		parent.msg.success("新建成功");
        		parent.layer.close(index);
        		parent.location.reload();
        	}else if(obj.result == '0'){
        		msg.failure(obj.info);
        	}else{
        		msg.failure(result);
        	}
         }, "text");
		
    return false;
  });
  
});
</script>

<jsp:include page="../../../iw/common/foot.jsp"></jsp:include>