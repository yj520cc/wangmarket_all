<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../../../iw/common/head.jsp">
	<jsp:param name="title" value="文件列表"/>
</jsp:include>

<body style="text-align:left; min-width:10px;">

<style>
#pathnav{
	height: 46px;
    line-height: 46px;
    font-size: 16px;
    color: #310404a3;
    padding-left: 15px;
}
.nav_a{
	color: #310404a3;
}
.nav_fengefu{
	padding-left:4px;
	padding-right:4px;
}
.suffix_img{
	height:25px;
	padding-right: 5px;
}
</style>
<div style="display:none;" id="pathnav">
	路径导航
	<script>
		var pathnav;
		var pathnavStr = '${pathnav}';
		try{
			pathnav = $.parseJSON(pathnavStr);
			if(pathnav.length > 0){
				var navHTML = '<a href="?path=" class="nav_a">全部文件</a>';	//导航的html	
				//遍历
				for(var i = 0;i < pathnav.length; i++) {
					if(i < pathnav.length){
						navHTML = navHTML + '<span class="nav_fengefu">></span>';
					}
					navHTML = navHTML + '<a href="?path='+pathnav[i].path+'" class="nav_a">'+pathnav[i].name+'</a>';
				}
				document.getElementById('pathnav').style.display='';	//显示出来
				document.getElementById('pathnav').innerHTML=navHTML;
			}
		}catch(e){ console.log(e); }
	</script>
</div>
<table class="layui-table" style="margin-top:0px;">
  <colgroup>
    <col width="120">
    <col width="120">
    <col>
    <col>
  </colgroup>
  <thead>
    <tr>
      <th>文件名</th>
      <th>大小</th>
      <th>最后更改时间</th>
      <th>操作</th>
    </tr>
  </thead>
  <tbody>
  	<c:forEach items="${list}" var="item">
		<tr>
			<td>
				<script>
					//显示文件类型图标
					
					var imgHtml = '<img src="/plugin/fileManager/suffix/unknow.svg" class="suffix_img" />';
					var suffix = '';	//该文件后缀
					if(${item.folder}){
						//是文件夹
						imgHtml = '<img src="/plugin/fileManager/suffix/folder.svg" class="suffix_img" />';
					}else{
						//是文件
						var thispaths = '${item.path }'.split('.');
						if(thispaths.length > 1){
							suffix = thispaths[thispaths.length-1].toLowerCase();
							if(suffix == 'jpg' || suffix == 'jpeg' || suffix == 'png' || suffix == 'gif' || suffix == 'svg' || suffix == 'bmp'){
								//如果是图片文件，那么直接显示图片本身就好
								imgHtml = '<img src="${SiteAttachmentFileUrl}${path}${item.path }?x-oss-process=image/resize,h_25" class="suffix_img" onclick="window.open(\'${SiteAttachmentFileUrl}${path}${item.path }\');" style="cursor:pointer;" />';
							}else{
								imgHtml = '<img src="/plugin/fileManager/suffix/'+suffix+'.svg" class="suffix_img" onerror="this.src=\'/plugin/fileManager/suffix/unknow.svg\';" />';
							}
						}
					}
				</script>
				<c:if test="${item.folder == true}">
					<a href="?path=${path }${item.path }/"><script>document.write(imgHtml);</script>${item.path }</a>
				</c:if>
				<c:if test="${item.folder == false}">
					<a href="javascript:window.open('${SiteAttachmentFileUrl}${path}${item.path }');"><script>document.write(imgHtml);</script>${item.path }</a>
				</c:if>
			</td>
			<td>
				<c:if test="${item.folder == false}">
					<x:fileSizeToInfo size="${item.size }"></x:fileSizeToInfo>
				</c:if>
			</td>
			<td style="width:160px;"><x:time linuxTime="${item.lastModified}" format="yyyy-MM-dd HH:mm:ss"></x:time></td>
			<td style="width:110px;">
				<c:if test="${item.folder == false}">
				   <button class="layui-btn layui-btn-sm " onclick="edit('${item.path }');" style="display:none;" id="${item.path }"><i class="layui-icon">&#xe642;</i></button>
				   <button class="layui-btn layui-btn-sm" onclick="deleteFile('${item.path }', this);" style="margin-left: 0px;"><i class="layui-icon">&#xe640;</i></button>
				</c:if>
			</td>
			<script>
				if((suffix == 'txt' || suffix == 'html' || suffix == 'xml' || suffix == 'js' || suffix == 'css') && ${item.size } < 100000){
					//可以在线编辑
					document.getElementById('${item.path }').style.display='';
				}
			</script>
		</tr>
  	</c:forEach>
  </tbody>
</table>
<!-- 
<button class="layui-btn" onclick="newHtml();" style="margin-left: 10px;margin-bottom: 30px;">
  新建文件夹
</button>
 -->
<button class="layui-btn" id="uploadFile" style="margin-left: 30px;margin-bottom: 30px; margin-top: 10px;">
  上传文件
</button>
<button class="layui-btn" onclick="createFolder();" style="margin-left: 10px;margin-bottom: 30px; margin-top: 10px;">
  新建文件夹
</button>
<button class="layui-btn" onclick="createFile();" style="margin-left: 10px;margin-bottom: 30px; margin-top: 10px;">
  新建文件
</button>

<div style="padding-right:15px; text-align: right;margin-top: -50px;">
	提示：&nbsp;&nbsp;&nbsp;
	<botton class=""><i class="layui-icon">&#xe642;</i></botton><span style="padding-left:12px;padding-right: 30px;">编辑</span>
	<botton class=""><i class="layui-icon">&#xe640;</i></botton><span style="padding-left:12px;padding-right: 30px;">删除</span>
</div>
<div style="padding: 20px;color: gray;">
<style>
.beizhu{
	padding-left:20px;
	padding-top:10px;
}
.beizhu li{
	list-style: decimal;
}
</style>
	<div style="font-size:16px; padding-top:10px;">备注：</div>
	<ul class="beizhu">
		<li>可上传的文件后缀包含：&nbsp;<script> document.write('${allowUploadSuffix}'.replace(/\|/g,"、")); </script></li>
		<li>允许上传不超过 <x:fileSizeToInfo size="${maxFileSizeKB*1024}"></x:fileSizeToInfo> 的文件，超过这个大小的，不允许被上传</li>
		<li>txt、html、xml、js、css 格式的文件，且文件大小不超过100KB的，提供在线编辑功能</li>
		<li>上传的文件，会自动赋予新的文件名(一串字符)</li> 
		<li>列表中的文件，一次最多列出500个，例如,一个目录中有510个文件，列表中只会显示500个文件，另外10个文件是不会显示的。请尽可能控制一个目录中不要超过500个文件</li> 
		<li>上传的文件会进行重命名</li>
		<li>这里上传或操作的文件，系统本身会经过安全过滤，您通过您绑定的域名是访问不到的。比如您在这里添加了一个 1.jpg 的文件，您可以直接点击这个文件进行预览查看，来获取到访问这个文件的url</li>
	</ul>
</div>

<script>
//编辑页面
function edit(path){
	var index = layer.open({
		type: 2, 
		title:'编辑内容', 
		area: ['600px', '460px'],
		shadeClose: true, //开启遮罩关闭
		content: '/plugin/fileManager/siteadmin/edit.do?path=${path}'+path
	});
	layer.full(index);
}

//新建文件夹
function createFolder(){
	var index = layer.open({
		type: 2, 
		title:'新建文件夹', 
		area: ['400px', '220px'],
		shadeClose: true, //开启遮罩关闭
		content: 'createFolder.do?path=${path}'
	});
}

//新建文件
function createFile(){
	var index = layer.open({
		type: 2, 
		title:'新建文件', 
		area: ['400px', '220px'],
		shadeClose: true, //开启遮罩关闭
		content: 'createFile.do?path=${path}'
	});
}

//删除文件
function deleteFile(path, obj){
	var deleteConfirmIndex = layer.confirm('确定要删除 ${path}'+path+' ？', {
		btn: ['删除','取消'] //按钮
	}, function(){
		layer.close(deleteConfirmIndex);
		parent.msg.loading("删除中");
		$.post('/plugin/fileManager/siteadmin/delete.do', {'path':'${path}'+path}, function(data){
			parent.msg.close();
			if(data.result == '1'){
				parent.msg.success('删除成功');
				//删除当前tr，将这一行删除掉
				try{
					var thisD = obj.parentNode.parentNode;
					thisD.parentNode.removeChild(thisD);
				}catch(e){ console.log(e); }
		 	}else if(data.result == '0'){
		 		parent.msg.failure(data.info);
		 	}else{
		 		parent.msg.failure('操作失败');
		 	}
		});
		
	}, function(){
		
	});
}

layui.use('upload', function(){
	layui.upload.render({
	  url: 'upload.do?path=${path}'
	  ,method :'post'
	  ,elem : '#uploadFile'
	  ,exts: '${allowUploadSuffix}'
	  ,field: 'file'
	  ,title :'上传文件'
	  ,size: '${maxFileSizeKB}'	//50MB ，这里单位是KB
      , before: function (obj) {
          parent.msg.loading("上传中");
      }
	  ,done: function(res, index, upload){
	  	parent.msg.close();
	    //上传成功返回值，必须为json格式
	    if(res.result == '1'){
	    	parent.msg.success("上传成功");
	    	location.reload();
	    }else{
	    	parent.msg.failure(res.info);
	    }
	  }
	}); 
});

</script>

</body>
</html>