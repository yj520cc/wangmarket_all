package com.xnx3.wangmarket.plugin.fileManager.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * XSS 工具类
 * @author 管雷鸣
 *
 */
public class XSSUtil {
	
	/**
	 * 判断文件名或路径是否有异常字符，这个异常字符也就是只要不是  [A-Za-z0-9_./] 的都是异常字符
	 * @param fileName 文件名或路径
	 * @return
	 */
	public static boolean find(String fileName){
	      // 创建 Pattern 对象  里面加了 . / 是因为文件名是 xxx.jpg 中间有. ，路径中间有 / 
	      Pattern p = Pattern.compile("([^A-Za-z0-9_./])");
	      // 现在创建 matcher 对象
	      Matcher m = p.matcher(fileName);
	      return m.find();
	}
	
	
	public static void main(String[] args) {
		System.out.println(find("aaasd1./"));
	}
}
