package com.xnx3.wangmarket.plugin.fileManager.controller.siteadmin;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import com.xnx3.wangmarket.agencyadmin.util.SessionUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.xnx3.StringUtil;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.AttachmentUtil;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.j2ee.util.AttachmentMode.bean.SubFileBean;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.j2ee.vo.UploadFileVO;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.plugin.fileManager.util.XSSUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 网站管理后台
 * @author 管雷鸣
 */
@Controller(value="fileManagerSiteAdminPluginController")
@RequestMapping("/plugin/fileManager/siteadmin/")
public class IndexController extends BasePluginController {
	@Resource
	private SqlService sqlService;


	/**
	 * 子文件列表
	 */
	@RequestMapping("list${url.suffix}")
	public String list(HttpServletRequest request,Model model,
			@RequestParam(value = "path", required = false , defaultValue="") String path){
		if(!haveSiteAuth()){
			return error(model, "请先登录");
		}
		
		//windows下，本地存储的时候，这个是不能用的。路径错误
//		if(SystemUtil.isWindowsOS()) {
//			return error(model, "本插件目前只支持Linux系统，暂不支持Windows系统。");
//		}
		
		Site site = SessionUtil.getSite();
		path = path.replaceAll("//", "/"); //obs场景下会多出一个来，所以进行减去
		 
		if(XSSUtil.find(path)){
			return error(model, "路径不合法。文件、文件夹夹名字只限数字、英文、下划线_");
		}
		
		//目录导航
		JSONArray pathJsonArray = new JSONArray();
		if(path.length() > 0){
			String[] paths = path.split("/");
			String currentPath = "";
			for (int i = 0; i < paths.length; i++) {
				currentPath = currentPath + paths[i] + "/";
				JSONObject json = new JSONObject();
				json.put("name", paths[i]);
				json.put("path", currentPath);
				pathJsonArray.add(json);
			}
		}
		//路径导航，格式如 [{"name":"news","path":"news/"},{"name":"dd","path":"news/dd/"},{"name":"aa","path":"news/dd/aa/"}]
		model.addAttribute("pathnav", pathJsonArray.toString());
		
		List<SubFileBean> list = AttachmentUtil.getSubFileList("site/"+site.getId()+"/"+path);
		int removeIndex = -1;	//要删除的list下标。这个云存储会用到，path替换后会存在空字符情况，所以要删除
		if(path.length() > 1) {
			//是子目录，那么文件path将进行替换，去除如 images/1.jpg 的 images/
			for (int i = 0; i < list.size(); i++) {
				SubFileBean bean = list.get(i);
//				System.out.println("--p:"+bean.getPath());
				if(bean.getPath().indexOf(path) > -1) {
					bean.setPath(bean.getPath().replace(path, ""));
				}
//				System.out.println("p:"+bean.getPath());
				if(bean.getPath().length() < 1 || bean.getPath().equals("site/"+site.getId()+"/")) {
					removeIndex = i;
				}
			}
		}
		if(removeIndex > -1) {
			list.remove(removeIndex);
		}
		
		model.addAttribute("list", list);
		
		//去除线上运行的path会带着 site/219/ 前缀的异常
		for (int i = 0; i < list.size(); i++) {
			SubFileBean bean = list.get(i);
			bean.setPath(bean.getPath().replace("site/"+site.getId()+"/", ""));
//			System.out.println(bean.getPath()+",  "+bean.isFolder());
		}
		
		if(Global.isJarRun) {
			//本地运行
		}else {
			//服务器tomcat中运行
			//path = ""; //路径用 ${SiteAttachmentFileUrl}、${item.path } 就能组合
		}
		model.addAttribute("path", path);
		
		//最大上传大小，单位 KB
		model.addAttribute("maxFileSizeKB", AttachmentUtil.getMaxFileSizeKB());			//文件最大大小
		model.addAttribute("allowUploadSuffix", Global.ossFileUploadImageSuffixList);	//允许上传的后缀名文件
		model.addAttribute("SiteAttachmentFileUrl", AttachmentUtil.netUrl()+"site/"+site.getId()+"/");	//站点附件访问url
		return "plugin/fileManager/siteadmin/list";
	}
	
	/**
	 * 编辑文件内容
	 * @return
	 */
	@RequestMapping("edit${url.suffix}")
	public String edit(HttpServletRequest request,Model model,
			@RequestParam(value = "path", required = false , defaultValue="") String path){
		if(!haveSiteAuth()){
			return error(model, "请先登录");
		}
		if(XSSUtil.find(path)){
			return error(model, "路径不合法。文件、文件夹夹名字只限数字、英文、下划线_");
		}
		
		Site site = SessionUtil.getSite();
		path = pathFilter(path, site);
		model.addAttribute("path", path);
		return "plugin/fileManager/siteadmin/edit";
	}
	
	/**
	 * edit 中，获取的原本的文件的文本内容
	 */
	@RequestMapping(value="getFileText${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO getFileText(HttpServletRequest request, Model model,
			@RequestParam(value = "path", required = false , defaultValue="") String path){
		if(!haveSiteAuth()){
			return error("请先登录");
		}
		if(path.length() == 0){
			return error("要编辑哪个文件？");
		}
		Site site = SessionUtil.getSite();
		
		//判断文件的后缀及大小，看是否允许被编辑
		//取后缀名
		String[] sp = path.split("\\.");
		if(sp.length < 2){
			return error("文件后缀不允许被在线编辑");
		}
		String suffix = sp[sp.length-1].trim().toLowerCase();
		if(!(suffix.equals("txt") || suffix.equals("html") || suffix.equals("xml") || suffix.equals("js") || suffix.equals("css"))){
			return error(suffix+" 后缀的文件不允许被在线编辑");
		}
		
		path = pathFilter(path, site);
		//改文件大小，单位是B
		long size = AttachmentUtil.getFileSize(path);
		//判断文件是否大于100KB，如果超过100KB，不允许在线编辑，免得被攻击让服务器内存崩掉
		if(size > 100000){
			return error("文件大小超过100KB，无法在线编辑");
		}
		
		String text = AttachmentUtil.getTextByPath(path);
		if(text == null){
			return error("编辑的文件不存在");
		}
		ActionLogUtil.insert(request, "编辑文件的内容 ftp plugin", StringUtil.filterXss(path));
		
		return success(text);
	}
	
	/**
	 * 保存 文件 的内容
	 * @param text 要保存的文件 的内容
	 */
	@RequestMapping(value="save${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO save(HttpServletRequest request, 
			@RequestParam(value = "path", required = false, defaultValue="") String path,
			@RequestParam(value = "text", required = false, defaultValue="") String text){
		if(!haveSiteAuth()){
			return error("请先登录");
		}
		if(XSSUtil.find(path)){
			return error("路径不合法。文件、文件夹夹名字只限数字、英文、下划线_");
		}
		
		Site site = SessionUtil.getSite();
		path = pathFilter(path, site);
		AttachmentUtil.putStringFile(path, text);
		
		ActionLogUtil.insert(request, "保存文件的内容 ftp plugin", StringUtil.filterXss(path));
		return success();
	}
	

	/**
	 * 删除文件
	 * @param path 要删除的文件，传入格式如 news/a.jpg  ，会自动补为site/id/news/a.jpg 
	 */
	@RequestMapping(value="delete${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO delete(HttpServletRequest request, 
			@RequestParam(value = "path", required = false, defaultValue="") String path){
		if(!haveSiteAuth()){
			return error("请先登录");
		}
		if(path.length() == 0){
			return error("请输入要删除哪个文件");
		}
		Site site = SessionUtil.getSite();
		
		//去掉path的前缀，变为在网站中的目录层次，免得修改了别的网站的信息
		AttachmentUtil.deleteObject("site/"+site.getId()+"/"+path);
		
		ActionLogUtil.insert(request, "删除文件 ftp plugin", StringUtil.filterXss(path));
		return success();
	}
	
	/**
	 * 新建文件夹
	 */
	@RequestMapping("createFolder${url.suffix}")
	public String createFolder(HttpServletRequest request,Model model,
			@RequestParam(value = "path", required = false , defaultValue="") String path){
		if(!haveSiteAuth()){
			return error(model, "请先登录");
		}
		if(XSSUtil.find(path)){
			return error(model, "路径不合法。文件、文件夹夹名字只限数字、英文、下划线_");
		}
		
		Site site = SessionUtil.getSite();
		path = pathFilter(path, site);
		model.addAttribute("path", path);
		return "plugin/fileManager/siteadmin/createFolder";
	}

	/**
	 * 新建文件夹
	 * @param path 要建立的文件夹所在路径，传入格式如 news/a/ 便是在news/路径下建立名为 a 的文件夹 
	 */
	@RequestMapping(value="createFolderSave${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO createFolderSave(HttpServletRequest request, 
			@RequestParam(value = "path", required = false, defaultValue="") String path,
			@RequestParam(value = "name", required = false, defaultValue="") String name){
		if(!haveSiteAuth()){
			return error("请先登录");
		}
		if(name.length() == 0){
			return error("请输入要创建的文件夹名字");
		}
		path = path + name +"/";
		if(XSSUtil.find(path)){
			return error("名字不合法。文件、文件夹夹名字只限数字、英文、下划线_");
		}
		Site site = SessionUtil.getSite();
		path = pathFilter(path, site);
		path = path.replaceAll("//", "/");	//obs场景下会多出一个来，所以进行减去
		AttachmentUtil.createFolder(path);
		
		ActionLogUtil.insert(request, "创建文件夹 ftp plugin", path);
		return success();
	}
	

	/**
	 * 上传文件
	 * @param path 要将文件上传到哪个目录下，传入格式如 news/a/ 
	 */
	@RequestMapping(value="upload${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO upload(HttpServletRequest request, 
			@RequestParam(value = "path", required = false, defaultValue="") String path,
			@RequestParam("file") MultipartFile multipartFile){
		if(!haveSiteAuth()){
			return error("请先登录");
		}
		if(XSSUtil.find(path)){
			return error("名字不合法。文件、文件夹夹名字只限数字、英文、下划线_");
		}
		Site site = SessionUtil.getSite();
		path = pathFilter(path, site);
		
		UploadFileVO vo = AttachmentUtil.uploadFileByMultipartFile(path, multipartFile);
		if(vo.getResult() - UploadFileVO.SUCCESS == 0){
			//成功
			ActionLogUtil.insert(request, "上传文件", StringUtil.filterXss(multipartFile.getName()));
		}
		return vo;
	}
	
	
	/**
	 * 新建文件
	 */
	@RequestMapping("createFile${url.suffix}")
	public String createFile(HttpServletRequest request,Model model,
			@RequestParam(value = "path", required = false , defaultValue="") String path){
		if(!haveSiteAuth()){
			return error(model, "请先登录");
		}
		if(XSSUtil.find(path)){
			return error(model, "路径不合法。文件、文件夹夹名字只限数字、英文、下划线_");
		}
		
		Site site = SessionUtil.getSite();
		path = pathFilter(path, site);
		model.addAttribute("path", path);
		return "plugin/fileManager/siteadmin/createFile";
	}

	/**
	 * 新建文件，保存，只允许创建txt、html、xml格式文件
	 * @param path 要在哪个目录下新建文件，传入格式如 news/a/
	 * @param name 新建的文件名，如 abc.txt
	 */
	@RequestMapping(value="createFileSave${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO createFileSave(HttpServletRequest request, 
			@RequestParam(value = "path", required = false, defaultValue="") String path,
			@RequestParam(value = "name", required = false, defaultValue="") String name){
		if(!haveSiteAuth()){
			return error("请先登录");
		}
		if(XSSUtil.find(path)){
			return error("名字不合法。文件、文件夹夹名字只限数字、英文、下划线_");
		}
		if(XSSUtil.find(name)){
			return error("文件名不合法。文件名字只限数字、英文、下划线_，如 abc.txt");
		}
		//取后缀名
		String[] sp = name.split("\\.");
		if(sp.length < 2){
			return error("没有文件后缀，不允许被创建");
		}
		String suffix = sp[sp.length-1].trim().toLowerCase();
		if(!(suffix.equals("txt") || suffix.equals("html") || suffix.equals("xml"))){
			return error("该后缀的文件不允许在线创建");
		}
		Site site = SessionUtil.getSite();
		path = pathFilter(path, site);
		
		AttachmentUtil.putStringFile(path+name, " ");
		ActionLogUtil.insert(request, "新建文件", path+name);
		return success();
	}
	
	/**
	 * 用户传入的path过滤，免得操作了别人的文件
	 * @param path 用户传入的path
	 * @param site 当前登录的网站
	 * @return 过滤好的path
	 */
	private static String pathFilter(String path, Site site){
		//去掉path的前缀，变为在网站中的目录层次，免得修改了别的网站的信息
		path = path.replace("site/"+site.getId()+"/", "");
		//加入site/id/，变为用户只能看自己网站下的目录文件
		path = "site/"+site.getId()+"/"+path;
		return path;
	}
}