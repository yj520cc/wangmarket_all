package com.xnx3.wangmarket.plugin.fileManager;

import com.xnx3.j2ee.pluginManage.PluginRegister;

/**
 * 文件管理
 * @author 管雷鸣
 *
 */
@PluginRegister(menuTitle="文件管理", menuHref="/plugin/fileManager/siteadmin/list.do", intro="文件管理，管理自己网站内的所有文件，在线编辑、新增、上传文件等。", applyToCMS=true, version="1.2.1", versionMin="5.7")
public class Plugin{

}
