package com.xnx3.wangmarket.plugin.siteSubAccount;

import com.xnx3.j2ee.pluginManage.PluginRegister;

/**
 * (站点) 子账号管理
 * @author 管雷鸣
 */
@PluginRegister(menuTitle = "子账号",menuHref="/plugin/siteSubAccount/user/list.do", applyToCMS=true, intro="网站子账号开通，可自由为其分配管理网站的哪些权限", version="1.4", versionMin="5.5")
public class Plugin{
	
}