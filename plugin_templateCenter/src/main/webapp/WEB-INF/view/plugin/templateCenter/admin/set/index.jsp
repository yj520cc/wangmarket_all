<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../../../iw/common/head.jsp">
	<jsp:param name="title" value="设置"/>
</jsp:include>
<script src="/<%=Global.CACHE_FILE %>Template_iscommon.js"></script>
<script src="/<%=Global.CACHE_FILE %>Template_type.js"></script>


<div style="padding:15px; padding-left:35px;  padding-top: 50px;">
	当前是否使用云端模版库：
	<span id="useCloudTemplate">
		加载中...
	</span>
	<a href="/template.do" target="_black" class="layui-btn layui-btn-sm" style="margin-left:10px;">查看模版列表</a>
</div>
<div style="margin-left: 35px; font-size: 12px; padding-bottom: 30px;">
	说明：用户在创建网站后，第一次登陆时，会进行选择模版，出现模版列表。另外还可以通过 <a href="/template.do" target="_black">${MASTER_SITE_URL}template.do</a> 进行查看可用模版
	<ul style=" margin-left: 35px;">
		<li>使用云端模版：在模版列表中会显示云端模版、本地自己模版库的模版</li>
		<li>不使用云端模版：模版列表中不会显示云端模版，只显示自己本地模版库中的模版。</li>
	</ul>
</div>
<hr class="layui-bg-gray">

<div style="margin-left: 35px; font-size: 14px;">
	如果你只是做一个网站的，那么完全不必用此方式做。你可以直接创建一个网站，有一些资源如 css、js 等需要引入的资源，可以单独放到一个其他的FTP中，通过URL绝对路径将所需的资源引入即可。
	比如，你将 style.css 放到了 ftp空间中，可以使用域名 http://1234.abc.com/css/style.css 打开浏览，那么你在做模版页面的时候，引入css资源文件时，直接这样写上绝对路径引入即可，如：
	<br/>
	&lt;link href="http://1234.abc.com/css/style.css" rel="stylesheet"&gt;
	
</div>
<hr class="layui-bg-gray">


<script>
//更改当前系统是否使用云端模版库，0不使用，1使用
function changeUseCloudTemplate(isuse){
	parent.iw.loading("更改中");    //显示“操作中”的等待提示
	$.post("/plugin/templateCenter/admin/set/updateCloudIsUse.do?isuse="+isuse, function(data){
	    parent.iw.loadClose();    //关闭“操作中”的等待提示
	    if(data.result == '1'){
	         parent.iw.msgSuccess('更改成功');
	         //更改界面显示
	         initUseCloudTemplate(isuse);
	     }else if(data.result == '0'){
	         parent.iw.msgFailure(data.info);
	     }else{
	         parent.iw.msgFailure();
	     }
	});
}

// isuse 是否使用。 0不使用，其他都是使用
function initUseCloudTemplate(isuse){
	var text = '';
	
	if(isuse != null && isuse == '0'){
		//不使用云端模版库
		text = '<span class="useValue">不使用</span><button onClick="changeUseCloudTemplate(1);" class="layui-btn layui-btn-sm" style="margin-left:10px;">改为使用云端模版库</button>';
	}else{
		//使用云端模版库
		text = '<span class="useValue">使用</span><button onClick="changeUseCloudTemplate(0);" class="layui-btn layui-btn-sm" style="margin-left:10px;">改为不使用云端模版库</button>';
	}
	
	document.getElementById('useCloudTemplate').innerHTML = text;
}
initUseCloudTemplate('${useCloudTemplate}');

</script>

<script src="/plugin/templateCenter/js/manage.js?v=<%=G.VERSION %>"></script>
<jsp:include page="../../../../iw/common/foot.jsp"></jsp:include>                  