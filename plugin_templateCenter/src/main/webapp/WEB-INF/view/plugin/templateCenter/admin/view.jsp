<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.entity.User"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<jsp:include page="../../../iw/common/head.jsp">
	<jsp:param name="title" value="模版信息"/>
</jsp:include>
<script src="/<%=Global.CACHE_FILE %>Template_iscommon.js"></script>
<script src="/<%=Global.CACHE_FILE %>Template_type.js"></script>

<script>
//页面上输出模版性质是共享还是私有
function writeIscommon(iscommon){
	if(iscommon == '1'){
		document.write('共享');
	}else{
		document.write('私有');
	}
}

//是否支持某个终端
function writeTerminalSupport(ter){
	if(ter == '1'){
		document.write('<i class="layui-icon layui-icon-ok"></i>');
	}else{
		//document.write('');
	}
}
</script>

<div style="text-align:center; padding:25px;">
	<a href="${attachmentUrl }websiteTemplate/${template.name }/preview.jpg" target="_black"><img src="${attachmentUrl }websiteTemplate/${template.name }/preview.jpg" style="max-height:200px;"/></a>
</div>

<table class="layui-table iw_table">
	<tbody>
		<tr>
			<td class="iw_table_td_view_name">模版编码</td>
			<td>${template.name }</td>
		</tr>
		<tr>
			<td class="iw_table_td_view_name">模版开发时间</td>
			<td><x:time linuxTime="${template['addtime'] }"></x:time></td>
		</tr>
		<tr>
			<td>模版性质</td>
			<td>
				<script type="text/javascript">writeIscommon(${template.iscommon });</script>
				<botton class="layui-btn layui-btn-xs" onclick="updateIscommon('${template['name'] }', ${template['iscommon'] });" style="margin-left: 23px;"><i class="layui-icon layui-icon-edit"></i></botton>
			</td>
		</tr>
		<tr>
			<td>所属分类</td>
			<td><script type="text/javascript">document.write(type[${template.type}]);</script></td>
		</tr>
		
		
		<tr>
			<td>模版说明</td>
			<td>${template['remark']}</td>
		</tr>
		<tr>
			<td>模版预览体验URL</td>
			<td>
				${template['previewUrl']}
				<botton class="layui-btn layui-btn-xs" onclick="updateSave('${template['name'] }', 'previewUrl', '模版预览体验URL', '${template['previewUrl'] }');" style="margin-left: 23px;"><i class="layui-icon layui-icon-edit"></i></botton>
			</td>
		</tr>
		<tr style="display:none;">
			<td>纯模版(wscso文件)URL</td>
			<td>
				${template.wscsoDownUrl}
				<botton class="layui-btn layui-btn-xs" onclick="updateSave('${template['name'] }', 'wscsoDownUrl', '纯模版(wscso文件)URL', '${template['wscsoDownUrl'] }');" style="margin-left: 23px;"><i class="layui-icon layui-icon-edit"></i></botton>
			</td>
		</tr>
		<tr style="display:none;">
			<td>模版资源(zip文件)URL</td>
			<td>
				${template.zipDownUrl}
				<botton class="layui-btn layui-btn-xs" onclick="updateSave('${template['name'] }', 'zipDownUrl', '模版资源(zip文件)URL', '${template['zipDownUrl'] }');" style="margin-left: 23px;"><i class="layui-icon layui-icon-edit"></i></botton>
			</td>
		</tr>
		
    </tbody>
</table>

<div style=" padding: 5px; padding-top: 26px; padding-left: 16px; font-size: 14px;">
	模版所支持的终端：
</div>
<table class="layui-table iw_table">
	<tbody>
		<tr>
			<td class="iw_table_td_view_name">电脑端</td>
			<td><script type="text/javascript">writeTerminalSupport(${template['terminalPc'] });</script></td>
		</tr>
		<tr>
			<td class="iw_table_td_view_name">手机端</td>
			<td><script type="text/javascript">writeTerminalSupport(${template['terminalMobile'] });</script></td>
		</tr>
		<tr>
			<td class="iw_table_td_view_name">Ipad</td>
			<td><script type="text/javascript">writeTerminalSupport(${template['terminalIpad'] });</script></td>
		</tr>
		<tr>
			<td class="iw_table_td_view_name">广告展示机</td>
			<td><script type="text/javascript">writeTerminalSupport(${template['terminalDisplay'] });</script></td>
		</tr>
    </tbody>
</table>


<script type="text/javascript">
//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
parent.layer.iframeAuto(index);

</script>

<script src="/plugin/templateCenter/js/manage.js?v=<%=G.VERSION %>"></script>
<jsp:include page="../../../iw/common/foot.jsp"></jsp:include>  