<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../../iw/common/head.jsp">
	<jsp:param name="title" value="模版列表"/>
</jsp:include>
<script src="/<%=Global.CACHE_FILE %>Template_iscommon.js"></script>
<script src="/<%=Global.CACHE_FILE %>Template_type.js"></script>

<jsp:include page="../../../iw/common/list/formSearch_formStart.jsp" ></jsp:include>
	<jsp:include page="../../../iw/common/list/formSearch_input.jsp">
		<jsp:param name="iw_label" value="模版编码"/>
		<jsp:param name="iw_name" value="name"/>
	</jsp:include>
	<jsp:include page="../../../iw/common/list/formSearch_input.jsp">
		<jsp:param name="iw_label" value="公司名"/>
		<jsp:param name="iw_name" value="companyname"/>
	</jsp:include>
	<jsp:include page="../../../iw/common/list/formSearch_input.jsp">
		<jsp:param name="iw_label" value="作者名"/>
		<jsp:param name="iw_name" value="username"/>
	</jsp:include>
	<jsp:include page="../../../iw/common/list/formSearch_input.jsp">
		<jsp:param name="iw_label" value="类型"/>
		<jsp:param name="iw_type" value="select"/>
		<jsp:param name="iw_name" value="type"/>
	</jsp:include>
	<jsp:include page="../../../iw/common/list/formSearch_input.jsp">
		<jsp:param name="iw_label" value="性质"/>
		<jsp:param name="iw_type" value="select"/>
		<jsp:param name="iw_name" value="iscommon"/>
	</jsp:include>
	
	<input class="layui-btn iw_list_search_submit" type="submit" value="搜索" />
	
	<button class="layui-btn layui-btn-normal" type="button" style="float: right; margin-right:10px;" id="loadLocalTemplateFile">
		<i class="layui-icon">&#xe67c;</i>
		添加/更新模版
	</button>
</form>	
        
<script>
layui.use('upload', function(){
	layui.upload.render({
	  url: 'importTemplate.do'
	  ,method :'post'
	  ,elem : '#loadLocalTemplateFile'
	  ,exts: 'zip'
	  ,field: 'templateFile'
	  //,size: '200000'	//200MB ，这里单位是KB
      , before: function (obj) {
          iw.loading("上传中");
      }
	  ,done: function(res, index, upload){
		iw.loadClose();
	    //上传成功返回值，必须为json格式
	    if(res.result == '1'){
	    	parent.iw.msgSuccess("模版更新成功！");
	    	//刷新当前页面
			window.location.reload();
	    }else{
	    	alert(res.info);
	    }
	  }
	}); 
});


//页面上输出模版性质是共享还是私有
function writeIscommon(iscommon){
	if(iscommon == '1'){
		document.write('公共');
	}else{
		document.write('<span style="color:red;">私有</span>');
	}
}

//是否支持某个终端
function writeTerminalSupport(ter){
	if(ter == '1'){
		document.write('<i class="layui-icon layui-icon-ok"></i>');
	}else{
		//document.write('');
	}
}
</script>         
  
<table class="layui-table iw_table">
	<thead>
		<tr>
			<th>模版编码</th>
			<th>公司名</th>
			<th>开发者名</th>
			<th>模版性质</th>
			<th>手机端</th>
			<th>电脑端</th>
			<th>IPad</th>
			<th>展示机</th>
			<th>分类</th>
			<th>创建时间</th>
			<th>操作</th>
		</tr> 
	</thead>
	<tbody>
		<c:forEach items="${list}" var="item">
			<tr>
				<td style="width:140px; cursor: pointer;" onclick="templateView(${item['id'] });">${item['name'] }</td>
				<td style="width:250px;">${item['companyname'] }</td>
				<td style="width:80px;">${item['username'] }</td>
				<td style="width:80px; cursor: pointer;" onclick="updateIscommon('${item['name'] }', ${item['iscommon'] });"><script type="text/javascript">writeIscommon(${item['iscommon'] });</script></td>
				<td style="width:40px;"><script type="text/javascript">writeTerminalSupport(${item['terminalMobile'] });</script></td>
				<td style="width:40px;"><script type="text/javascript">writeTerminalSupport(${item['terminalPc'] });</script></td>
				<td style="width:40px;"><script type="text/javascript">writeTerminalSupport(${item['terminalIpad'] });</script></td>
				<td style="width:40px;"><script type="text/javascript">writeTerminalSupport(${item['terminalDisplay'] });</script></td>
				<td style="width:80px;"><script type="text/javascript">document.write(type[${item['type']}]);</script></td>
				<td style="width:100px;"><x:time linuxTime="${item['addtime'] }" format="yyyy-MM-dd"></x:time></td>
				<td style="width:100px;">
					<botton class="layui-btn layui-btn-sm" onclick="templateView(${item['id'] });" style="margin-left: 3px;"><i class="layui-icon layui-icon-tips"></i></botton>
				</td>
			</tr>
		</c:forEach>
  </tbody>
</table>
<!-- 通用分页跳转 -->
<jsp:include page="../../../iw/common/page.jsp" ></jsp:include>


<div style="padding: 20px;color: gray;">
	<div>使用提示:</div>
	<div>这里只显示你自己的私有模版。云端模版不会显示。</div>
	<div>列表中，点击某项模板的模版性质，可对其进行改动。模版性质也就是模版开放成度，包括：
		<ul style=" margin-left: 35px; font-size: 14px;">
			<li>私有：这个模版是某个建站用户私有的，别人看不到，也无法使用。只有这个用户有这个模版的使用权。</li>
			<li>公共：所有人都能看到这个模版。所有人都能使用这个模版。会出现在模版列表中。</li>
		</ul>
	</div>
	<div>当云端模版跟你这里模版库的模版有相同名字的模版时，会优先使用你自己模版库的模版。也就是你自己的模版的优先级要高于云端模版库。</div>
	<div>点击右上方的 <i class="layui-icon">&#xe67c;</i> 添加/更新模版 可添加或更新当前模版。
		<ul style=" margin-left: 35px; font-size: 14px;">
			<li>当选择的模版是现在本地模版库所没有的，则会添加模版，将其加入进本地模版库。</li>
			<li>当选择的模版在现在的本地模版库已经存在是，会将当前上传的模版覆盖之前存在的模版，起到更新作用。更新只会更新zip压缩包的模版文件以及template.wscso文件，并不会更新当前模版的的开发信息，比如公司名、分类
			是否支持手机端、电脑端等。</li>
			<li>导入的模版，默认是私有的，你可以点击其，将其改为公共，用户创建网站选择模版时，边可选择这个模版了。</li>
		</ul>
	</div>
	<div>所上传的zip文件大小无限制，但建议尽量将其简化。</div>
</div>


<script>
//查看详情信息
function templateView(id){
	layer.open({
		type: 2, 
		title:'查看模版信息', 
		area: ['460px', '770px'],
		shadeClose: true, //开启遮罩关闭
		content: 'view.do?id='+id
	});
}

</script>

<script src="/plugin/templateCenter/js/manage.js?v=<%=G.VERSION %>"></script>
<jsp:include page="../../../iw/common/foot.jsp"></jsp:include>                  