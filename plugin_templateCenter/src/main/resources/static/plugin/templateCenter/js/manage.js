/*
 * 修改模版性质，修改成共享还是私有
 * templateName 要修改的模版名字
 * currentIscommon 传入当前是共享还是私有的
 */
function updateIscommon(templateName, currentIscommon){
	var newIsCommon = currentIscommon? '0':'1';
	
	layer.confirm('确定要将模版 '+templateName+' 的开放程度改为 [ '+iscommon[newIsCommon]+' ] ?', {icon: 3, title:'提示'}, function(index){
		parent.iw.loading("更改中");    //显示“更改中”的等待提示
		$.post(
		    "updateIscommon.do", 
		    { "iscommon": newIsCommon, "templateName":templateName }, 
		        function(data){
		        iw.loadClose();    //关闭“更改中”的等待提示
		        if(data.result != '1'){
		        	parent.iw.msgFailure(data.info);
		        }else{
		        	parent.iw.msgSuccess("更改成功");
		        	//刷新当前页面
		        	window.location.reload();
		        }
		    }, 
		"json");
		layer.close(index);
	});
}

/**
 * 更改模版的一些信息
 * templateName 更改的模版的名字
 * columnName 更改模版的那个列，如 preview_pic
 * columnNameRemark 更改模版的那个列，文字说明，如模版预览图
 * oldValue 此模版这个列旧的值
 */
function updateSave(templateName, columnName, columnNameRemark, oldValue){
	layer.prompt({
		  formType: 2,
		  value: oldValue,
		  title: columnNameRemark,
		  area: ['300px', '150px'] //自定义文本域宽高
	}, function(value, index, elem){
		iw.loading("保存中");
		  $.post(
			    "updateSave.do", 
			    { "templateName":templateName, "columnName":columnName, "value":value }, 
			        function(data){
			        iw.loadClose();    //关闭“更改中”的等待提示
			        if(data.result != '1'){
			        	parent.iw.msgFailure(data.info);
			        }else{
			        	parent.iw.msgSuccess("更改成功");
			        	//刷新当前页面
			        	window.location.reload();
			        }
			    }, 
			"json");
		  
		  
		  layer.close(index);
	});
}
