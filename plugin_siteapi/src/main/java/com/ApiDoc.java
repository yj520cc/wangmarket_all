package com;

import com.xnx3.doc.JavaDoc;

public class ApiDoc {
	public static void main(String[] args) {
		JavaDoc doc = new JavaDoc("com.xnx3.wangmarket.plugin.siteapi.controller");
		
		doc.name = "网市场云建站-网站开放API";
		doc.welcome = "<p>您可以使用开放接口来获取网站的栏目、文章、以及全局变量等信息</p>"
				+ "<p style=\"width:100%; text-align:center;\"><img style=\"width:200px;\" src=\"http://cdn.weiunity.com/site/1893/templateimage/dca2d002c6ca42da943d648cc62b8fdc.jpg\" /></p>"
				+ "<p>微信公众号：wangmarket</p>";
		doc.domain = "http://localhost:8080";
		doc.version = "v5.6.0.20220428";
		
		doc.generateHtmlDoc();
	}
}
