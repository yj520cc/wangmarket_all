package com.xnx3.wangmarket.plugin.siteapi.controller;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.BaseVO;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.wangmarket.admin.cache.TemplateCMS;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.entity.SiteColumn;
import com.xnx3.wangmarket.admin.entity.Template;
import com.xnx3.wangmarket.plugin.siteapi.vo.ColumnListVO;
import com.xnx3.wangmarket.plugin.siteapi.vo.bean.ColumnBean;

/**
 * 栏目相关
 * @author 管雷鸣
 */
@Controller(value="SiteAPIColumnPluginController")
@RequestMapping("/plugin/siteapi/column/")
public class ColumnController extends BasePluginController {
	@Resource
	private SqlService sqlService;
	@Resource
	private SqlCacheService sqlCacheService;
	
	/**
	 * 获取网站内的栏目列表
	 * @author 管雷鸣
	 * @param siteid 网站id，要获取的栏目列表是属于哪个网站的。对应 site.id
	 */
	@ResponseBody
	@RequestMapping(value="list.json",method= {RequestMethod.POST})
	public ColumnListVO list(HttpServletRequest request, 
			@RequestParam(required = true, defaultValue="219") int siteid){
		
		ColumnListVO vo = new ColumnListVO();
		if(siteid < 1){
			//请传入站点id
			vo.setBaseVO(BaseVO.FAILURE, "请传入siteid，不然怎么知道是要获取哪个网站的呢");
			return vo;
		}
		
		Site site = sqlCacheService.findById(Site.class, siteid);
		if(site == null){
			vo.setBaseVO(BaseVO.FAILURE, "要获取的网站不存在");
			return vo;
		}
		
		//获取当前的栏目列表
	    List<SiteColumn> list = sqlCacheService.findByProperty(SiteColumn.class, "siteid", siteid, 10);
	    
	    //列表中的值，替换公共标签使用
	  	Template templateEntity = sqlCacheService.findAloneByProperty(Template.class, "name", site.getTemplateName());
	  	TemplateCMS template = new TemplateCMS(site, templateEntity);
	    
	    //将实体类转化为bean
  		List<ColumnBean> beanList = new ArrayList<ColumnBean>();
  		for (int i = 0; i < list.size(); i++) {
  			SiteColumn siteColumn = list.get(i);
  			ColumnBean bean = new ColumnBean(siteColumn);
  			bean.setIcon(template.replacePublicTag(bean.getIcon()));
  			beanList.add(bean);
  		}
	    vo.setList(beanList);
	    
	    ActionLogUtil.insert(request, siteid,"获取栏目列表");	//日志记录
		return vo;
	}
	
	
}