package com.xnx3.wangmarket.plugin.siteapi.vo;

import java.util.ArrayList;
import java.util.List;

import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.News;
import com.xnx3.wangmarket.plugin.siteapi.vo.bean.NewsBean;

/**
 * 文章列表
 * @author 管雷鸣
 *
 */
public class NewsListVO extends BaseVO{
	private List<NewsBean> list;	//文章属性列表
	private Page page;	//分页信息
	
	public List<NewsBean> getList() {
		return list;
	}
	public void setList(List<News> list) {
		//将news转化为newsbean
		this.list = new ArrayList<NewsBean>();
		
		for (int i = 0; i < list.size(); i++) {
			this.list.add(new NewsBean(list.get(i)));
		}
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
}
