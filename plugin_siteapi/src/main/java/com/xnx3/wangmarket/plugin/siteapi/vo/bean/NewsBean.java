package com.xnx3.wangmarket.plugin.siteapi.vo.bean;

import com.xnx3.wangmarket.admin.entity.News;

/**
 * 文章属性信息，对应 News 表
 * @author 管雷鸣
 *
 */
public class NewsBean {
	private Integer id;	//文章的id编号
	private Integer addtime;	//添加文章的时间，十位时间戳
	private String title;	//文章的标题
	private String titlepic;	//标题图片，图片url地址
	private String intro;	//文章的简介
	private Integer cid;	//栏目的id编号，属于哪一个栏目的文章。对应column.id
	
	public NewsBean() {
	}
	
	public NewsBean(News news) {
		if(news == null){
			return;
		}
		
		this.id = news.getId();
		this.addtime = news.getAddtime();
		this.title = news.getTitle();
		this.titlepic = news.getTitlepic();
		this.intro = news.getIntro();
		this.cid = news.getCid();
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAddtime() {
		return addtime;
	}
	public void setAddtime(Integer addtime) {
		this.addtime = addtime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitlepic() {
		return titlepic;
	}
	public void setTitlepic(String titlepic) {
		this.titlepic = titlepic;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}

	
}
