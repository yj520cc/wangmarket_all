package com.xnx3.wangmarket.plugin.siteapi.controller;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.BaseVO;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.wangmarket.admin.entity.News;
import com.xnx3.wangmarket.admin.entity.NewsData;
import com.xnx3.wangmarket.admin.entity.SiteColumn;
import com.xnx3.wangmarket.plugin.siteapi.vo.NewsListVO;
import com.xnx3.wangmarket.plugin.siteapi.vo.NewsVO;

import net.sf.json.JSONObject;

/**
 * 文章方面
 * @author 管雷鸣
 */
@Controller(value="SiteAPINewsPluginController")
@RequestMapping("/plugin/siteapi/news/")
public class NewsController extends BasePluginController {
	@Resource
	private SqlService sqlService;
	@Resource
	private SqlCacheService sqlCacheService;
	
	/**
	 * 获取网站内的文章列表
	 * @author 管雷鸣
	 * @param siteid 网站id，要获取的文章列表是属于哪个网站的。对应 site.id
	 * @param columnid 栏目id，要获取的文章列表是属于哪个栏目的。
	 * 				<p>如不传入，则是获取该网站内的所有文章</p>
	 * @param everyNumber 每页显示几条，非必填。如不传入，默认返回12条文章数据。
	 * @param title 是否要根据文章标题进行搜索，如果要根据标题进行搜索，则传入title，支持模糊搜索
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="list.json",method= {RequestMethod.POST})
	public NewsListVO list(HttpServletRequest request, 
			@RequestParam(required = true, defaultValue="219") int siteid,
			@RequestParam(required = false, defaultValue="0") int columnid,
			@RequestParam(required = false, defaultValue="12") int everyNumber,
			@RequestParam(required = false, defaultValue="") String title){
		NewsListVO vo = new NewsListVO();
		if(siteid < 1){
			//请传入站点id
			vo.setBaseVO(BaseVO.FAILURE, "请传入siteid，不然怎么知道是要获取哪个网站的呢");
			return vo;
		}
		
		//排序方式，默认是id desc
		String orderBySql = "id DESC";
		//指定了栏目
		if(columnid > 0){
			SiteColumn siteColumn = sqlCacheService.findById(SiteColumn.class, columnid);
			if(siteColumn == null){
				vo.setBaseVO(BaseVO.FAILURE, "栏目不存在");
				return vo;
			}
			switch (siteColumn.getListRank()) {
			case 1:	//SiteColumn.LIST_RANK_ADDTIME_DESC
				orderBySql = "addtime DESC";
				break;
			case 2:	//LIST_RANK_ADDTIME_ASC
				orderBySql = "addtime ASC";
				break;
			default:
				break;
			}
		}
		
		
		Sql sql = new Sql(request);
	    /*
	     * 设置可搜索字段。这里填写的跟user表的字段名对应。只有这里配置了的字段，才会有效。这里没有配置，则不会进行筛选
	     * 具体规则可参考： http://note.youdao.com/noteshare?id=3ccef2de6a5cda01f95f832b02e356d0&sub=D53E681BBFF04822977C7CFBF8827863
	     */
	    sql.setSearchColumn(new String[]{"title","siteid"});
	    if(columnid > 0) {
	    	sql.appendWhere("cid = " + columnid);
	    }
	    //查询user数据表的记录总条数。 传入的user：数据表的名字为user
	    int count = sqlService.count("news", sql.getWhere());
	    //创建分页，并设定每页显示15条
	    Page page = new Page(count, everyNumber, request);
	    //创建查询语句，只有SELECT、FROM，原生sql查询。其他的where、limit等会自动拼接
	    sql.setSelectFromAndPage("SELECT * FROM news", page);
	    sql.setOrderBy(orderBySql);
	    //因只查询的一个表，所以可以将查询结果转化为实体类，用List接收。
	    List<News> list = sqlService.findBySql(sql, News.class);
	    
	    vo.setList(list);
	    vo.setPage(page);
	    
	    ActionLogUtil.insert(request, siteid,"获取文章列表");	//日志记录
		return vo;
	}
	
	
	/**
	 * 根据文章的id编号，获取文章详细信息。
	 * @author 管雷鸣
	 * @param id 文章的id，也就是news.id ，要获取的是哪个文章
	 */
	@ResponseBody
	@RequestMapping(value="view.json",method= {RequestMethod.POST})
	public NewsVO view(HttpServletRequest request, @RequestParam(required = true, defaultValue="1") int id){
		NewsVO vo = new NewsVO();
		
		//调取文章信息
		News news = sqlCacheService.findById(News.class, id, 10);
		if(news == null){
			vo.setBaseVO(BaseVO.FAILURE, "文章不存在");
			return vo;
		}
		
		//调取文章内容
		NewsData newsData = sqlCacheService.findById(NewsData.class, news.getId(), 10);
		if(newsData == null){
			vo.setBaseVO(BaseVO.FAILURE, "文章的详情信息不存在");
			return vo;
		}
		vo.setNews(news);
		if(newsData.getExtend() == null || newsData.getExtend().length() == 0){
			vo.setExtend(new JSONObject());
		}else{
			vo.setExtend(JSONObject.fromObject(newsData.getExtend()));
		}
		vo.setText(newsData.getText());
		
		//调取文章当前所属栏目
		SiteColumn siteColumn = sqlCacheService.findById(SiteColumn.class, news.getCid(), 10);
		vo.setColumn(siteColumn);
		
		return vo;
	}
	
}