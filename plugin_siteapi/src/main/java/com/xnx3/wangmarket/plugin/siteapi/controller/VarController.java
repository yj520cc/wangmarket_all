package com.xnx3.wangmarket.plugin.siteapi.controller;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.BaseVO;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.wangmarket.admin.cache.TemplateCMS;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.entity.Template;
import com.xnx3.wangmarket.admin.service.SiteVarService;
import com.xnx3.wangmarket.admin.util.ActionLogUtil;
import com.xnx3.wangmarket.plugin.siteapi.vo.VarListVO;
import com.xnx3.wangmarket.plugin.siteapi.vo.bean.VarBean;
import net.sf.json.JSONObject;

/**
 * 网站全局变量方面
 * @author 管雷鸣
 */
@Controller(value="SiteAPIVarPluginController")
@RequestMapping("/plugin/siteapi/var/")
public class VarController extends BasePluginController {
	@Resource
	private SiteVarService siteVarService;
	@Resource
	private SqlCacheService sqlCacheService;
	
	/**
	 * 获取指定网站的全局变量列表
	 * @author 管雷鸣
	 * @param siteid 网站id，要获取的数据是属于哪个网站的。对应 site.id
	 */
	@ResponseBody
	@RequestMapping(value="list.json",method= {RequestMethod.POST})
	public VarListVO list(HttpServletRequest request, 
			@RequestParam(required = true, defaultValue="219") int siteid){
		VarListVO vo = new VarListVO();
		if(siteid < 1){
			vo.setBaseVO(BaseVO.FAILURE, "请传入siteid，不然怎么知道是要获取哪个网站的呢");
			return vo;
		}
		
		Site site = sqlCacheService.findById(Site.class, siteid);
		if(site == null){
			vo.setBaseVO(BaseVO.FAILURE, "要获取的网站不存在");
			return vo;
		}
		
		JSONObject json = siteVarService.getVar(siteid);
		
		
		//列表中的值，替换公共标签使用
		Template templateEntity = sqlCacheService.findAloneByProperty(Template.class, "name", site.getTemplateName());
		TemplateCMS template = new TemplateCMS(site, templateEntity);
		
		//将json转化为list形式
		List<VarBean> list = new ArrayList<VarBean>();
		Iterator<String> iter = json.keys();
        while (iter.hasNext()) {
        	String key = iter.next();
        	JSONObject item = json.getJSONObject(key);
        	
        	VarBean varBean = new VarBean(item);
        	varBean.setName(key);
        	varBean.setValue(template.replacePublicTag(item.getString("value").replaceAll("\r|\n", " ")));
            
            list.add(varBean);
        }
		vo.setList(list);
        
        ActionLogUtil.insert(request, site.getId(),  "查看网站全局变量列表");
		return vo;
	}
	
	
}