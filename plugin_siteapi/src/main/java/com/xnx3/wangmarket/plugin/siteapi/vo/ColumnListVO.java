package com.xnx3.wangmarket.plugin.siteapi.vo;

import java.util.ArrayList;
import java.util.List;

import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.SiteColumn;
import com.xnx3.wangmarket.plugin.siteapi.vo.bean.ColumnBean;
import com.xnx3.wangmarket.plugin.siteapi.vo.bean.NewsBean;

import net.sf.json.JSONObject;

/**
 * 栏目列表
 * @author 管雷鸣
 *
 */
public class ColumnListVO extends BaseVO{
	private List<ColumnBean> list;	//栏目属性列表

	public List<ColumnBean> getList() {
		return list;
	}

	public void setList(List<ColumnBean> list) {
		this.list = list;
	}

	
}
