package com.xnx3.wangmarket.plugin.siteapi.controller;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;

/**
 * 管理后台点进来跳转的入口，来决定跳转到index。html还是跳转到某个接口
 * <ignore>
 * @author 管雷鸣
 */
@Controller(value="SiteAPIPluginEntryController")
@RequestMapping("/plugin/siteapi/")
public class EntryController extends BasePluginController {
	
	/**
	 * 如果当前已授权，就进入具体接口。
	 * 如果未授权那么跳转到index.html，首页有关注公众号的广告
	 */
	@RequestMapping(value="entry.do")
	public String entry(HttpServletRequest request){
		if(com.xnx3.wangmarket.Authorization.copyright) {
			//未授权，要显示相关广告标识
			return redirect("http://api.zvo.cn/wangmarket/api/v5.6.0.20220428/index.html");
		}else {
			//已授权，不需要显示授权文字
			return redirect("http://api.zvo.cn/wangmarket/api/v5.6.0.20220428/plugin.siteapi.column.list.json.html");
		}
	}
	
	
}