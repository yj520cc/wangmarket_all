package com.xnx3.wangmarket.plugin.siteapi.vo;

import java.util.ArrayList;
import java.util.List;

import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.News;
import com.xnx3.wangmarket.plugin.siteapi.vo.bean.NewsBean;
import com.xnx3.wangmarket.plugin.siteapi.vo.bean.VarBean;

/**
 * 网站的全局变量列表
 * @author 管雷鸣
 *
 */
public class VarListVO extends BaseVO{
	private List<VarBean> list;	//全局变量列表

	public List<VarBean> getList() {
		return list;
	}

	public void setList(List<VarBean> list) {
		this.list = list;
	}
	
}
