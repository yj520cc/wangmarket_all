package com.xnx3.wangmarket.plugin.siteapi;

import com.xnx3.j2ee.pluginManage.PluginRegister;

/**
 * 网站开放API
 * @author 管雷鸣
 */
@PluginRegister(version = "1.0", menuHref = "/plugin/siteapi/entry.do", menuTitle = "网站开放API",intro="通过此插件，可以以api方式获取站点的文章、全局参数等", versionMin="5.3", applyToCMS = true)
public class Plugin{}