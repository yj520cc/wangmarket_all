package com.xnx3.wangmarket.plugin.siteapi.vo.bean;

import com.xnx3.wangmarket.admin.entity.SiteColumn;

/**
 * 栏目信息
 * @author 管雷鸣
 *
 */
public class ColumnBean {
	private Integer id;		//栏目的 id 编号
	private String name;	//栏目的名称
	private String icon;	//栏目图片图标url地址，可在模版中使用{siteColumn.icon}进行调用此图以显示
	private Integer rank;	//栏目间的排序
	private Short type;		//栏目类型 <br/> 7：信息列表 <br/> 8：独立页面
	private String codeName;		//栏目代码
	private String parentCodeName;	//父栏目代码
	private Integer listNum;		//列表中每页显示多少条
	private Short listRank;			//列表排序规则，当前栏目若是信息列表，信息列表的排序规则
	private String keywords;	//SEO关键字，限制50字以内
	private String description;		//SEO描述，限制200字以内
	
	public ColumnBean() {
	}
	
	public ColumnBean(SiteColumn siteColumn) {
		if(siteColumn == null){
			return;
		}
		
		this.id = siteColumn.getId();
		this.name = siteColumn.getName();
		this.icon = siteColumn.getIcon();
		this.rank = siteColumn.getRank();
		this.type = siteColumn.getType();
		this.codeName = siteColumn.getCodeName();
		this.parentCodeName = siteColumn.getParentCodeName();
		this.listNum = siteColumn.getListNum();
		this.listRank = siteColumn.getListRank();
		this.keywords = siteColumn.getKeywords();
		this.description = siteColumn.getDescription();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public Short getType() {
		return type;
	}
	public void setType(Short type) {
		this.type = type;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getParentCodeName() {
		return parentCodeName;
	}
	public void setParentCodeName(String parentCodeName) {
		this.parentCodeName = parentCodeName;
	}
	public Integer getListNum() {
		return listNum;
	}
	public void setListNum(Integer listNum) {
		this.listNum = listNum;
	}
	public Short getListRank() {
		return listRank;
	}
	public void setListRank(Short listRank) {
		this.listRank = listRank;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
