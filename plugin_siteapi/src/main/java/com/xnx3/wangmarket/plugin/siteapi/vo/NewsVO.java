package com.xnx3.wangmarket.plugin.siteapi.vo;

import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.News;
import com.xnx3.wangmarket.admin.entity.SiteColumn;
import com.xnx3.wangmarket.plugin.siteapi.vo.bean.ColumnBean;
import com.xnx3.wangmarket.plugin.siteapi.vo.bean.NewsBean;
import net.sf.json.JSONObject;

/**
 * 文章详情
 * @author 管雷鸣
 *
 */
public class NewsVO extends BaseVO{
	private NewsBean news;	//文章属性
	private JSONObject extend;	//文章自定义的扩展属性（输入模型中扩展的输入项）
	private String text;		//文章详细内容
	private ColumnBean column;	//当前文章所属的栏目
	
	public NewsBean getNews() {
		return news;
	}
	public void setNews(News news) {
		this.news = new NewsBean(news);
	}
	public JSONObject getExtend() {
		return extend;
	}
	public void setExtend(JSONObject extend) {
		this.extend = extend;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public ColumnBean getColumn() {
		return column;
	}
	public void setColumn(SiteColumn siteColumn) {
		this.column = new ColumnBean(siteColumn);
	}
	
}
