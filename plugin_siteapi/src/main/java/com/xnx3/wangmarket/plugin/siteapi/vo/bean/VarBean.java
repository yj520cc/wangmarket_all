package com.xnx3.wangmarket.plugin.siteapi.vo.bean;

import com.xnx3.json.JSONUtil;
import net.sf.json.JSONObject;

/**
 * 网站全局变量的某一个变量的bean
 * @author 管雷鸣
 *
 */
public class VarBean {
	
	/*
	    {
		    "description": "电脑端最底部显示的客服热线",
		    "name": "kefurexian",
		    "title": "客服热线",
		    "type": "text",
		    "value": "0086-536-8256000",
		    "valueItems": ""
		},
	 */
	
	private String name;	//相当于一个唯一id标识
	private String title;	//这一项的说明
	private String value;	//这一项的值
	
	public VarBean() {
	}
	
	public VarBean(JSONObject json) {
		this.name = JSONUtil.getString(json, "name");
		this.title = JSONUtil.getString(json, "title");
		this.value = JSONUtil.getString(json, "value");
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
