package com.xnx3.wangmarket.plugin.keyword.controller.siteadmin;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.util.SessionUtil;
import com.xnx3.wangmarket.plugin.keyword.entity.Keyword;
import com.xnx3.wangmarket.plugin.keyword.service.KeywordService;
/**
 * CMS模式下，输入模型相关操作
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/keyword/siteadmin/")
public class KeywordPluginController extends BasePluginController {
	@Resource
	private SqlService sqlService;
	@Resource
	private KeywordService keywordService;
	
	
	/**
	 * 网站管理后台的关键字替换列表
	 */
	@RequestMapping("list${url.suffix}")
	public String list(HttpServletRequest request, HttpServletResponse response, Model model){
		if(!haveSiteAuth()){
			return error(model, "请先登录");
		}
		Site site = SessionUtil.getSite();
		
		Sql sql = new Sql(request);
	    sql.setSearchTable("plugin_keyword");
	    //增加添加搜索字段。这里的搜索字段跟log表的字段名对应
//	    sql.setSearchColumn(new String[]{"word"});
	    sql.appendWhere("siteid = "+site.getId());
	    //查询log数据表的记录总条数
	    int count = sqlService.count("plugin_keyword", sql.getWhere());
	    //每页显示100条
	    Page page = new Page(count, 100, request);
	    //创建查询语句，只有SELECT、FROM，原生sql查询。其他的where、limit等会自动拼接
	    sql.setSelectFromAndPage("SELECT * FROM plugin_keyword", page);
	    
	    //当用户没有选择排序方式时，系统默认排序。
	    sql.setDefaultOrderBy("id DESC");
	    List<Keyword> list = sqlService.findBySql(sql, Keyword.class);
	    model.addAttribute("site", site);
	    model.addAttribute("list", list);
		return "plugin/keyword/siteadmin/list";
	}
	
	/**
	 * 添加、编辑
	 * @param id 若传入值，则是修改；若是0，则是新增
	 * @return
	 */
	@RequestMapping("edit${url.suffix}")
	public String edit(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam(value = "id", required = false , defaultValue="0") int id){
		if(!haveSiteAuth()){
			return error(model, "请先登录");
		}
		Site site = SessionUtil.getSite();
		
		Keyword keyword;
		if(id>0){
			//编辑
			keyword = sqlService.findById(Keyword.class, id);
			if(keyword == null){
				return error(model, "要修改的关键字不存在");
			}
			if(keyword.getSiteid() - site.getId() != 0){
				return error(model, "要修改的关键字不属于你，无法修改");
			}
		}else{
			keyword = new Keyword();
			keyword.setSiteid(site.getId());
		}
		
		model.addAttribute("keyword", keyword);
		return "plugin/keyword/siteadmin/edit";
	}
	
	/**
	 * 关键字保存
	 * @return
	 */
	@RequestMapping(value="save${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO save(Keyword keywordInput, HttpServletRequest request,Model model){
		if(!haveSiteAuth()){
			return error("请先登录");
		}
		Site site = SessionUtil.getSite();
		
		Keyword keyword;
		if(keywordInput.getId() == null || keywordInput.getId() == 0 ){
			//新增
			keyword = new Keyword();
			keyword.setSiteid(site.getId());
		}else{
			//修改
			keyword = sqlService.findById(Keyword.class, keywordInput.getId());
			if(keyword == null){
				return error("要修改的关键字不存在");
			}
			if(keyword.getSiteid() - site.getId() != 0){
				return error("要修改的关键字不属于你，无法修改");
			}
		}
		
//		keyword.setResultWord(keywordInput.getResultWord());
		keyword.setWord(keywordInput.getWord());
		sqlService.save(keyword);
		
		//刷新缓存
		keywordService.refreshKeywordListCache(site);
		
		return success();
	}
	
	/**
	 * 关键字删除
	 * @param id 要删除的关键字的id
	 * @return
	 */
	@RequestMapping(value="delete${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO delete(HttpServletRequest request,Model model,
			@RequestParam(value = "id", required = false , defaultValue="0") int id){
		if(!haveSiteAuth()){
			return error("请先登录");
		}
		if(id < 1){
			return error("您要删除哪个关键字呢？");
		}
		Site site = SessionUtil.getSite();
		
		Keyword keyword = sqlService.findById(Keyword.class, id);
		if(keyword == null){
			return error("要删除的关键字不存在");
		}
		if(keyword.getSiteid() - site.getId() != 0){
			return error("要修改的关键字不属于你，无法修改");
		}
		
		sqlService.delete(keyword);
		
		//刷新缓存
		keywordService.refreshKeywordListCache(site);
		
		return success();
	}
	
	//同步数据 keywords表到 site.keywords 中
	private void refreshSiteKeywords(Site siteOriginal) {
		//
		
		
		
	}
}