<%@page import="com.xnx3.wangmarket.admin.cache.Site"%>
<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../../iw/common/head.jsp">
	<jsp:param name="title" value="关键词列表"/>
</jsp:include>

<jsp:include page="../../../iw/common/list/formSearch_formStart.jsp" ></jsp:include>
	
	<button class="layui-btn layui-btn-normal" type="button" style="float: left; margin-left:10px; margin-bottom:7px;" onclick="edit(0,'');">
		<i class="layui-icon">&#xe67c;</i>
		添加关键词
	</button>
</form>	
<style>
.iw_table  {
	width: 500px;
    margin-top: 40px;
    margin-left: 10px;
}
.layui-btn.layui-btn-primary{
    height: 18px;
    line-height: 18px;
    padding: 0 5px;
    font-size: 13px;
}
</style>
<table class="layui-table iw_table">
	<thead>
		<tr>
			<th>关键词</th>
			<th>操作</th>
		</tr> 
	</thead>
	<tbody>
		<c:forEach items="${list}" var="item">
			<tr>
				<td>${item['word'] }</td>
				<td style="width:100px;">
					<botton class="layui-btn layui-btn-sm" onclick="edit(${item['id'] }, '${item['word'] }');" style="margin-left: 3px;">编辑</botton>
					<botton class="layui-btn layui-btn-sm" onclick="deleteKeyword(${item['id'] }, '${item['word'] }');" style="margin-left: 3px;">删除</botton>
				</td>
			</tr>
		</c:forEach>
  </tbody>
</table>

<div style="padding: 20px;color: gray;">
	<div>说明:</div>
	<div>内容管理中，编写的文章正文内容 (也就是模板中 {news.text} 所调出来的内容 )，如果其中有与之匹配的关键字，在生成网站时会自动给这些关键词加上超链接。</div>
	<div>加的超链接为： http://${site.bindDomain } <button onclick="updateBindDomain_info();" class="layui-btn layui-btn-primary">点此修改</button></div>
	<div>比如文章中有某段为“为农作物喷洒农药可有效降低病虫害”</div>
	<div>生成整站后，会变为 :</div>
	<div><pre>为农作物喷洒&lt;a href="http://${site.bindDomain }" class="keyword"&gt;农药&lt;a&gt;</pre>可有效降低病虫害</div>
</div>

<script>
//添加、编辑关键字
function edit(id, word){
	msg.popups({
	    url:'edit.do?id='+id,
	    width:'420px',
	    height:'163px',
	    padding:'1px'
	});
}

//删除关键字
function deleteKeyword(id, word){
	msg.confirm('确定要删除关键字“'+word+'”？', function(){	
		parent.msg.loading("删除中");    //显示“操作中”的等待提示
		$.post('delete.do?id='+id, function(data){
		    parent.msg.close();    //关闭“操作中”的等待提示
		    if(data.result == '1'){
		        parent.msg.success('删除成功');
		        window.location.reload();	//刷新当前页
		     }else if(data.result == '0'){
		         parent.msg.failure(data.info);
		     }else{
		         parent.msg.failure();
		     }
		});
		
	}, function(){
	});
}
//更改网站自己绑定的域名
function updateBindDomain_info(){
	parent.updateBindDomain();
}
</script>

<script src="/plugin/templateCenter/js/manage.js?v=<%=G.VERSION %>"></script>
<jsp:include page="../../../iw/common/foot.jsp"></jsp:include>                  