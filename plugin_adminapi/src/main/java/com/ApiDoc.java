package com;

import com.xnx3.doc.JavaDoc;

public class ApiDoc {
	public static void main(String[] args) {
		JavaDoc doc = new JavaDoc("com.xnx3.wangmarket.plugin.adminapi.controller");
		
		doc.name = "网市场云建站-后台API";
		doc.welcome = "<p>商家后台、网站管理后台的API接口</p>"
				+ "<p>这里只是列出了部分，如果有某些接口你想用但当前没有的， 可以反馈给我们。</p>"
				+ "<p style=\"width:100%; text-align:center;\"><img style=\"width:200px;\" src=\"http://cdn.weiunity.com/site/1893/templateimage/dca2d002c6ca42da943d648cc62b8fdc.jpg\" /></p>"
				+ "<p>微信公众号：wangmarket</p>";
		doc.domain = "http://localhost:8080";
		doc.version = "v5.6.0.20220426";
		
		doc.generateHtmlDoc();
	}
}
