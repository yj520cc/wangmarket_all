package com.xnx3.wangmarket.plugin.adminapi.system;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import com.xnx3.j2ee.Func;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.pluginManage.interfaces.SpringMVCInterceptorInterface;
import com.xnx3.j2ee.util.SessionUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.G;

/**
 * 是否已登录网站用户
 * @author 管雷鸣
 *
 */
public class SpringMVCInterceptorSiteLogin implements SpringMVCInterceptorInterface{

	@Override
	public List<String> pathPatterns() {
		List<String> list = new ArrayList<String>();
 		list.add("/plugin/adminapi/site/**");
 		return list;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String path = request.getServletPath();
		
		User user = SessionUtil.getUser();
		if(user == null) {
			//先登录
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.getWriter().write("{"
					+ "\"result\":\"" + BaseVO.NOT_LOGIN + "\","
					+ "\"info\":\"please login\""
				+ "}");
			return false;
		}
		
		if(!Func.isAuthorityBySpecific(user.getAuthority(), ""+G.ROLE_ID_SITE)){
			//无权使用
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.getWriter().write("{"
					+ "\"result\":\"" + BaseVO.FAILURE + "\","
					+ "\"info\":\"403, your not site user\""
				+ "}");
			return false;
		}
		
		return SpringMVCInterceptorInterface.super.preHandle(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		SpringMVCInterceptorInterface.super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		SpringMVCInterceptorInterface.super.afterCompletion(request, response, handler, ex);
	}
	
}
