package com.xnx3.wangmarket.plugin.adminapi.controller.agency;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.StringUtil;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.util.SpringUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;

/**
 * 代理后台-网站管理
 * @author 管雷鸣
 */
@Controller(value="AdminApiPluginAgencySiteController")
@RequestMapping("/plugin/adminapi/agency/site/")
public class SiteController extends BasePluginController {
	@Resource
	private SqlCacheService sqlCacheService;
	
	/**
	 * 开通网站
	 * @author 管雷鸣
	 * @param token 当前操作用户的登录标识 <required>
	 * 				<p>可通过 <a href="plugin.adminapi.login.json.html">/plugin/adminapi/login.json</a> 取得 </p>
	 * @param siteName 要开通的网站的名字，比如叫某个要做网站的公司的名字 <example=潍坊雷鸣云网络科技有限公司>
	 * @param username 登录的用户名，用于登录网站管理后台使用 <example=guanleiming>
	 * @param password 登录的密码，用户登录网站管理后台使用 <example=guanleiming>
	 * @return 若开通成功，则当前开通的网站的id唯一标识会存在于info中返回
	 */
	@RequestMapping(value="create.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO create(HttpServletRequest request, 
			@RequestParam(required = true , defaultValue="") String siteName,
			@RequestParam(required = true , defaultValue="") String username,
			@RequestParam(required = true , defaultValue="") String password){
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		
		BaseVO vo = ((com.xnx3.wangmarket.agencyadmin.controller.AgencyUserController)SpringUtil.getBean("agencyUserController")).addSubmit(request, user, (short)3,siteName, "", "", "", "", "", "", "");
		if(vo.getResult() - BaseVO.SUCCESS == 0) {
			//开通成功，取得网站的id
			vo.setInfo(StringUtil.subString(vo.getInfo(), null, "_"));
		}
		
		return vo;
	}
	

	/**
	 * 给网站更改登录密码
	 * @author 管雷鸣
	 * @param token 当前操作用户的登录标识 <required>
	 * 				<p>可通过 <a href="plugin.adminapi.login.json.html">/plugin/adminapi/login.json</a> 取得 </p>
	 * @param siteid 要更改密码网站id，网站唯一标识 <example=219>
	 * @param newPassword 要更改的新密码 <example=123>
	 * @return 操作结果
	 */
	@RequestMapping(value="updatePasword.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO updatePasword(HttpServletRequest request,
			@RequestParam(required = true) int siteid,
			@RequestParam(required = true) String newPassword){
		Site site = sqlCacheService.findById(Site.class, siteid);
		if(site == null) {
			return error("要更改密码的站点不存在");
		}
		
		return ((com.xnx3.wangmarket.agencyadmin.controller.AgencyUserController)SpringUtil.getBean("agencyUserController")).siteUpdatePassword(request, site.getUserid(), newPassword);
	}
	

	/**
	 * 暂停/冻结 网站
	 * <p>冻结后，此网站的网站管理后台将无法登录，网站也无法打开</p>
	 * @author 管雷鸣
	 * @param token 当前操作用户的登录标识 <required>
	 * 				<p>可通过 <a href="plugin.adminapi.login.json.html">/plugin/adminapi/login.json</a> 取得 </p>
	 * @param siteid 要冻结的网站的id，唯一标识 <example=219>
	 * @return 操作结果
	 */
	@RequestMapping(value="freeze.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO freeze(HttpServletRequest request,
			@RequestParam(required = true) int siteid){
		return ((com.xnx3.wangmarket.agencyadmin.controller.AgencyUserController)SpringUtil.getBean("agencyUserController")).sitePause(request, siteid);
	}
	
	/**
	 * 解除 暂停/冻结 的网站
	 * <p>解除冻结后，网站将恢复正常访问，管理后台也能正常登录使用</p>
	 * @author 管雷鸣
	 * @param token 当前操作用户的登录标识 <required>
	 * 				<p>可通过 <a href="plugin.adminapi.login.json.html">/plugin/adminapi/login.json</a> 取得 </p>
	 * @param siteid 要解除冻结的网站的id，唯一标识 <example=219>
	 * @return 操作结果
	 */
	@RequestMapping(value="unfreeze.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO unfreeze(HttpServletRequest request,
			@RequestParam(required = true) int siteid){
		return ((com.xnx3.wangmarket.agencyadmin.controller.AgencyUserController)SpringUtil.getBean("agencyUserController")).siteRemovePause(request, siteid);
	}
	
	/**
	 * 延长使用时间
	 * <p>延长网站的使用期限，使用时长</p>
	 * @author 管雷鸣
	 * @param token 当前操作用户的登录标识 <required>
	 * 				<p>可通过 <a href="plugin.adminapi.login.json.html">/plugin/adminapi/login.json</a> 取得 </p>
	 * @param siteid 要延长的站点的id，唯一标识 <example=219>
	 * @param year 要延长几年，可填写1～10，最大可往后延长10年 
	 * @return 操作结果
	 */
	@RequestMapping(value="lengthen.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO lengthen(HttpServletRequest request, Model model,
			@RequestParam(required = true) int siteid,
			@RequestParam(required = true, defaultValue = "1") int year){
		return ((com.xnx3.wangmarket.agencyadmin.controller.AgencyUserController)SpringUtil.getBean("agencyUserController")).siteXuFie(request, model, siteid, year);
	}
}