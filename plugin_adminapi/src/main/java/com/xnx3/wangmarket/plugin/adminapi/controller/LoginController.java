package com.xnx3.wangmarket.plugin.adminapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.ApplicationPropertiesUtil;
import com.xnx3.j2ee.util.IpUtil;
import com.xnx3.CacheUtil;
import com.xnx3.DateUtil;
import com.xnx3.exception.NotReturnValueException;
import com.xnx3.j2ee.Func;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.G;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.entity.SiteUser;
import com.xnx3.wangmarket.admin.util.TemplateAdminMenu.TemplateMenuEnum;
import com.xnx3.wangmarket.agencyadmin.entity.Agency;
import com.xnx3.wangmarket.agencyadmin.entity.AgencyData;
import com.xnx3.wangmarket.agencyadmin.util.SessionUtil;
import com.xnx3.wangmarket.plugin.adminapi.vo.LoginVO;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;

/**
 * 登录 
 * @author 管雷鸣
 */
@Controller(value="AdminApiPluginLoginController")
@RequestMapping("/plugin/adminapi/")
public class LoginController extends BasePluginController {
	@Resource
	private UserService userService;
	@Resource
	private SqlService sqlService;
	
	/**
	 * 使用用户名加密码进行登录 <login>
	 * @author 管雷鸣
	 * @param username 登录的用户名或邮箱 <required> <example=wangzhan>
	 * @param password 登录密码 <required> <example=wangzhan>
	 * @return 登录结果
	 */
	@RequestMapping(value="login.json", method = RequestMethod.POST)
	@ResponseBody
	public LoginVO login(HttpServletRequest request){
		LoginVO vo = new LoginVO();
		String wmLogin = ApplicationPropertiesUtil.getProperty("wm.login");
		if(wmLogin != null && wmLogin.equalsIgnoreCase("false")) {
			vo.setBaseVO(LoginVO.FAILURE, "此接口不允许被使用！您可修改application.properties中的wm.login=true ，重启项目，即可使用本接口");
			return vo;
		}
		
		//获取用户当前ip
		String ip = IpUtil.getIpAddress(request);
		//判断是否已被拦截控制不允许登录
		Object cacheObj = CacheUtil.get(ip);
		int ipCishu = 0;	//这个ip当前第几次请求
		if(cacheObj != null) {
			ipCishu = (int) cacheObj;
		}
		if(ipCishu > 4) {
			vo.setBaseVO(LoginVO.FAILURE, "您当前尝试过于频繁，请一小时后再试");
			return vo;
		}
		
		BaseVO baseVO =  userService.loginByUsernameAndPassword(request);
		vo.setBaseVO(baseVO);
		if(baseVO.getResult() == BaseVO.SUCCESS){
			//得到当前登录的用户的信息
			User user = getUser();

			//得到上级的代理信息
			/*
			 * 如果当前用户是代理，那这里是他上级代理的user.id
			 * 如果当前用户是网站管理元，那这里是给他开通网站的上级代理user.id
			 * 如果当前用户只是网站管理员开通的一个子用户，管理网站某个固定功能的，那这里还是这个网站的上级代理user.id
			 */
			int parentAgencyUserid = 0;
			Agency parentAgency = null;	//上级代理的信息
			//当前时间
			int currentTime = DateUtil.timeForUnix10();	
			
			//判断权限
			if(Func.isAuthorityBySpecific(user.getAuthority(), G.ROLE_ID_SITE+"")){
				//网站后台
				
				Site site = null; //当前管理的网站
				
				//得到当前用户，在网市场中，user的扩展表 site_user 的信息
				SiteUser siteUser = sqlService.findById(SiteUser.class, user.getId());
				if(siteUser == null){
					//是网站管理员
					
					siteUser = new SiteUser();
					
					//如果不是子账户，那直接获取user.referrerid 即可
					parentAgencyUserid = user.getReferrerid();
					
					//是网站管理者，拥有所有权限的
					//得到当前用户站点的相关信息，加入userBean，以存入Session缓存起来
					site = sqlService.findAloneBySqlQuery("SELECT * FROM site WHERE userid = "+getUserId()+" ORDER BY id DESC", Site.class);
					//将拥有所有功能的管理权限，将功能菜单全部遍历出来，赋予这个用户
					Map<String, String> menuMap = new HashMap<String, String>();
					for (TemplateMenuEnum e : TemplateMenuEnum.values()) {
						menuMap.put(e.id, "1");
					}
					SessionUtil.setSiteMenuRole(menuMap);
				}else {
					//是网站管理员开通的子用户，那么需要查询 一下网站管理员的用户信息
					
					//当前暂时不允许子用户登录
					if(true) {
						vo.setBaseVO(BaseVO.FAILURE, "当前是网站子用户，请用网站的管理者账号登录");
						return vo;
					}
					
					User siteAdminUser = sqlService.findById(User.class, user.getReferrerid());
					if(siteAdminUser == null){
						vo.setBaseVO(BaseVO.FAILURE, "登陆失败，未发现您所属网站的管理者");
						return vo;
					}
					parentAgencyUserid = siteAdminUser.getReferrerid();
					
					//是网站管理子用户.既然是有子账户了，那肯定子账户插件是使用了，也就可以进行一下操作了
					site = sqlService.findById(Site.class, siteUser.getSiteid());
					//网站子用户，需要读取他拥有哪些权限，也缓存起来
					List<Map<String, Object>> menuList = sqlService.findMapBySqlQuery("SELECT menu FROM plugin_sitesubaccount_user_role WHERE userid = "+user.getId());
					Map<String, String> menuMap = new HashMap<String, String>();
					for (int i = 0; i < menuList.size(); i++) {
						Map<String, Object> menu = menuList.get(i);
						if(menu.get("menu") != null){
							String m = (String) menu.get("menu");
							menuMap.put(m, "1");
						}
					}
					SessionUtil.setSiteMenuRole(menuMap);
				}
				
				if(site == null){
					vo.setResult(BaseVO.FAILURE);
					vo.setInfo("出错！所管理的网站不存在！");
					return vo;
				}
				//将所管理的网站加入session缓存
				SessionUtil.setSite(site);
				
				//得到上级代理的信息
				parentAgency = sqlService.findAloneBySqlQuery("SELECT * FROM agency WHERE userid = " + parentAgencyUserid, Agency.class);
				vo.setParentAgency(parentAgency);
				
				//判断网站用户是否是已过期，使用期满，将无法使用
				if(site != null && site.getExpiretime() != null && site.getExpiretime() < currentTime){
					//您的网站已到期。若要继续使用，请续费
					try {
						vo.setBaseVO(BaseVO.FAILURE, "您的网站已于 "+DateUtil.dateFormat(site.getExpiretime(), "yyyy-MM-dd")+" 到期");
					} catch (NotReturnValueException e) {
						e.printStackTrace();
					}
					SessionUtil.logout();	//退出登录，销毁session
					return vo;
				}
				
				//缓存进session
				com.xnx3.wangmarket.admin.util.SessionUtil.setSiteUser(siteUser);
				
				ActionLogUtil.insertUpdateDatabase(request, "用户名密码模式登录成功","进入网站管理后台");
			}else if(Func.isAuthorityBySpecific(user.getAuthority(), G.ROLE_ID_AGENCY+"")){
				//代理后台
				
				//得到当前用户代理的相关信息，加入userBean，以存入Session缓存起来
				Agency myAgency = sqlService.findAloneBySqlQuery("SELECT * FROM agency WHERE userid = " + getUserId(), Agency.class);
				SessionUtil.setAgency(myAgency);
				if(myAgency != null){
					//得到当前代理用户的变长表信息
					AgencyData myAgencyData = sqlService.findAloneBySqlQuery("SELECT * FROM agency_data WHERE id = " + myAgency.getId(), AgencyData.class);
					SessionUtil.setAgencyData(myAgencyData);
				}
				
				//判断当前代理是否是已过期，使用期满，将无法登录
				if (myAgency != null && myAgency.getExpiretime() != null && myAgency.getExpiretime() < currentTime){
					//您的代理资格已到期。若要继续使用，请联系您的上级
					try {
						vo.setBaseVO(BaseVO.FAILURE, "您的代理资格已于 "+DateUtil.dateFormat(myAgency.getExpiretime(), "yyyy-MM-dd")+" 到期");
					} catch (NotReturnValueException e) {
						e.printStackTrace();
					}
					SessionUtil.logout();
					return vo;
				}
				
				ActionLogUtil.insertUpdateDatabase(request, "用户名密码模式登录成功","进入代理后台, "+myAgency.toString());
			}else {
				//不是以上两者
				ActionLogUtil.insert(request, "当前登录账号并非代理账号或网站管理员账号","username:"+user.getUsername());
				vo.setBaseVO(LoginVO.FAILURE, "当前登录账号并非代理账号或网站管理员账号");
				return vo;
			}
			
			/***** 得到上级代理的信息 *****/
			SessionUtil.setParentAgency(parentAgency);
			if(parentAgency != null){
				//得到上级代理的变长表信息
				AgencyData parentAgencyData = sqlService.findAloneBySqlQuery("SELECT * FROM agency_data WHERE id = " + parentAgency.getId(), AgencyData.class);
				SessionUtil.setParentAgencyData(parentAgencyData);
			}
			
			
			ActionLogUtil.insert(request, "用户名密码模式登录成功");
			
			
			//将sessionid加入vo返回
			HttpSession session = request.getSession();
			vo.setToken(session.getId());
			
			//加入user信息
			vo.setUser(getUser());
			
			vo.setBaseVO(BaseVO.SUCCESS, "登录成功");
			return vo;
		}else {
			CacheUtil.set(ip,ipCishu++, 3600);	//记录错误次数
			vo.setBaseVO(LoginVO.FAILURE, baseVO.getInfo());
			return vo;
		}
	}
	

	/**
	 * 登录并重定向进入网站管理后台
	 * <p>这个跟login.json接口的区别是 login.json 返回的是json响应，而本接口是进行重定向跳转。</p>
	 * <p>应用场景是用户可以直接访问本接口，就能自动跳转到网站管理后台页面。主要用于你原本系统整合上网市场作为其中一个建站模块，通过这个跳转，在你原本系统上可以快速切换进入网站管理后台。网站管理后台的界面、布局、显示的功能，您可以自由进行定制扩展。</p>
	 * <p>注意，这个是在浏览器直接打开的，并非通过ajax请求的! 你直接点下面的请求按钮测试不行</p>
	 * @author 管雷鸣
	 * @param username 登录的用户名或邮箱 <required> <example=wangzhan>
	 * @param password 登录密码 <required> <example=wangzhan>
	 * @return 如果登录成功，则会自动跳转到网站管理后台页面
	 */
	@RequestMapping(value="loginRedirectSiteadmin.do")
	public String loginRedirectSiteadmin(HttpServletRequest request, Model model){
		LoginVO vo = login(request);
		if(vo.getResult() - BaseVO.FAILURE == 0) {
			return error(model, vo.getInfo());
		}
		
		return redirect("template/index.do?token="+vo.getToken());
	}
	
	/**
	 * 退出登录
	 * @author 管雷鸣
	 * @param token 当前操作用户的登录标识 <required>
	 * 				<p>可通过 <a href="plugin.adminapi.login.json.html">/plugin/adminapi/login.json</a> 取得 </p>
	 */
	@RequestMapping(value="logout.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO logout(HttpServletRequest request,Model model){
		SessionUtil.logout();
		return success();
	}
}
