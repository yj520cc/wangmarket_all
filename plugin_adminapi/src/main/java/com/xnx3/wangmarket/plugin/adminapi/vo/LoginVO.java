package com.xnx3.wangmarket.plugin.adminapi.vo;

import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.system.responseBody.ResponseBodyManage;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.agencyadmin.entity.Agency;

/**
 * 登陆接口的返回值
 * @author 管雷鸣
 */
@ResponseBodyManage(ignoreField = {"password","currency","nickname","authority","money", "head","isfreeze","sex","sign","freezemoney","referrerid","salt","version","email","state","allowCreateSubAgency","expiretime","parentId","allowSubAgencyCreateSub","siteSize","addtime","userid"}, nullSetDefaultValue = true)
public class LoginVO extends BaseVO {
	private String token;	//sessionid
	private User user;		//登录的用户信息
	private Agency parentAgency;	//当前用户的上级代理的信息。

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Agency getParentAgency() {
		return parentAgency;
	}
	public void setParentAgency(Agency parentAgency) {
		this.parentAgency = parentAgency;
	}
	
}
