package com.xnx3.wangmarket.plugin.adminapi.controller.site;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.xnx3.j2ee.util.SpringUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.j2ee.vo.UploadFileVO;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;

/**
 * 网站-基本功能
 * @author 管雷鸣
 */
@Controller(value="AdminApiPluginTemplateController")
@RequestMapping("/plugin/adminapi/site/")
public class TemplateController extends BasePluginController {
	
	/**
	 * 生成整站
	 * @author 管雷鸣
	 * @param token 当前操作用户的登录标识 <required>
	 * 				<p>可通过 <a href="plugin.adminapi.login.json.html">/plugin/adminapi/login.json</a> 取得 </p>
	 */
	@RequestMapping(value="generate.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO generate(Model model,HttpServletRequest request){
		return ((com.xnx3.wangmarket.admin.controller.TemplateController)SpringUtil.getBean("templateController")).refreshForTemplate(model, request);
	}
	
	/**
	 * 上传图片接口
	 * @author 管雷鸣
	 * @param token 当前操作用户的登录标识 <required>
	 * 				<p>可通过 <a href="plugin.adminapi.login.json.html">/plugin/adminapi/login.json</a> 取得 </p>
	 * @param image 上传的图片
	 */
	@RequestMapping(value="uploadImage.json", method = RequestMethod.POST)
	@ResponseBody
	public UploadFileVO uploadImage(HttpServletRequest request, 
			@RequestParam(value="image") MultipartFile image){
		return ((com.xnx3.wangmarket.admin.controller.TemplateController)SpringUtil.getBean("templateController")).uploadImage(request, image);
	}
}