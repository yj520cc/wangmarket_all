package com.xnx3.wangmarket.plugin.adminapi.controller.site;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.util.SpringUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.News;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;

/**
 * 网站-内容管理
 * @author 管雷鸣
 */
@Controller(value="AdminApiPluginNewsController")
@RequestMapping("/plugin/adminapi/site/news/")
public class NewsController extends BasePluginController {
	
	/**
	 * 发布/修改 文章
	 * <p><b>注意:</b>有的用户通过此接口，用采集器来采集文章，大量导入文章，针对这种场景，我们强烈建议你不要使用云存储的安装方式，采用单台服务器安装的方式，附件都存在于服务器本身，这样当文章的量大时，生成html静态页面时速度会有几何倍数的提高</p>
	 * @author 管雷鸣
	 * @param token 当前操作用户的登录标识 <required>
	 * 				<p>可通过 <a href="plugin.adminapi.login.json.html">/plugin/adminapi/login.json</a> 取得 </p>
	 * @param id 要修改的是哪篇文章
	 * 			<ul>
	 * 				<li>如果是发布文章，此处不用填写，则是代表发布文章</li>
	 * 				<li>如果是修改文章，这里必须有值，代表修改的是哪篇文章</li>
	 * 			</ul>
	 * @param cid 要发布的文章属于哪个栏目。 <example=2>
	 * 			<ul>
	 * 				<li>如果是发布文章，此处必须填写，这里是对应的栏目的id </li>
	 * 				<li>如果修改文章，此处不用传</li>
	 * 			<ul>
	 * @param title 文章标题 <example=我是文章标题>
	 * @param titlepic 文章图片，传入的是图片url。可先用 <a href="plugin.adminapi.site.uploadImage.json.html">/plugin/adminapi/site/uploadImage.json</a> 将图片上传 <example=http://res.zvo.cn/default.jpg>
	 * @param intro 文章简介信息 <example=我是文章的简介>
	 * @param text 文章的正文，详细内容，也就是富文本编辑器编辑的，包含html的内容 <example=我是正文内容>
	 * @return 是否保存成功的结果
	 */
	@RequestMapping(value="save.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO saveNews(HttpServletRequest request, 
			@RequestParam(required = false , defaultValue="") Integer id,
			@RequestParam(required = false , defaultValue="") Integer cid,
			@RequestParam(required = true , defaultValue="") String title,
			@RequestParam(required = false , defaultValue="") String titlepic,
			@RequestParam(required = false , defaultValue="") String intro,
			@RequestParam(required = false , defaultValue="") String text){
		
		News news = new News();
		news.setCid(cid);
		news.setId(id);
		news.setIntro(intro);
		news.setTitle(title);
		news.setTitlepic(titlepic);
		
		return ((com.xnx3.wangmarket.admin.controller.NewsController)SpringUtil.getBean("newsController")).saveNews(news, text, request);
	}
}