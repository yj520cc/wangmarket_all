package com.xnx3.wangmarket.plugin.adminapi;

import com.xnx3.j2ee.pluginManage.PluginRegister;

/**
 * 代理后台、网站管理后台的某些开放api接口
 * @author 管雷鸣
 */
@PluginRegister(menuTitle = "后台开放API", intro="代理后台、网站管理后台的某些开放api接口", menuHref="/plugin/adminapi/entry.do", applyToCMS=true, applyToAgency = true, version="1.0", versionMin="5.6")
public class Plugin{
}