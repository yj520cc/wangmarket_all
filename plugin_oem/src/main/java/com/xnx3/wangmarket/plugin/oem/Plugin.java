package com.xnx3.wangmarket.plugin.oem;

import com.xnx3.j2ee.pluginManage.PluginRegister;

/**
 * OEM定制
 * @author 管雷鸣
 */
@PluginRegister(version = "1.0", menuTitle = "OEM定制", menuHref="/plugin/oem/index.jsp", applyToSuperAdmin=true, versionMin="5.3")
public class Plugin{}