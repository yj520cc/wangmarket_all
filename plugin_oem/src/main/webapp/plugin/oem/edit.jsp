<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 引入通用头部 -->
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="插件开发学习DEMO" />
</jsp:include>

<form class="layui-form" style="padding-top:35px; margin-bottom: 10px; padding-right:35px;">
	<input type="text" name="name" style='display:none' value="<%=request.getParameter("name") %>" />
	 <div class="layui-form-item">
		<label class="layui-form-label" id="label_columnName">值</label>
		<div class="layui-input-block">
			<input type="text"  name="value" required lay-verify="required" autocomplete="off" class="layui-input" value="<%=SystemUtil.get(request.getParameter("name"))%>">
		</div>
	</div>

   	<div class="layui-form-item" style="padding-top:15px;">
		<div class="layui-input-block">
			<button class="layui-btn" lay-submit="" lay-filter="demo1">保存修改</button>
		</div>
	</div>
</form>

<script>
//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
parent.layer.iframeAuto(index);

layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form;
  //监听提交
  form.on('submit(demo1)', function(data){
		parent.parent.msg.loading("保存中");
		var d=$("form").serialize();
        $.post("/admin/system/variableSave.do", d, function (result) { 
        	parent.parent.msg.close();
        	var obj = JSON.parse(result);
        	if(obj.result == '1'){
        		parent.parent.msg.success("操作成功")
        		parent.layer.close(index);
        		parent.location.reload();	//刷新父窗口
        	}else if(obj.result == '0'){
        		parent.parent.msg.failure(obj.info);
        	}else{
        		parent.parent.msg.failure(result);
        	}
         }, "text");
		
    return false;
  });
  
});  
</script>

<!-- 引入通用尾部 -->
<jsp:include page="/wm/common/foot.jsp"></jsp:include>
