<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 引入通用头部 -->
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="插件开发学习DEMO" />
</jsp:include>
<style>
.layui-form-label {
	width: 100px;
	margin-left: -20px;
}
</style>

<%
	String SELECT_TEMPLATE_YINDAO_USE = SystemUtil.get("SELECT_TEMPLATE_YINDAO_USE");
	if(SELECT_TEMPLATE_YINDAO_USE == null || SELECT_TEMPLATE_YINDAO_USE.trim().equals("")){
		SELECT_TEMPLATE_YINDAO_USE = "lmyglm1";
	}
	if(SELECT_TEMPLATE_YINDAO_USE.equals("0")){
		SELECT_TEMPLATE_YINDAO_USE = "0  (不使用引导)";
	}

%>

<%-- <input value="<%=SystemUtil.get("SITE_NAME")%>" /> --%>

<form id="form" class="layui-form" action=""
	style="padding: 25px; padding-top: 35px; margin-bottom: 10px;">
	<input type="hidden" id="id" name="id">
	<div class="layui-form-item" id="xnx3_editMode">
		<label class="layui-form-label" id="columnEditMode">系统名字：</label>
		<div class="layui-input-block">
			<input type="text" readonly="readonly" id="edit"
				value="<%=SystemUtil.get("SITE_NAME")%>" class="layui-input"
				onclick="showEdit( '系统名字','SITE_NAME');">
		</div>
		<div class="explain"
			style="font-size: 12px; color: gray; padding-top: 3px; padding-left: 110px;">
			本建站系统的名字。如在登录页面显示等</div>
	</div>

	<div class="layui-form-item" id="xnx3_editMode">
		<label class="layui-form-label" id="columnEditMode">使用入门视频：</label>
		<div class="layui-input-block">
			<input type="text" readonly="readonly"
				value="<%=SystemUtil.get("SITEUSER_FIRST_USE_EXPLAIN_URL")%>"
				class="layui-input"
				onclick="showEdit('使用入门视频', 'SITEUSER_FIRST_USE_EXPLAIN_URL');">
		</div>
		<div class="explain"
			style="font-size: 12px; color: gray; padding-top: 3px; padding-left: 110px;">
			1. 网站建站用户第一天登陆网站管理后台时，在欢迎页面会自动通过iframe引入入门使用说明的页面URL <br /> 2.
			网站管理后台左侧菜单的【帮助说明】-【使用入门】所打开的页面URL

		</div>
	</div>

	<div class="layui-form-item" id="xnx3_editMode">
		<label class="layui-form-label" id="columnEditMode">模板开发说明：</label>
		<div class="layui-input-block">
			<input type="text" readonly="readonly"
				value="<%=SystemUtil.get("SITE_TEMPLATE_DEVELOP_URL")%>"
				class="layui-input"
				onclick="showEdit('模板开发说明', 'SITE_TEMPLATE_DEVELOP_URL');">
		</div>
		<div class="explain"
			style="font-size: 12px; color: gray; padding-top: 3px; padding-left: 110px;">
			网站管理后台左侧菜单的【帮助说明】-【模板开发】所打开的页面URL</div>
	</div>

	<div class="layui-form-item" id="xnx3_editMode">
		<label class="layui-form-label" id="columnEditMode">绑定域名示例：</label>
		<div class="layui-input-block">
			<input type="text" readonly="readonly"
				value="<%=SystemUtil.get("BIND_DOMAIN_DEMO")%>" class="layui-input"
				onclick="showEdit('绑定域名示例', 'BIND_DOMAIN_DEMO');">
		</div>
		<div class="explain"
			style="font-size: 12px; color: gray; padding-top: 3px; padding-left: 110px;">
			网站管理后台左侧菜单的【系统设置】-【绑定域名】，所弹出的绑定域名弹出窗口，第一步的填写格式示例 <a href="javascript:showBindDomain();">[点此查看]</a></div>
	</div>
	
	<div class="layui-form-item" id="xnx3_editMode">
		<label class="layui-form-label" id="columnEditMode">引导选择模板：</label>
		<div class="layui-input-block">
			<input type="text" readonly="readonly"
				value="<%=SELECT_TEMPLATE_YINDAO_USE %>" class="layui-input"
				onclick="showEdit('引导选择模板', 'SELECT_TEMPLATE_YINDAO_USE');">
		</div>
		<div class="explain"
			style="font-size: 12px; color: gray; padding-top: 3px; padding-left: 110px;">
			创建一个新网站后，第一次登录网站管理后台，将会进入选择模板页面，默认系统会出现引导，引导用户选择 lmyglm1 的模板，这里你可以控制引导选择模板是哪个。<br/>
			1. 这里可以填写某个模板编号，比如填写 lmyglm1 ，那么在选择模板引导时，引导会指引选择这个编号的模板。<br>
			2. 这里可以填写 0，注意是一个 0 ，那么表示将不出现选择模板的引导提示。		
		</div>
	</div>
	
	

</form>
<script type="text/javascript">
	//自适应弹出层大小
	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引

	//弹出编辑框
	function showEdit(title, name) {
		layer.open({
			type : 2,
			title : '修改 ' + title,
			area : [ '500px', '650px' ],
			shadeClose : true, //开启遮罩关闭
			content : '/plugin/oem/edit.jsp?name=' + name
		});
	}
	
	/**
	 * 显示绑定域名的图片示例
	 */
	function showBindDomain(){
		msg.popups({
		    url:'/plugin/oem/bind_domain.png',
		    padding:'1px',
		    height:'436px'
		});
	}
	
	/**
	 * 弹出修改窗口，修改 agency 的某个信息
	 * @param name agency的数据表某列名字，如name、qq、address
	 * @param description 描述，弹出窗口的说明信息
	 * @param oldValue 旧的值，当前的值
	 */
	 /* function popItem(name, description,oldValue){
		alert(name)
		layer.prompt({
		  formType: 2,
		  value: oldValue,
		  title: description,
		  area: ['400px', '100px'] //自定义文本域宽高
		}, function(value, index, elem){
		  layer.close(index);
		  console.log(value)
		  parent.iw.loading('修改中');
		  $.post("/admin/system/variableSave.do",{"name":name, "value":value } , function (result) { 
	        	parent.parent.msg.close();
	        	var obj = JSON.parse(result);
	        	if(obj.result == '1'){
	        		parent.parent.msg.success("操作成功")
	        		parent.location.reload();	//刷新父窗口
	        	}else if(obj.result == '0'){
	        		parent.parent.msg.failure(obj.info);
	        	}else{
	        		parent.parent.msg.failure(result);
	        	}
	         }, "text");
		  
		});
	} */
</script>

<!-- 引入通用尾部 -->
<jsp:include page="/wm/common/foot.jsp"></jsp:include>
