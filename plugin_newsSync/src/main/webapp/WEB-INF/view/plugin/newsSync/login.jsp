<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<jsp:include page="../../iw/common/head.jsp">
	<jsp:param name="title" value="登录"/>
</jsp:include>

<style>
.myForm{
	margin: 0 auto;
    width: 495px;
    height: 360px;
    border-width: 1px 1px 1px 1px;
    padding: 0px;
    border-radius: 20px;
    overflow: hidden;
    -webkit-box-shadow: 0 0 10px #e2e2e2;
    -moz-box-shadow: 0 0 10px #e2e2e2;
    box-shadow: 0 0 10px #e2e2e2;
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -248px;
    margin-top: -181px;
}

@media screen and (max-width:600px){
	.myForm{
		margin: 0 auto;
	    width: 100%;
	    height: 100%;
	    border-width: 0px;
	    padding: 0px;
	    border-radius: 0px;
	    overflow: auto;
	    -webkit-box-shadow: 0 0 0px #e2e2e2;
	    -moz-box-shadow: 0 0 0px #e2e2e2;
	    box-shadow: 0 0 0px #e2e2e2;
	    position: static;
	    left: 0px;
	    top: 0px;
	    margin-left: 0px;
	    margin-top: 0px;
	}
}

.touming{
	background: rgba(0,190,150,0.1) none repeat scroll !important;
}
.baisetouming{
	background: rgba(250,250,250,0.1) none repeat scroll !important;
}
</style>

<form class="layui-form layui-elem-quote layui-quote-nm myForm">
  <div class="layui-form-item touming" style="height: 40px;margin-top: 20px;background-color: #eeeeee;line-height: 70px;text-align: center;font-size: 25px;color: #3F4056;">
    请输入A网站的账号密码以验证
  </div>
  <div style="padding: 30px 50px 40px 0px;">
  	<div class="layui-form-item">
	    <label class="layui-form-label">用户名</label>
	    <div class="layui-input-block">
	      <input type="text" name="username" required  lay-verify="required" value="" placeholder="请输入 用户名/邮箱" autocomplete="off" class="layui-input baisetouming">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label"> 密 码&nbsp;&nbsp; </label>
	    <div class="layui-input-block">
	      <input type="password" name="password" required lay-verify="required" value="" placeholder="请输入密码" autocomplete="off" class="layui-input baisetouming">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label"> 验证码 </label>
	    <div class="layui-input-inline">
	      <input type="text" name="code" required lay-verify="required" placeholder="请输入右侧验证码" autocomplete="off" class="layui-input baisetouming">
	    </div>
	    <div class="layui-form-mid layui-word-aux" style="padding-top: 3px;padding-bottom: 0px;"><img id="code" src="/captcha.do" onclick="reloadCode();" style="height: 29px;width: 110px; cursor: pointer;" /></div>
	  </div>
	  <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button class="layui-btn" lay-submit lay-filter="formDemo" style="opacity:0.6; margin-right: 50px;">绑定</button>
	      <button type="reset" class="layui-btn layui-btn-primary baisetouming" style="width: 90px;">重置</button>
	    </div>
	  </div>
  </div>
</form>
 
<script>
//Demo
layui.use('form', function(){
  var form = layui.form;
  
  //监听提交
  form.on('submit(formDemo)', function(data){
	parent.parent.iw.loading("验证中...");
    var d=$("form").serialize();
	$.post("loginSubmit.do", d, function (result) {
		parent.parent.iw.loadClose();
       	var obj = JSON.parse(result);
       	if(obj.result == '1'){
       		parent.parent.iw.msgSuccess("绑定成功！");
       		window.parent.location.reload();
       	}else if(obj.result == '0'){
       		//登陆失败
       		reloadCode();
       		layer.msg(obj.info, {shade: 0.3})
       	}else if(obj.result == '11'){
       		//网站已过期。弹出提示
       		reloadCode();
       		layer.open({
			  title: '到期提示'
			  ,content: obj.info
			});     
       	}else{
       		reloadCode();
       		layer.msg(result, {shade: 0.3})
       	}
	}, "text");
    return false;
  });
});

//重新加载验证码
function reloadCode(){
var code=document.getElementById('code');
code.setAttribute('src','/captcha.do?'+Math.random());
//这里必须加入随机数不然地址相同我发重新加载
}
</script>


</body>
</html>