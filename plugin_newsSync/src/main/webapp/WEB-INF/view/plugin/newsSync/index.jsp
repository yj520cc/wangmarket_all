<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="../../iw/common/head.jsp">
    <jsp:param name="title" value=""/>
</jsp:include>
<style>
body{
	margin: 30px;
}
h3{
	margin-bottom:6px;
}
li{
	list-style: initial;
}
ul{
	color: gray;
}
</style>

<div>
	<h3>插件功能：</h3>
	<ul>
		<li>自动同步当前网站中，内容管理中的文章信息。一个网站发文章，其他网站也会自动发</li>
		<li>自动同步别的网站生成整站的功能。当别的网站生成整站时，本网站也会自动生成整站</li>
	</ul>
</div>


<div style="margin-top:40px">
	<h3>使用场景（以做一个手机站跟一个电脑站为例）:</h3>
	<ul>
		客户需要做一个网站，要求手机网站跟电脑网站单独设计。那么用响应式不行的情况下，只能用两套模版了，而我们系统一个网站一套模版，也就是要用两个网站来实现，一个手机网站，一个电脑网站。
		<br/>两个网站做出来了，但是每当客户发新闻的时候，因为是两个网站，所以需要客户在电脑网站发完文章后，还要登录手机网站再发一次文章。同一个工作重复两次，给客户造成了负担，客户感觉不专业。
		<br/>这个内容同步插件便是在这种情境需求下，研发出来的。做好一个电脑网站、一个手机网站后，吧手机网站绑定到电脑网站（两个网站的栏目代码要能一一对应），那么这样后面发文章时，只需要登录电脑网站，发文章、生成整站，另一个手机网站也会自动将电脑站的文章同步过去、并生成整站。
	</ul>
</div>



<div style="margin-top:40px">
	<h3>注意事项及说明：</h3>
	<ul>
		<li>假设定义此网站为B网站，还有一个其他的网站为A网站，想要A网站中如果发新闻、改动文章，B网站中的也要自动改动，而不需要再登录B网站复制粘贴发布。</li>
		<li>例如，当A网站中，新闻动态(栏目代码为 xinwendongtai )栏目下，新增了一篇文章后，该插件会自动去B网站找到栏目代码为 xinwendongtai 的栏目，也为其增加一篇一摸一样的文章。这个同步的规则，便是根据两个网站的栏目代码来关联的。</li>
		<li>如果A网站中，有个栏目，其栏目代码在B网站中没有对应的，那么这个栏目下的文章变动(增加、修改、删除等)就不会同步到B网站。因为在B网站找不到对应要修改的栏目。</li>
		<li>B网站绑定了A网站，C网站绑定了B网站，A网站文章改动，会同步到B网站，但是不会同步到C网站。</li>
		<li>B网站绑定了A网站，那么A网站就不会再被其他网站绑定。例如C网站也想绑定A网站，是绑定不上的。一个网站只能被绑定一次。（如果有其他需求，需要一个网站可以绑定多次的场景，可以联系说明，会考虑实际使用价值，若有价值这么做，会考虑增加多绑定。）</li>
		<li>例如B网站在2020.1.10日绑定了A网站，A网站中有一篇文章发布时间是在2020.1.9日，也就是在绑定之前文章就已经存在了，那么A网站中，删除这篇文章后，B网站不会有任何变动。</li>
		<li>B网站绑定了A网站，当A网站点击了生成整站，B网站也会自动生成整站，无需再登录B网站。</li>
	</ul>
</div>


<div style="margin-top:50px;">
	当前使用状态：<span class="not_qiyong">未启用</span><span class="qiyong">已启用</span>
	<div class="qiyong">
		您已绑定网站: <b>${sourceSite.name }</b>（网站编号:${sourceSite.id }），对方网站中，内容管理有改动，会自动同步到当前的网站中  
		<button type="button" class="layui-btn layui-btn-sm" style="margin-left:40px;" onclick="chexiaobind();">撤销绑定(变为不启用此功能)</button>
	</div>
	<div class="not_qiyong">
		如上所述，当前网站为B网站，
		<button type="button" class="layui-btn layui-btn-sm" style="margin-left:10px;" onclick="login();">点此绑定A网站</button>
	</div>
	
</div>
<script>
if('${sitebind.id}'.length > 0){
	//使用
	$(".qiyong").show();
	$(".not_qiyong").hide();
}else{
	$(".not_qiyong").show();
	$(".qiyong").hide();
}
</script>
 
<script>
//查看数据表详情信息
function login(){
	layer.open({
		type: 2, 
		title:'绑定A网站', 
		area: ['480px', '470px'],
		shadeClose: true, //开启遮罩关闭
		content: 'login.do'
	});
}

//撤销绑定
function chexiaobind(){
	var dtv_confirm = layer.confirm('您确定要撤销绑定吗？撤销绑定后，如果再重新绑定，之前已经有的数据将不会再同步。强烈建议不要瞎点，不要随意撤销。', {
	  btn: ['撤销绑定','取消'] //按钮
	}, function(){
		layer.close(dtv_confirm);
		parent.iw.loading("撤销中");
		$.post("cancelBind.do", function(data){
			parent.iw.loadClose();
			if(data.result == '1'){
				parent.iw.msgSuccess("撤销成功");
				location.reload();
	     	}else if(data.result == '0'){
	     		parent.iw.msgFailure(data.info);
	     	}else{
	     		parent.iw.msgFailure();
	     	}
		});
    }, function(){
	});
}
</script>
<jsp:include page="../../iw/common/foot.jsp"></jsp:include>