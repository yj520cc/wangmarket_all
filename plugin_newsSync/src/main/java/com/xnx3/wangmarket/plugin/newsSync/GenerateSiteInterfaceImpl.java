package com.xnx3.wangmarket.plugin.newsSync;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xnx3.j2ee.util.SpringUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.bean.NewsDataBean;
import com.xnx3.wangmarket.admin.cache.TemplateCMS;
import com.xnx3.wangmarket.admin.entity.News;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.entity.SiteColumn;
import com.xnx3.wangmarket.admin.pluginManage.interfaces.GenerateSiteInterface;
import com.xnx3.wangmarket.admin.service.TemplateService;
import com.xnx3.wangmarket.admin.service.impl.TemplateServiceImpl;
import com.xnx3.wangmarket.plugin.newsSync.entity.SiteBind;

/**
 * 实现生成站点相关
 * @author 管雷鸣
 *
 */
public class GenerateSiteInterfaceImpl  implements GenerateSiteInterface{

	@Override
	public BaseVO generateSiteBefore(HttpServletRequest request, Site site) {
		return null;
	}

	@Override
	public void generateSiteFinish(HttpServletRequest request, Site site, Map<String, SiteColumn> siteColumnMap,
			Map<String, List<News>> newsMap, Map<Integer, NewsDataBean> newsDataMap, TemplateCMS template) {
		Plugin plugin = new Plugin();
		
		List<SiteBind> siteBindList = plugin.getSiteBindList();
		if(siteBindList.size() == 0){
			return;
		}
		
		TemplateService templateService = SpringUtil.getBean(TemplateServiceImpl.class);
		for (int i = 0; i < siteBindList.size(); i++) {
			SiteBind siteBind = siteBindList.get(i);
			templateService.generateSiteHTML(request, SpringUtil.getSqlService().findById(Site.class, siteBind.getOtherId()));
		}
	}
}
