<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../iw/common/head.jsp">
	<jsp:param name="title" value="set"/>
</jsp:include>
<script src="/<%=Global.CACHE_FILE %>Site_generateHtmlStorageType.js"></script>
<script src="/<%=Global.CACHE_FILE %>Endpoint_endpoint.js"></script>

<style>
.layui-form-label{
	width: 150px;
}
.layui-input-block {
    margin-left: 180px;
}
</style>

<form class="layui-form" style="padding-top:35px; margin-bottom: 10px; padding-right:35px;">
	<div class="layui-form-item">
		<label class="layui-form-label">存储方式</label>
		<div class="layui-input-block">
			<script type="text/javascript">writeSelectAllOptionForgenerateHtmlStorageType_('${site.generateHtmlStorageType }','HTML存储方式', 1);</script>
			<div class="explain" id="storageTypeExplain"><!-- 这种存储方式的说明 --></div>
		</div>
		
	</div>
	
	<div id="div_obs">
		<div class="layui-form-item">
			<label class="layui-form-label">accessKeyId</label>
			<div class="layui-input-block">
				<input type="text" id="akid" name="accessKeyId" class="layui-input" value="${obs.accessKeyId }">
			</div>
			
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">accessKeySecret</label>
			<div class="layui-input-block">
				<input type="text" id="akst" name="accessKeySecret" class="layui-input" value="${obs.accessKeySecret }">
			</div>
		</div>
<%--		<div class="layui-form-item">--%>
<%--			<label class="layui-form-label">endpoint</label>--%>
<%--			<div class="layui-input-block">--%>
<%--				<input type="text" name="endpoint" class="layui-input" value="${obs.endpoint }">--%>
<%--				<div class="explain">填写如：obs.cn-north-4.myhuaweicloud.com</div>--%>
<%--			</div>--%>
<%--		</div>--%>
<%--		<div class="layui-form-item">--%>
<%--			<label class="layui-form-label">bucket name</label>--%>
<%--			<div class="layui-input-block">--%>
<%--				<input type="text" name="bucketName" class="layui-input" value="${obs.bucketName }">--%>
<%--				<div class="explain">桶名称</div>--%>
<%--			</div>--%>
<%--		</div>--%>
		<div class="layui-form-item">
			<label class="layui-form-label">endpoint</label>
			<div class="layui-input-block">
				<script type="text/javascript">writeSelectAllOptionForendpoint_('${obs.endpoint }','请选择', '');</script>
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">bucket name</label>
			<div class="layui-input-block">
				<input type="text" id="bucketName" name="bucketName" class="layui-input" value="${obs.bucketName }" style="padding-right: 120px;">
				<button type="button" class="layui-btn" onclick="createObsBucket()" style="float: right;margin-top: -38px;">
					创建
				</button>
				<div class="explain">桶名称</div>
			</div>
		</div>
	
	</div>
	
	
	<div id="div_sftp">
		<div class="layui-form-item">
			<label class="layui-form-label">host</label>
			<div class="layui-input-block">
				<input type="text" id="host" name="host" class="layui-input" value="${sftp.host }">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">username</label>
			<div class="layui-input-block">
				<input type="text" id="username" name="username" class="layui-input" value="${sftp.username }">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">password</label>
			<div class="layui-input-block">
				<input type="text" id="password" name="password" class="layui-input" value="${sftp.password }">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">path</label>
			<div class="layui-input-block">
				<input type="text" id="path" name="path" class="layui-input" value="${sftp.path }">
				<div class="explain">填写如：  /mnt/site/219/   那么生成的网站html将会存在于这个路径下</div>
			</div>
		</div>
	</div>
	
   	<div class="layui-form-item" style="padding-top:15px;">
		<div class="layui-input-block">
			<button class="layui-btn" lay-submit="" lay-filter="demo1">保存</button>
		</div>
	</div>
</form>


<script>
layui.use(['form', 'layedit', 'laydate'], function(){
	var form = layui.form;

	//监听提交
	form.on('submit(demo1)', function(data){
		parent.parent.msg.loading("保存中");
		var postDate = wm.getJsonObjectByForm($("form"));	//提交的表单数据
		wm.post('save.json', postDate, function (obj) { 
			parent.parent.msg.close();
			if(obj.result == '1'){
				parent.parent.msg.success("操作成功", function(){
					// 重新加载当前页
					parent.parent.loadIframeByUrl("/plugin/htmlSeparate/set.do");
				})
				// parent.layer.close(index);
				// parent.location.reload();	//刷新父窗口
			}else if(obj.result == '0'){
				parent.parent.msg.failure(obj.info);
			}else{
				parent.parent.msg.failure(result);
			}
		});
	    
		return false;
	});
	
	//当类型发生变动改变
	form.on('select(generateHtmlStorageType)', function (data) {
		selectTypeChange();
	});
	
});

//当类型改变后，相应的自定义网址也会显示或者隐藏、模版也会相应显示或者隐藏
function selectTypeChange(){
	document.getElementById("div_obs").style.display="none";
	document.getElementById("div_sftp").style.display="none";
	document.getElementById("storageTypeExplain").innerHTML = ''; //清空说明备注
	
	if(document.getElementById("generateHtmlStorageType").value == 'default'){
		document.getElementById("storageTypeExplain").innerHTML = '默认的存储方式。您当前如果安装时使用的时OBS云存储，那这里就是OBS云存储；如果您没有配置云存储，用的本地存储，那这里就时使用本地存储。如果您不懂，那用这个就对了';
	}else if(document.getElementById("generateHtmlStorageType").value == 'obs'){
		document.getElementById("div_obs").style.display="";
		document.getElementById("storageTypeExplain").innerHTML = '将生成的网站的静态html页面存储到华为云obs中';
	}else if(document.getElementById("generateHtmlStorageType").value == 'local'){
		document.getElementById("storageTypeExplain").innerHTML = '将生成的网站的静态html页面存储到服务器本身的请求缓存中(服务器的tomcat/wangmarket_data/domain_io_cache/)<br/>使用场景为：使用的云存储，但是网站内容很多，比如网站内容管理中有一万篇文章，那么使用了云存储的方式，在生成时牵扯到网络推送耗时，整个生成整站的时长可能长达几分钟，感觉上生成整站会非常慢，非常迟钝。这种情况下就可使用此，直接将生成的html文件放到服务器本身，来极大提高html文件生成速度。';
	}else if(document.getElementById("generateHtmlStorageType").value == 'sftp' || document.getElementById("generateHtmlStorageType").value == 'ftp'){
		document.getElementById("div_sftp").style.display="";
		document.getElementById("storageTypeExplain").innerHTML = '将生成的网站的静态html页面存储到指定的'+document.getElementById("generateHtmlStorageType").value+'中';
	}
	
}
selectTypeChange();

/**
 * 创建OBS桶
 */
function createObsBucket() {
	let param = {
		"akid" : $("#akid").val(),
		"akst" : $("#akst").val(),
		"area" : $("#endpoint").val()
	}

	console.log(param);

	parent.parent.msg.loading("创建中");
	wm.post('/plugin/huaWeiYunObsService/createObs.json', param, function (obj) {
		parent.parent.msg.close();
		if(obj.result == '1'){
			parent.parent.msg.success("操作成功");
			// 覆盖input
			$("#bucketName").val(obj.info);
		}else if(obj.result == '0'){
			parent.parent.msg.failure(obj.info);
			console.log(obj.info);
		}else{
			parent.parent.msg.failure(result);
		}
	});
}
</script>


</div>
	<div style="padding: 20px;color: gray;">
		<div><b>本插件介绍:</b></div>
		<div>
			本插件的核心在于，将网站访问跟后台系统完全分离。<br/>
			比如使用华为云OBS存储，将生成的网站html页面全部放到OBS上，这样网站进行域名绑定时，就不必在将域名解析到本系统，而是直接将域名解析到华为云OBS即可！当访客访问域名时，直接通过域名访问到OBS托管的html页面，完全不经过本系统。即使对方想进行恶意及DDOS等攻击，也完全不会到我们的后端服务器。而攻击的防御将完全有华为云OBS进行承担。<br/>
			像是集团网站、政务网站等、以及有安全攻防测试等单位，强烈建议使用此方式。
		</div>
	</div>
	<div style="padding: 20px; padding-top:0px; color: gray;">
		<div><b>使用后的优缺点:</b></div>
		<div>
			比如使用华为云OBS对象存储后，html文件将存在于配置的华为云obs中，与此同时，本系统的预览网站功能将会失效，你可以将域名绑定到OBS上后直接访问域名进行预览查看。<br/>
			另外一些功能插件将会失效，比如在线客服、百度商桥插件等，是依赖插件才能在网站上显示出来的，并不是直接在模板页面的html代码中的，这些插件将会失效。
		</div>
	</div>
</div>

<jsp:include page="../../iw/common/foot.jsp"></jsp:include>  