<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../iw/common/head.jsp">
	<jsp:param name="title" value="set"/>
</jsp:include>
<%
//在v5.5增加的这个设置，跟随 oem 插件增加的
String bindDomainDemo = SystemUtil.get("BIND_DOMAIN_DEMO");
if(bindDomainDemo == null || bindDomainDemo.length() == 0 || bindDomainDemo.equalsIgnoreCase("null")) {
	bindDomainDemo = "www.guanleiming.com";
}
%>
<script src="/<%=Global.CACHE_FILE %>Site_generateHtmlStorageType.js"></script>

<style>
.layui-form-label{
	width: 150px;
}
.layui-input-block {
    margin-left: 180px;
}
body{
	background-color: white;
}
</style>

<form class="layui-form" style="padding-top:35px; margin-bottom: 10px; padding-right:35px;">

	<c:if test="${obs.customDomain == '' || obs.customDomain == null }">
		<div id="div_obs">
			<div class="layui-form-item">
				<label class="layui-form-label">当前未绑定域名</label>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">请输入要绑定的域名</label>
				<div class="layui-input-block">
					<input type="text" name="customdomain" class="layui-input" value="${obs.customDomain }" style="padding-right: 120px;" placeholder="填写格式如 <%=bindDomainDemo %>">
					<button class="layui-btn" lay-submit="" lay-filter="bindDomain" style="float: right;margin-top: -38px;">
						立即绑定
					</button>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${obs.customDomain != '' && obs.customDomain != null }">
		<div style="padding: 20px;color: gray;">
			<div>当前已绑定域名：${obs.customDomain }</div>
		</div>
	</c:if>
</form>


<script>
layui.use(['form', 'layedit', 'laydate'], function(){
	var form = layui.form;

	//监听提交
	form.on('submit(bindDomain)', function(data){
		var postDate = wm.getJsonObjectByForm($("form"));	//提交的表单数据
		if(postDate.customdomain.indexOf(':') > -1 || postDate.customdomain.indexOf('/') > -1){
			msg.alert('域名格式校验异常<br/>填写格式如：<br/><%=bindDomainDemo %>');
			return false;
		}
		
		parent.parent.msg.loading("保存中");
		
		wm.post('/plugin/huaWeiYunObsService/bindCustomDomain.json', postDate, function (obj) {
			parent.parent.msg.close();
			if(obj.result == '1'){
				parent.parent.msg.success("操作成功");
				// parent.location.reload();	//刷新父窗口
				// 重新加载当前页
				//parent.parent.loadIframeByUrl("/plugin/htmlSeparate/set.do");
				window.location.reload();
			}else if(obj.result == '0'){
				parent.parent.msg.failure(obj.info);
			}else{
				parent.parent.msg.failure(result);
			}
		});
	    
		return false;
	});
	
});

</script>


</div>
	<div style="padding: 20px;color: gray;">
		<div>域名要解析到的CNAME地址：${obs.bucketName }.${obs.endpoint }</div>
	</div>
	<div style="padding: 20px;color: gray;">
		<div>
			提示：如果要绑定多个域名、或更改要绑定的域名等，可直接进入华为云控制台进行操作
		</div>
	</div>
</div>

<jsp:include page="../../iw/common/foot.jsp"></jsp:include>  