package com.xnx3.wangmarket.plugin.htmlSeparate.storageInterfaceImpl;

import java.io.UnsupportedEncodingException;

import com.jcraft.jsch.SftpException;
import com.xnx3.FileUtil;
import com.xnx3.StringUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.util.SFTPUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.cache.generateSite.GenerateHtmlInterface;
import com.xnx3.wangmarket.plugin.htmlSeparate.entity.HtmlStorageSftp;

/**
 * SFTP GenerateHtmlInterface 接口的实现
 * @author 管雷鸣
 */
public class SFtpGenerateHtmlInterfaceImpl implements GenerateHtmlInterface{
	private SFTPUtil sftp;
	private HtmlStorageSftp htmlStorageSftp;
	
	public SFtpGenerateHtmlInterfaceImpl(HtmlStorageSftp htmlStorageSftp) {
		sftp = new SFTPUtil();
		sftp.setHost(htmlStorageSftp.getHost());
		sftp.setUsername(htmlStorageSftp.getUsername());
		sftp.setPassword(htmlStorageSftp.getPassword());
		sftp.setPort(htmlStorageSftp.getPort());
		this.htmlStorageSftp = htmlStorageSftp;
	}
	
	private void openConnectCheck() {
		if(sftp.getSftp() == null || !sftp.getSftp().isConnected()) {
			//链接没打开，那么打开链接
			sftp.connect();
			try {
				sftp.getSftp().cd(htmlStorageSftp.getPath()); //切换到指定上传目录
			} catch (SftpException e) {
				e.printStackTrace();
			} 
		}
	}
	
	@Override
	public BaseVO putStringFile(String text, String path) {
		openConnectCheck();
		return uploadFile(path, text);
	}
	

    /**
     * 上传单个文件到服务器指定路径下。
     * <p>注意，这个路径如果不存在，会自动创建</p>
     * @param fileName 上传到sftp后的文件名，传入如 index.html
     * @param text html文件的内容
     */
    public BaseVO uploadFile(String fileName,String text){
        long start = System.currentTimeMillis();

        try {
			try {
				sftp.getSftp().put(StringUtil.stringToInputStream(text, FileUtil.UTF8), fileName);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} catch (SftpException e) {
			e.printStackTrace();
			return BaseVO.failure(e.getMessage());
		}
        
//        ConsoleUtil.debug("文件 ["+fileName+"] 同步成功,耗时："+(System.currentTimeMillis() - start)+"ms");
        return BaseVO.success();
    }
    
}
