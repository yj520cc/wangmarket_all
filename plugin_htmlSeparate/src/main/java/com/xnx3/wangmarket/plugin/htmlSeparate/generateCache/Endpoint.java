package com.xnx3.wangmarket.plugin.htmlSeparate.generateCache;

import com.xnx3.j2ee.generateCache.BaseGenerate;
import org.springframework.stereotype.Component;

/**
 * OBS所在地区
 * @author 刘晓腾
 */
@Component(value="GenerateOBSEndpointPlugin")
public class Endpoint extends BaseGenerate {

    public Endpoint() {
        endpoint();
    }

    /**
     * html存储的方式
     */
    public void endpoint(){
        createCacheObject("endpoint");
        cacheAdd("obs.cn-north-4.myhuaweicloud.com" , "华北-北京四");
        cacheAdd("obs.cn-north-1.myhuaweicloud.com" , "华北-北京一");
        cacheAdd("obs.cn-north-9.myhuaweicloud.com" , "华北-乌兰察布一");
        cacheAdd("obs.cn-east-2.myhuaweicloud.com" , "华东-上海二");
        cacheAdd("obs.cn-east-3.myhuaweicloud.com" , "华东-上海一");
        cacheAdd("obs.cn-south-1.myhuaweicloud.com" , "华南-广州");
        cacheAdd("obs.cn-southwest-2.myhuaweicloud.com" , "西南-贵阳一");
        cacheAdd("obs.ap-southeast-1.myhuaweicloud.com" , "中国-香港");
        cacheAdd("obs.na-mexico-1.myhuaweicloud.com" , "拉美-墨西哥城一");
        cacheAdd("obs.sa-brazil-1.myhuaweicloud.com" , "拉美-圣保罗一");
        cacheAdd("obs.ap-southeast-3.myhuaweicloud.com" , "亚太-新加坡");
        cacheAdd("obs.af-south-1.myhuaweicloud.com" , "非洲-约翰内斯堡");
        generateCacheFile();
    }

}
