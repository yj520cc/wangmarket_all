package com.xnx3.wangmarket.plugin.htmlSeparate.storageInterfaceImpl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.xnx3.FileUtil;
import com.xnx3.j2ee.util.CacheUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.net.HttpUtil;
import com.xnx3.wangmarket.admin.bean.NewsDataBean;
import com.xnx3.wangmarket.admin.cache.TemplateCMS;
import com.xnx3.wangmarket.admin.cache.generateSite.GenerateHtmlInterface;
import com.xnx3.wangmarket.admin.entity.News;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.entity.SiteColumn;
import com.xnx3.wangmarket.admin.pluginManage.interfaces.GenerateSiteInterface;
import com.xnx3.wangmarket.admin.util.WangmarketDataUtil;
import com.xnx3.wangmarket.domain.bean.TextBean;

/**
 * 生成到本地服务器。注意这里是生成到服务器的缓存区域中，访问的时候直接从缓存中取
 * @author 管雷鸣
 */
public class LocalGenerateHtmlInterfaceImpl implements GenerateHtmlInterface, GenerateSiteInterface{
	private Site site;	//是哪个站点要生成
	//存储站点的htmlfile 列表， key：siteid， value：htmlfile列表，如 about.html ,在生成整站时加入这里，生成整站完成后统一生成domain_io缓存，然后自动清空掉这里
	public static Map<Integer, List<String>> siteHtmlFileList = new HashMap<Integer, List<String>>();
	
	/**
	 * 这个是给插件加载初始化用的。
	 * @deprecated
	 */
	public LocalGenerateHtmlInterfaceImpl() {
		
	}
	
	public LocalGenerateHtmlInterfaceImpl(Site site) {
		this.site = site;
	}
	
	@Override
	public BaseVO putStringFile(String text, String path) {
		if(site == null) {
			return BaseVO.failure("异常，site未发现");
		}
		
		//写入txt缓存文件
		try {
			FileUtil.write(WangmarketDataUtil.getWangmarketDataRootPath()+"domain_io_cache"+File.separator+"site"+File.separator+site.getId()+File.separator+path, text, FileUtil.UTF8);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		Object obj = CacheUtil.get("site:"+site.getId());
//		Map<String, TextBean> map = new HashMap<String, TextBean>();
//		if(obj != null) {
//			//这个站点已经有过缓存了
//			map = (Map<String, TextBean>) obj;
//		}
//		TextBean bean = new TextBean();
//		bean.setExistMenory(false); //用io文件方式
//		map.put(path, bean);
//		//加入domain缓存，这样读取的时候直接从缓存中读
//		CacheUtil.set("site:"+site.getId(), map);
		
		//加入文件名list中，待生成网站完毕，用这些写出 domain_io 缓存标识
		List<String> list = siteHtmlFileList.get(site.getId());
		if(list == null) {
			list = new LinkedList<String>();
		}
		list.add(path);
		siteHtmlFileList.put(site.getId(), list);
		
		return BaseVO.success();
	}

	@Override
	public BaseVO generateSiteBefore(HttpServletRequest request, Site site) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void generateSiteFinish(HttpServletRequest request, Site site, Map<String, SiteColumn> siteColumnMap,
			Map<String, List<News>> newsMap, Map<Integer, NewsDataBean> newsDataMap, TemplateCMS template) {
		// TODO Auto-generated method stub
		List<String> list = siteHtmlFileList.get(site.getId());
		if(list == null || list.size() < 1) {
			//ConsoleUtil.info("无缓存文件，网站还没有任何html页面");
			return;
		}
		
		Map<String, TextBean> map = new HashMap<String, TextBean>();
		for (int i = 0; i<list.size(); i++) {
			TextBean bean = new TextBean();
			bean.setExistMenory(false); //用io文件方式
			map.put(list.get(i), bean);
		}
		//加入domain缓存，这样读取的时候直接从缓存中读
		CacheUtil.set("site:"+site.getId(), map);
		ConsoleUtil.debug("缓存 domain_io 标识 ："+list.size()+"条");
		
		//清理内存
		siteHtmlFileList.remove(site.getId());
	}
	
}
