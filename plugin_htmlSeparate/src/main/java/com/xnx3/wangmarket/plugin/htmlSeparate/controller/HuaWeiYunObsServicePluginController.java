package com.xnx3.wangmarket.plugin.htmlSeparate.controller;
import javax.annotation.Resource;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.util.SessionUtil;
import com.xnx3.wangmarket.plugin.htmlSeparate.entity.HtmlStorageObs;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.obs.services.exception.ObsException;
import com.xnx3.DateUtil;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.entity.System;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.SystemService;
import com.xnx3.j2ee.util.AttachmentUtil;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.j2ee.util.AttachmentMode.HuaweiyunOBSMode;
import com.xnx3.j2ee.util.AttachmentMode.hander.OBSHandler;
import com.xnx3.j2ee.vo.BaseVO;

import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Locale;
import java.util.TimeZone;

/**
 * OBS相关
 * @author 刘晓腾
 */
@Controller(value="HuaWeiYunObsServicePluginController")
@RequestMapping("/plugin/huaWeiYunObsService/")
public class HuaWeiYunObsServicePluginController extends BasePluginController {

    @Resource
    private SqlService sqlService;

    @Resource
    private SystemService systemService;

    /**
     * 自动创建OBS以及更新数据库参数
     * @author 刘晓腾
     * @param akid 华为云 accessKeyId
     * @param akst 华为云 accessKeySecret
     * @param area OBS的地区，也就是 endpoint
     * @return
     */
    @RequestMapping("createObs.json")
    @ResponseBody
    public BaseVO createOBS(HttpServletRequest request,
                            @RequestParam(value = "akid", required = true , defaultValue="") String akid,
                            @RequestParam(value = "akst", required = true , defaultValue="") String akst,
                            @RequestParam(value = "area", required = true , defaultValue="") String area){

        // 查出网站信息
        Site cacheSite = SessionUtil.getSite();
        if(cacheSite == null) {
            return error("网站不存在");
        }
        int siteId = cacheSite.getId();

        java.lang.System.out.println(akid);
        java.lang.System.out.println(akst);
        java.lang.System.out.println(area);

        /*
         * 初始化判断 system 的数据
         */
        if("".equals(akid)){
            return error("请先填写 Access Key Id");
        }

        //从通用区域( HUAWEIYUN_COMMON_ENDPOINT )中，取得所属区域，设置当前obs的区域
        if("".equals(area)){
            return error("请选择Endpoint");
        }

        OBSHandler obsHandler = new OBSHandler(akid, akst, area);
        //检测OBS是否能成功连接上
        try {
            obsHandler.getBuckets().size();
        } catch (ObsException obs) {
        	return error("异常：" + obs.getErrorMessage());
        } catch (Exception e) {
            return error("连接华为云OBS服务失败" + e.getMessage());
        }

        String bucketName = "wangmarket-" + siteId + "-" + DateUtil.timeForUnix10();
        // 创建一个华为云OBS桶
        obsHandler.createOBSBucket(bucketName);

        // 检测这个当前数据库中保存的bucket是否已经存在，是否已经创建了
        boolean have = obsHandler.getObsClient().headBucket(obsHandler.getObsBucketName());
        if(!have){
            // 如果不存在，则创建一个。
            bucketName = "wangmarket-" + siteId + "-" + DateUtil.timeForUnix10();
            obsHandler.createOBSBucket(bucketName);

            // 创建完毕后再请求看看，是不是真的创建成功了
            if(obsHandler.getObsClient().headBucket(obsHandler.getObsBucketName())){

            }else{
                return error("检测到OBS的BucketName不存在，系统进行自动创建时，失败!如有需要，请联系我们。");
            }
        }

        // 创建成功，返回名字
        return success(bucketName);
    }


    /**
     * 华为云OBS绑定自定义域名
     * @author 刘晓腾
     * @param customdomain 要绑定的自定义域名
     * @return
     */
    @ResponseBody
    @RequestMapping("bindCustomDomain.json")
    public BaseVO bindCustomDomain(HttpServletRequest request,
           @RequestParam(value = "customdomain", required = true , defaultValue="") String customdomain) {

        if ("".equals(customdomain)) {
            return error("请输入要绑定的域名");
        }

        /*
         * 查出网站信息
         */
        Site cacheSite = SessionUtil.getSite();
        if(cacheSite == null) {
            return error("网站不存在");
        }
        // 网站信息
        Site site = sqlService.findById(Site.class, cacheSite.getId());
        // 查出配置的OBS的信息
        HtmlStorageObs obs = sqlService.findById(HtmlStorageObs.class, site.getId());
        if(obs == null) {
            return error("请先配置OBS");
        }

        /*
         * 请求华为云OBS的API，绑定自定义域名
         */
        // 使用HttpClient
        CloseableHttpClient httpClient = HttpClients.createDefault();

        // Date
        DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String requestTime = dateFormat.format(DateUtil.timeForUnix13());

        // 请求的URL
        String requestUrl = "http://" + obs.getBucketName() + "." + obs.getEndpoint() + "/?customdomain=" + customdomain;

        // 使用Put请求
        HttpPut httpPut = new HttpPut(requestUrl);
        // 设置Date参数
        httpPut.addHeader("Date", requestTime);

        /* 计算签名
         * StringToSign =
         *     HTTP-Verb + "\n" +
         *     Content-MD5 + "\n" +
         *     Content-Type + "\n" +
         *     Date + "\n" +
         *     CanonicalizedHeaders + CanonicalizedResource
         */
        String contentMd5 = "";
        String contentType = "";
        String canonicalizedHeaders = "";
        String canonicalizedResource = "/" + obs.getBucketName() + "/";

        String canonicalString = "PUT" + "\n" + contentMd5 + "\n" + contentType + "\n" + requestTime + "\n"
                + canonicalizedHeaders + canonicalizedResource;
        java.lang.System.out.println("StringToSign:[" + canonicalString + "]");

        // 签名
        String signature = null;
        // 进行计算
        try {
            SecretKeySpec signingKey = new SecretKeySpec(obs.getAccessKeySecret().getBytes("UTF-8"), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
            signature = Base64.getEncoder().encodeToString(mac.doFinal(canonicalString.getBytes("UTF-8")));


            // 添加签名头域
            httpPut.addHeader("Authorization", "OBS " + obs.getAccessKeyId() + ":" + signature);

            // 发送请求
            CloseableHttpResponse httpResponse = httpClient.execute(httpPut);

            // 打印结果
            java.lang.System.out.println("Request Message:");
            java.lang.System.out.println(httpPut.getRequestLine());
            for (Header header: httpPut.getAllHeaders()) {
                java.lang.System.out.println(header.getName() + ":" + header.getValue());
            }

            java.lang.System.out.println("Response Message:");
            java.lang.System.out.println(httpResponse.getStatusLine());
            for (Header header: httpResponse.getAllHeaders()) {
                java.lang.System.out.println(header.getName() + ":" + header.getValue());
            }

            // 解析结果
            String resultCode = httpResponse.getStatusLine().toString().replace("HTTP/1.1 ", "");
            if (!resultCode.startsWith("200")) {
                return error("绑定失败<br/>可能原因是该域名已经绑定过了(别的网站)您可以换个域名试试。");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return error("绑定失败,"+e.getMessage());
        }

        // 记录绑定信息
        obs.setCustomDomain(customdomain);
        sqlService.save(obs);

        return success();
    }

}
