package com.xnx3.wangmarket.plugin.htmlSeparate.controller;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.obs.services.ObsClient;
import com.obs.services.exception.ObsException;
import com.obs.services.model.HeaderResponse;
import com.obs.services.model.WebsiteConfiguration;
import com.xnx3.StringUtil;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.util.AttachmentUtil;
import com.xnx3.j2ee.util.CacheUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.net.HttpUtil;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.util.ActionLogUtil;
import com.xnx3.wangmarket.admin.util.SessionUtil;
import com.xnx3.wangmarket.plugin.htmlSeparate.entity.HtmlStorageObs;
import com.xnx3.wangmarket.plugin.htmlSeparate.entity.HtmlStorageSftp;
import com.xnx3.wangmarket.plugin.htmlSeparate.vo.HtmlStorageObsVO;

/**
 * htmlSeparate
 * @author 管雷鸣
 */
@Controller(value="HtmlSeparatePluginController")
@RequestMapping("/plugin/htmlSeparate/")
public class SetController extends BasePluginController {
	@Resource
	private SqlService sqlService;

	
	/**
	 * 设置
	 */
	@RequestMapping(value="set.do")
	public String set(HttpServletRequest request, Model model){

		Site site = SessionUtil.getSite();
		if(site == null) {
			return error(model, "网站不存在");
		}

		// 是否已经配置了华为云
		boolean hasObs = false;

		if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_OBS)) {
			//obs
			HtmlStorageObs obs = sqlService.findById(HtmlStorageObs.class, site.getId());
			if(obs == null) {
				obs = new HtmlStorageObs();
			}
			model.addAttribute("obs", obs);

			// 只有四个都填了，才设置为true，否则就回到华为云配置页面重新填写
			if ((obs.getAccessKeyId() != null && !"".equals(obs.getAccessKeyId()))
				&& (obs.getAccessKeySecret() != null && !"".equals(obs.getAccessKeySecret()))
				&& (obs.getEndpoint() != null && !"".equals(obs.getEndpoint()))
				&& (obs.getBucketName() != null && !"".equals(obs.getBucketName()))) {
				hasObs = true;
			}
		}else if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_FTP) || site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_SFTP)) {
			//ftp
			//sftp
			HtmlStorageSftp sftp = sqlService.findById(HtmlStorageSftp.class, site.getId());
			if(sftp == null) {
				sftp = new HtmlStorageSftp();
				sftp.setId(site.getId());
			}
			if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_FTP)) {
				sftp.setPort(21);
			}else if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_SFTP)) {
				sftp.setPort(22);
			}
			model.addAttribute("sftp", sftp);
		}else if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_LOCAL)) {
			//local
		}

		model.addAttribute("site", site);

		// 根据标记判断该执行哪一步
		if (hasObs == false) {
			return "plugin/htmlSeparate/set";
		} else {
			return "plugin/htmlSeparate/setCustomDomainPage";
		}
	}
	
	

	/**
	 * 设置保存
	 * @param generateHtmlStorageType site.generateHtmlStorageType
	 * @param accessKeyId HtmlStorageObs.accessKeyId
	 * @param accessKeySecret HtmlStorageObs.accessKeySecret
	 * @param endpoint HtmlStorageObs.endpoint
	 * @param bucketName HtmlStorageObs.bucketName
	 * @param param host sftp.host
	 * @param param username sftp.username
	 * @param param password sftp.password
	 * @param param path sftp.path
	 * 
	 */
	@RequestMapping(value="save.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO save(HttpServletRequest request,
			@RequestParam(value = "generateHtmlStorageType", required = true , defaultValue="default") String generateHtmlStorageType,
			@RequestParam(value = "bucketName", required = false , defaultValue="") String bucketName,
			@RequestParam(value = "accessKeyId", required = false , defaultValue="") String accessKeyId,
			@RequestParam(value = "accessKeySecret", required = false , defaultValue="") String accessKeySecret,
			@RequestParam(value = "endpoint", required = false , defaultValue="") String endpoint,
			@RequestParam(required = false , defaultValue="") String host,
			@RequestParam(required = false , defaultValue="") String username,
			@RequestParam(required = false , defaultValue="") String password,
			@RequestParam(required = false , defaultValue="") String path){
		BaseVO vo = new BaseVO();
		
		Site cacheSite = SessionUtil.getSite();
		if(cacheSite == null) {
			return error("网站不存在");
		}
		Site site = sqlService.findById(Site.class, cacheSite.getId());
		
		if(generateHtmlStorageType.equalsIgnoreCase(Site.GENERATE_HTML_STORAGE_TYPE_OBS)) {
			/*** 保存obs ***/
			HtmlStorageObs obs = sqlService.findById(HtmlStorageObs.class, site.getId());
			if(obs == null) {
				obs = new HtmlStorageObs();
				obs.setId(site.getId());
			}
			obs.setAccessKeyId(accessKeyId);
			obs.setAccessKeySecret(accessKeySecret);
			obs.setEndpoint(endpoint);
			obs.setBucketName(bucketName);
			sqlService.save(obs);
			
			//设置OBS的默认主页和错误页面
			// 创建ObsClient实例
			ObsClient obsClient = new ObsClient(obs.getAccessKeyId(), obs.getAccessKeySecret(), obs.getEndpoint());
			WebsiteConfiguration config = new WebsiteConfiguration();
			// 配置默认主页
			config.setSuffix("index.html");
			// 配置404错误页面
			config.setKey("error404.html");
			HeaderResponse hr = obsClient.setBucketWebsite(obs.getBucketName(), config);
			System.out.println(hr.getStatusCode());
			
			//上传一个 error404.html
			String html = "<html><head><meta charset=\"UTF-8\"><title>404</title></head><body>404 error , page not find !<script>if(window.location.pathname == '/' || window.location.pathname == '/index.html'){alert('请在网站管理后台的右侧菜单中，找到【生成整站】并点击，生成网站。');}</script></body></html>";
			try {
				obsClient.putObject(obs.getBucketName(), config.getKey(), new ByteArrayInputStream(html.getBytes(HttpUtil.UTF8)));
			} catch (ObsException | UnsupportedEncodingException e) {
				e.printStackTrace();
				return error("自动创建error404.html失败："+e.getMessage());
			}
		}else if(generateHtmlStorageType.equalsIgnoreCase(Site.GENERATE_HTML_STORAGE_TYPE_LOCAL)) {
			//local服务器

			//判断当前是否使用的时服务器本地存储
			if(AttachmentUtil.isMode(AttachmentUtil.MODE_LOCAL_FILE)) {
				return error("您当前使用的就是服务器本身进行存储html，所以无需再配置网站分离将html文件存储到服务器本身！");
			}
			//判断当前是否使用了redis，如果没用redis，系统一重启，缓存就没了，就不从缓存文件读取了
			if(!CacheUtil.isUseRedis()) {
				return error("此类型必须采用Redis。请联系您的安装人员，为您配置Redis");
			}
		}else if(generateHtmlStorageType.equalsIgnoreCase(Site.GENERATE_HTML_STORAGE_TYPE_SFTP) || generateHtmlStorageType.equalsIgnoreCase(Site.GENERATE_HTML_STORAGE_TYPE_FTP)) {
			/*** 保存sftp参数 ***/
			HtmlStorageSftp sftp = sqlService.findById(HtmlStorageSftp.class, site.getId());
			if(sftp == null) {
				sftp = new HtmlStorageSftp();
				sftp.setId(site.getId());
			}
			if(generateHtmlStorageType.equalsIgnoreCase(Site.GENERATE_HTML_STORAGE_TYPE_SFTP)) {
				sftp.setPort(22);
			}else if(generateHtmlStorageType.equalsIgnoreCase(Site.GENERATE_HTML_STORAGE_TYPE_FTP)) {
				sftp.setPort(21);
			}
			
			sftp.setHost(host);
			sftp.setPassword(password);
			sftp.setPath(path);
			sftp.setUsername(username);
			sqlService.save(sftp);
		}
		
		/**** 保存site ****/
		site.setGenerateHtmlStorageType(StringUtil.filterXss(generateHtmlStorageType));
		sqlService.save(site);
		
		ActionLogUtil.insertUpdateDatabase(request, "修改 htmlSeparate set 保存");
		
		//更新当前Session缓存
		SessionUtil.setSite(site);
		return vo;
	}

	/**
	 * 设置华为云自定义域名的页面
	 */
	@RequestMapping(value="setCustomDomainPage.do")
	public String setCustomDomainPage(HttpServletRequest request, Model model){

		Site site = SessionUtil.getSite();
		if(site == null) {
			return error(model, "网站不存在");
		}

		if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_OBS)) {
			//obs
			HtmlStorageObs obs = sqlService.findById(HtmlStorageObs.class, site.getId());
			if(obs == null) {
				obs = new HtmlStorageObs();
			}
			model.addAttribute("obs", obs);
		}else if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_FTP)) {
			//ftp
		}

		model.addAttribute("site", site);

		return "plugin/htmlSeparate/setCustomDomainPage";
	}


	/**
	 * 获取当前设置的OBS的信息
	 */
	@RequestMapping(value="getHtmlStorageObs.json", method = RequestMethod.POST)
	@ResponseBody
	public HtmlStorageObsVO getHtmlStorageObs(HttpServletRequest request){
		HtmlStorageObsVO vo = new HtmlStorageObsVO();
		
		if(!SessionUtil.isLogin()) {
			vo.setBaseVO(BaseVO.FAILURE, "请先登录");
			return vo;
		}
		Site site = SessionUtil.getSite();
		if(site == null) {
			vo.setBaseVO(BaseVO.FAILURE, "网站不存在");
			return vo;
		}

		HtmlStorageObs obs = sqlService.findById(HtmlStorageObs.class, site.getId());
		if(obs == null) {
			obs = new HtmlStorageObs();
		}
		vo.setObs(obs);
		return vo;
	}
	
	public static void main(String[] args) {
		
//		ObsClient obsClient = new ObsClient(xxx, "je56lHbJ62VOhoSXcsfI9InmPAtVY9ut5ZqaI29O", "obs.cn-north-4.myhuaweicloud.com");
//		WebsiteConfiguration config = new WebsiteConfiguration();
//		// 配置默认主页
//		config.setSuffix("index.html");
//		// 配置404错误页面
//		config.setKey("error404.html");
//		HeaderResponse hr = obsClient.setBucketWebsite("wangmarket-219-1658913803", config);
//		System.out.println(hr.getStatusCode());
	}
}