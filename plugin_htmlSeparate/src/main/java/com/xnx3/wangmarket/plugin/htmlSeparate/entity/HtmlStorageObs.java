package com.xnx3.wangmarket.plugin.htmlSeparate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 表单反馈
 * @author 管雷鸣
 */
@Entity(name="plugin_htmlseparate_obs")
@Table(name = "plugin_htmlseparate_obs")
public class HtmlStorageObs implements java.io.Serializable {

	private Integer id;			//网站id，对应site.id
	private String accessKeyId;
	private String accessKeySecret;
	private String endpoint;
	private String bucketName;
	private String customDomain;	// OBS绑定的自定义域名

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "access_key_id", columnDefinition="char(50) comment '' default ''")
	public String getAccessKeyId() {
		return accessKeyId;
	}

	public void setAccessKeyId(String accessKeyId) {
		this.accessKeyId = accessKeyId;
	}

	@Column(name = "access_key_secret", columnDefinition="char(50) comment '' default ''")
	public String getAccessKeySecret() {
		return accessKeySecret;
	}

	public void setAccessKeySecret(String accessKeySecret) {
		this.accessKeySecret = accessKeySecret;
	}
	
	@Column(name = "endpoint", columnDefinition="char(50) comment '' default ''")
	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	
	@Column(name = "bucket_name", columnDefinition="char(50) comment '' default ''")
	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	@Column(name = "custom_domain", columnDefinition="char(50) comment 'OBS绑定的自定义域名' default ''")
	public String getCustomDomain() {
		if (customDomain == null) {
			customDomain = "";
		}
		return customDomain;
	}

	public void setCustomDomain(String customDomain) {
		this.customDomain = customDomain;
	}

	@Override
	public String toString() {
		return "HtmlStorageObs [id=" + id + ", accessKeyId=" + accessKeyId + ", accessKeySecret=" + accessKeySecret
				+ ", endpoint=" + endpoint + ", bucketName=" + bucketName + ", customDomain=" + customDomain + "]";
	}

}