package com.xnx3.wangmarket.plugin.htmlSeparate.generateCache;

import org.springframework.stereotype.Component;
import com.xnx3.j2ee.generateCache.BaseGenerate;

/**
 * 轮播图
 * @author 管雷鸣
 */
@Component(value="GenerateSiteHtmlSeparatePlugin")
public class Site extends BaseGenerate {
	public Site() {
		generateHtmlStorageType();
	}
	
	/**
	 * html存储的方式
	 */
	public void generateHtmlStorageType(){
		createCacheObject("generateHtmlStorageType");
		cacheAdd(com.xnx3.wangmarket.admin.entity.Site.GENERATE_HTML_STORAGE_TYPE_DEFAULT , "默认");
		cacheAdd(com.xnx3.wangmarket.admin.entity.Site.GENERATE_HTML_STORAGE_TYPE_OBS , "华为云OBS对象存储");
		cacheAdd(com.xnx3.wangmarket.admin.entity.Site.GENERATE_HTML_STORAGE_TYPE_SFTP , "第三方SFTP存储");
		cacheAdd(com.xnx3.wangmarket.admin.entity.Site.GENERATE_HTML_STORAGE_TYPE_LOCAL , "服务器本地存储");
		cacheAdd(com.xnx3.wangmarket.admin.entity.Site.GENERATE_HTML_STORAGE_TYPE_FTP , "第三方FTP存储");
		generateCacheFile();
	}
	
}
