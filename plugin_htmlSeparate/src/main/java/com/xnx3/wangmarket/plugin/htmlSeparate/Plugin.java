package com.xnx3.wangmarket.plugin.htmlSeparate;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xnx3.FileUtil;
import com.xnx3.j2ee.pluginManage.PluginRegister;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.util.SpringUtil;
import com.xnx3.wangmarket.admin.cache.generateSite.DefaultGenerateHtmlInterfaceImpl;
import com.xnx3.wangmarket.admin.cache.generateSite.GenerateHtmlInterface;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.pluginManage.interfaces.GenerateHtmlStorateInterface;
import com.xnx3.wangmarket.admin.util.WangmarketDataUtil;
import com.xnx3.wangmarket.domain.bean.TextBean;
import com.xnx3.wangmarket.domain.util.GainSource;
import com.xnx3.wangmarket.plugin.htmlSeparate.entity.HtmlStorageObs;
import com.xnx3.wangmarket.plugin.htmlSeparate.entity.HtmlStorageSftp;
import com.xnx3.wangmarket.plugin.htmlSeparate.storageInterfaceImpl.FtpGenerateHtmlInterfaceImpl;
import com.xnx3.wangmarket.plugin.htmlSeparate.storageInterfaceImpl.LocalGenerateHtmlInterfaceImpl;
import com.xnx3.wangmarket.plugin.htmlSeparate.storageInterfaceImpl.ObsGenerateHtmlInterfaceImpl;
import com.xnx3.wangmarket.plugin.htmlSeparate.storageInterfaceImpl.SFtpGenerateHtmlInterfaceImpl;

/**
 * 网站html分离
 * @author 管雷鸣
 */
@PluginRegister(version="1.2", menuTitle = "网站分离",menuHref="/plugin/htmlSeparate/set.do", applyToCMS=true, intro="将生成的网站html页面，采用单独的obs进行存放，可以将域名直接解析到obs即可完成。域名直接解析到obs，可隐藏后端服务器，极大提高安全系数", versionMin="5.5")
public class Plugin implements GenerateHtmlStorateInterface{

	@Override
	public GenerateHtmlInterface getGenerateHtmlInterfaceImpl(HttpServletRequest request, Site site) {
		if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_DEFAULT)) {
			//默认存储方式
			return new DefaultGenerateHtmlInterfaceImpl(site);
		}else if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_OBS)) {
			//obs
			HtmlStorageObs obs = SpringUtil.getSqlService().findById(HtmlStorageObs.class, site.getId());
			if(obs == null) {
				ConsoleUtil.error("HtmlStorageObs is null ! site:"+site.toString());
				return null;
			}
			
			ObsGenerateHtmlInterfaceImpl obsImpl = new ObsGenerateHtmlInterfaceImpl(obs.getAccessKeyId(), obs.getAccessKeySecret(), obs.getEndpoint(), obs.getBucketName());
//			ConsoleUtil.log(obsImpl.toString());
			return obsImpl;
		}else if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_LOCAL)) {
			
			//检测缓存文件夹是否存在，如果不存在，则创建
			GainSource.directoryInit(site.getId());	
			
			//写入txt缓存文件
			return new LocalGenerateHtmlInterfaceImpl(site);
		}else if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_SFTP) || site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_FTP)) {
			//sftp、ftp设置
			HtmlStorageSftp sftp = SpringUtil.getSqlService().findById(HtmlStorageSftp.class, site.getId());
			if(sftp == null) {
				ConsoleUtil.error("HtmlStorageSftp is null ! site:"+site.toString());
				return null;
			}
			
			if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_SFTP)) {
				SFtpGenerateHtmlInterfaceImpl sftpImpl = new SFtpGenerateHtmlInterfaceImpl(sftp);
				return sftpImpl;
			}else if(site.getGenerateHtmlStorageType().equals(Site.GENERATE_HTML_STORAGE_TYPE_FTP)) {
				FtpGenerateHtmlInterfaceImpl ftpImpl = new FtpGenerateHtmlInterfaceImpl(sftp);
				return ftpImpl;
			}
		}
		
		return null;
	}

}