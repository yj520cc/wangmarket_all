package com.xnx3.wangmarket.plugin.htmlSeparate.vo;

import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.plugin.htmlSeparate.entity.HtmlStorageObs;

public class HtmlStorageObsVO extends BaseVO{
	private HtmlStorageObs obs;

	public HtmlStorageObs getObs() {
		return obs;
	}

	public void setObs(HtmlStorageObs obs) {
		this.obs = obs;
	}
}
