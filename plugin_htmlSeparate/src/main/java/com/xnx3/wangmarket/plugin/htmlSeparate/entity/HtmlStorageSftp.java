package com.xnx3.wangmarket.plugin.htmlSeparate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 存储到SFTP、FTP中
 * @author 管雷鸣
 */
@Entity(name="plugin_htmlseparate_sftp")
@Table(name = "plugin_htmlseparate_sftp")
public class HtmlStorageSftp implements java.io.Serializable {

	private Integer id;			//网站id，对应site.id
	private String host;		//填入域名，或ip，如 192.168.1.1
	private String username;	//登录的用户名
	private String password;	//登录的密码
	private String path;		//存储的路径，格式如 /site/219/ 	
	private Integer port;		//端口号。这里默认就是22，sftp端口默认22

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "host", columnDefinition="char(50) comment '' default ''")
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	@Column(name = "username", columnDefinition="char(50) comment '' default ''")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", columnDefinition="char(50) comment '' default ''")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "path", columnDefinition="char(50) comment '' default ''")
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	@Column(name = "port", columnDefinition="int(11) comment ''")
	public Integer getPort() {
		if(port == null) {
			port = 22;
		}
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
	
}