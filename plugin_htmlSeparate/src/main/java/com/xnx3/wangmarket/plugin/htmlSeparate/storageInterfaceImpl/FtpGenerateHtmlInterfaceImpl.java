package com.xnx3.wangmarket.plugin.htmlSeparate.storageInterfaceImpl;

import java.io.UnsupportedEncodingException;

import com.jcraft.jsch.SftpException;
import com.xnx3.FileUtil;
import com.xnx3.StringUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.util.FTPUtil;
import com.xnx3.j2ee.util.SFTPUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.cache.generateSite.GenerateHtmlInterface;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.plugin.htmlSeparate.entity.HtmlStorageSftp;

/**
 * FTP GenerateHtmlInterface 接口的实现
 * @author 管雷鸣
 */
public class FtpGenerateHtmlInterfaceImpl implements GenerateHtmlInterface{
	private FTPUtil ftp;
	private HtmlStorageSftp htmlStorageSftp;
	
	public FtpGenerateHtmlInterfaceImpl(HtmlStorageSftp htmlStorageSftp) {
		ftp = new FTPUtil(htmlStorageSftp.getHost(), 21, htmlStorageSftp.getUsername(), htmlStorageSftp.getPassword());
		this.htmlStorageSftp = htmlStorageSftp;
	}
	
	@Override
	public BaseVO putStringFile(String text, String path) {
		boolean b = ftp.upload(htmlStorageSftp.getPath(), text, path);
		if(b) {
			return BaseVO.success();
		}else {
			return BaseVO.failure("upload false");
		}
	}

}
