package com;

import com.xnx3.doc.JavaDoc;

public class ApiDoc {
	public static void main(String[] args) {
		JavaDoc doc = new JavaDoc("com.xnx3");
		
		doc.name = "wm";
		doc.domain = "http://localhost:8080";
		doc.version = "2022.04.23";
		
		doc.generateHtmlDoc();
	}
}
