package com.xnx3.wm.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.vo.BaseVO;

/**
 * 用户相关
 * @author 管雷鸣
 */
@Controller(value="WMUser_Controller")
@RequestMapping("/wm/user/")
public class UserController extends BaseController {
	@Resource
	private UserService userService;
	@Resource
	private SqlService sqlService;
	@Resource
	private SqlCacheService sqlCacheService;
	
	/**
	 * 网站更改密码
	 * @param newPassword 要更改上的新密码
	 */
	@RequestMapping(value="updatePassword.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO updatePassword(HttpServletRequest request,
			@RequestParam(value = "newPassword", required = false, defaultValue="") String newPassword){
		int userid = getUserId(); 
		if(userid < 1){
			return error("尚未登录");
		}
		ActionLogUtil.insertUpdateDatabase(request, "网站更改密码", newPassword);
		return userService.updatePassword(userid, newPassword);
	}
	
}
