package com.xnx3.j2ee.system.responseBody;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

/**
 * responsebody 返回json响应，忽略某些设定的字段
 * @author 管雷鸣
 * @since 5.0
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface ResponseBodyManage {
	
	/**
	 * 要忽略哪些字段，比如忽略 id、password两个字段，就传入 {"id","password"}
	 */
	String[] ignoreField() default {};
	
	/**
	 * json中如果有字段属性为null，自动赋予默认值
	 * <ul>
	 * 	<li>字符串类型为null，会自动变为： ""</li>
	 * 	<li>数字型为null，会自动变为： 0</li>
	 * 	<li>list列表类型的为null，会自动变为空的json列表： []</li>
	 * 	<li>对象类型为null，会自动变为空的json对象： {}</li>
	 * </ul>
	 * @return true:自动赋予默认值；  false:不予理会null值的存在，就原样返回
	 */
	boolean nullSetDefaultValue() default false;
}
