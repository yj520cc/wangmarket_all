package com.xnx3.j2ee.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.impl.SqlServiceImpl;

/**
 * 操作 Spring 的工具类，如获取spring中的bean
 */
@Component
@Order(10000)
public class SpringUtil implements ApplicationContextAware {
	
	/**
	 * 操作 Spring 的工具类
	 */
	public SpringUtil() {
		ConsoleUtil.info("Spring Scan : SpringUtil");
	}
	
	/**
	 * 上下文对象实例
	 */
	private static ApplicationContext applicationContext;
	
	/**
	 * 设置 {@link ApplicationContext}
	 * @param applicationContext spring自动注入的，这里不需要java实现
	 */
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		SpringUtil.applicationContext = applicationContext;
	}

	/**
	 * 获取applicationContext
	 * @return {@link ApplicationContext}
	 */
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * 通过name获取 Bean。
	 * <br/>使用示例：
	 * 	<pre>
	 * 		SqlService sqlService = SpringUtil.getBean("sqlService");
	 * 	</pre>
	 * @param name 要获取的bean名字，如 sqlService
	 * @return 对象，需要强制类型转换
	 */
	public static Object getBean(String name) {
		return getApplicationContext().getBean(name);
	}

	/**
	 * 通过class获取Bean
	 * @param clazz 传入如 {@link SqlServiceImpl}.class
	 * @return 返回如 {@link SqlService}
	 * @deprecated 此方法会有意外导致bean关闭，请使用 {@link #getBean(String)} 
	 */
	public static <T> T getBean(Class<T> clazz) {
		return getApplicationContext().getBean(clazz);
	}

	/**
	 * 通过name,以及Clazz返回指定的Bean
	 * @param name 要获取的bean名字，如 sqlService
	 * @param clazz 传入如 {@link SqlServiceImpl}.class
	 * @return 返回如 {@link SqlService}
	 */
	public static <T> T getBean(String name, Class<T> clazz) {
		return getApplicationContext().getBean(name, clazz);
	}
	

	/**
	 * 获取数据库的操作 {@link SqlService}
	 * @return {@link SqlService}
	 */
	public static SqlService getSqlService(){
//		return getBean(SqlServiceImpl.class);
		return (SqlService)getBean("sqlService");
	}
	
	/**
	 * 获取数据库的操作 {@link SqlCacheService}
	 * @return {@link SqlCacheService}
	 */
	public static SqlCacheService getSqlCacheService(){
		return (SqlCacheService)getBean("sqlCacheService");
	}
	
}
