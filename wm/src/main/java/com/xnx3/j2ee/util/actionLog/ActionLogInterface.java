package com.xnx3.j2ee.util.actionLog;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.xnx3.j2ee.vo.ActionLogListVO;

/**
 * 实现动作日志记录的接口
 * @author 管雷鸣
 *
 */
public interface ActionLogInterface {
	
	/**
	 * 增加一条日志到缓存中，等待提交。这里增加的并不是立即提交。如果想要立即提交，可以设置。。。
	 * @param map 提交的键值对
	 */
	public void add(Map<String, Object> map);
	
	/**
	 * 将缓存中的日志提交
	 * @return true:成功
	 */
	public boolean cacheCommit();
	
	/**
	 * 将日志查询出来，以列表+分页的数据输出
	 * @param query 要查询的单词或者文字
	 * @param everyPageNumber 每页显示多少条，也就是list返回多少条数据
	 * @param request {@link HttpServletRequest} 分页会使用到这个
	 * @return {@link ActionLogListVO}
	 */
	public ActionLogListVO list(String query, int everyPageNumber, HttpServletRequest request);
	
	/**
	 * 同上面的 list,只不过这里可以自定义操作 indexName。
	 * <p>获取到的数据排序规则：这里是按照数据加入的顺序，倒序排列，插入越往后的，显示越靠前</p>
	 * @param indexName 索引名字
	 * @param query 查询条件，传入如： name:guanleiming AND age:123
	 * @param everyPageNumber 每页显示几条，最大100
	 * @param request {@link HttpServletRequest} 分页会使用到这个
	 * @return 如果失败， vo.getResult() == ActionLogListVO.FAILURE
	 */
	public ActionLogListVO list(String indexName, String query,int everyPageNumber, HttpServletRequest request);
	
}
