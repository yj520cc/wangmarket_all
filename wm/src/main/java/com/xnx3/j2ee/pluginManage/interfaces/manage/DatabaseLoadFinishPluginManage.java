package com.xnx3.j2ee.pluginManage.interfaces.manage;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import com.xnx3.ScanClassUtil;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.util.ConsoleUtil;

/**
 * Shiro 权限，哪个url、目录需要登录，哪个不需要登录，在这里修改
 * @author 管雷鸣
 *
 */
@Component(value="PluginManageForDatabaseLoadFinish")
public class DatabaseLoadFinishPluginManage {
	//处理html源代码的插件，这里开启项目时，便将有关此的插件加入此处
	public static List<Class<?>> classList;
	static{
		classList = new ArrayList<Class<?>>();
		
		new Thread(new Runnable() {
			public void run() {
				while(Global.system.size() < 1){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				//当 system数据表 加载后才会执行
				List<Class<?>> allClassList = ScanClassUtil.getClasses("com.xnx3");
				classList = ScanClassUtil.searchByInterfaceName(allClassList, "com.xnx3.j2ee.pluginManage.interfaces.DatabaseLoadFinishInterface");
				for (int i = 0; i < classList.size(); i++) {
					ConsoleUtil.info("装载 DatabaseLoadFinish 插件："+classList.get(i).getName());
				}
				
				new DatabaseLoadFinishThread().start();
			}
		}).start();
		
	}
	
}
class DatabaseLoadFinishThread extends Thread{
	
	@Override
	public void run() {
		
		for (int i = 0; i < DatabaseLoadFinishPluginManage.classList.size(); i++) {
			try {
				Class<?> c = DatabaseLoadFinishPluginManage.classList.get(i);
				Object invoke = null;
				invoke = c.newInstance();
				//运用newInstance()来生成这个新获取方法的实例
				Method m = c.getMethod("databaseLoadFinish",new Class[]{});	//获取要调用的init方法
				//动态构造的Method对象invoke委托动态构造的InvokeTest对象，执行对应形参的add方法
				m.invoke(invoke, new Object[]{});
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| InstantiationException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}	
		}
		
	}
	
}
