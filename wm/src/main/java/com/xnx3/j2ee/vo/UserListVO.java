package com.xnx3.j2ee.vo;

import java.util.List;

import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.system.responseBody.ResponseBodyManage;
import com.xnx3.j2ee.util.Page;

/**
 * 用户列表
 * @author 管雷鸣
 */
@ResponseBodyManage(ignoreField = {"password","salt","version"}, nullSetDefaultValue = true)
public class UserListVO extends BaseVO {
	
	private List<User> list;
	private Page page;
	
	public List<User> getList() {
		return list;
	}
	public void setList(List<User> list) {
		this.list = list;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	
}
