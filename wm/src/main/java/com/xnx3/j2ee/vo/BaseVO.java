package com.xnx3.j2ee.vo;

/**
 * <b>result</b>：执行成功{@link #SUCCESS}/失败{@link #FAILURE}
 * <br/><b>info</b>：执行结果，若成功，此项可忽略，若失败，返回失败原因
 * <br/>(所有JSON返回值VO的父类)
 * @author 管雷鸣
 *
 */
public class BaseVO extends com.xnx3.BaseVO {
	
	/**
	 * 接口需要登录才能获取到数据，但当前用户尚未登录，所以获取不到数据，result返回2
	 */
	public final static int NOT_LOGIN = 2;

	/*
	 * 执行结果
	 * <li>0:失败
	 * <li>1:成功
	 */
	private int result;	//执行结果。<ul><li>0:失败</li><li>1:成功</li><li>2:未登录，需要登录</li></ul>
	
	/*
	 * 执行信息,如执行成功，会返回执行成功的信息，执行失败，会返回为什么会失败的信息
	 */
	private String info; //执行信息。<ul><li>执行成功，会返回执行成功的信息</li><li>执行失败，会返回为什么会失败的信息</li></ul>
	
	public BaseVO(){
		super();
	}
	
	/**
	 * 将 {@link com.xnx3.BaseVO} 转为 {@link com.xnx3.j2ee.vo.BaseVO}
	 * @param baseVO
	 */
	public void setBaseVOForSuper(com.xnx3.BaseVO baseVO){
		setBaseVO(baseVO.getResult(), baseVO.getInfo());
	}
	
	/**
	 * 返回失败得 {@link BaseVO}
	 * @param info 失败得info信息，失败原因
	 * @return 失败得{@link BaseVO}
	 */
	public static BaseVO failure(String info) {
		BaseVO vo = new BaseVO();
		vo.setBaseVO(BaseVO.FAILURE, info);
		return vo;
	}
	
	/**
	 * 返回成功的 {@link BaseVO}
	 * @return 失败得{@link BaseVO}
	 */
	public static BaseVO success() {
		BaseVO vo = new BaseVO();
		vo.setResult(BaseVO.SUCCESS);
		return vo;
	}
	
	/**
	 * 返回成功的 {@link BaseVO}
	 * @return 成功的{@link BaseVO}
	 */
	public static BaseVO success(String info) {
		BaseVO vo = new BaseVO();
		vo.setResult(BaseVO.SUCCESS);
		vo.setInfo(info);
		return vo;
	}

	@Override
	public String toString() {
		return "BaseVO [getResult()=" + getResult() + ", getInfo()="
				+ getInfo() + "]";
	}
	
}
