package com.xnx3.j2ee.vo;

/**
 * 文件上传规则，允许上传哪些后缀、最大上传大小等
 * @author 管雷鸣
 */
public class UploadFileRuleVO extends BaseVO {
	
	private String allowUploadSuffix;	//允许上传的文件后缀列表，多个以|分割，如 png|jpg|rar|zip
	private int maxFileSizeKB;			//最大上传限制，单位：KB，在AttachmentUtil.getMaxFileSizeKB()获取
	
	public String getAllowUploadSuffix() {
		return allowUploadSuffix;
	}
	public void setAllowUploadSuffix(String allowUploadSuffix) {
		this.allowUploadSuffix = allowUploadSuffix;
	}
	public int getMaxFileSizeKB() {
		return maxFileSizeKB;
	}
	public void setMaxFileSizeKB(int maxFileSizeKB) {
		this.maxFileSizeKB = maxFileSizeKB;
	}
	
	@Override
	public String toString() {
		return "UploadFileRuleVO [allowUploadSuffix=" + allowUploadSuffix + ", maxFileSizeKB=" + maxFileSizeKB + "]";
	}
	
}
