<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="资源列表"/>
</jsp:include>
<style>
.iw_table tbody tr td .titleIcon{
	padding-right:10px;
}
</style>
<jsp:include page="../../common/list/formSearch_formStart.jsp" ></jsp:include>
	<jsp:include page="../../common/list/formSearch_input.jsp">
		<jsp:param name="iw_label" value="名字"/>
		<jsp:param name="iw_name" value="name"/>
	</jsp:include>
	
	<jsp:include page="../../common/list/formSearch_input.jsp">
		<jsp:param name="iw_label" value="url"/>
		<jsp:param name="iw_name" value="url"/>
	</jsp:include>
	
	<jsp:include page="../../common/list/formSearch_input.jsp">
		<jsp:param name="iw_label" value="描述"/>
		<jsp:param name="iw_name" value="description"/>
	</jsp:include>
	
	<input class="layui-btn iw_list_search_submit" type="submit" value="搜索" />
	<a href="javascript:permission(0,0);" id="add_first_permission_aTag" class="layui-btn layui-btn-normal" style="float: right; margin-right:10px;"><i class="layui-icon">&#xe654;</i>&nbsp;顶级资源</a>
</form>

<table class="layui-table iw_table">
	<thead>
		<tr>
			<th>菜单名称</th>
			<th>描述</th>
			<th>percode</th>
			<th>url</th>
			<th>作为菜单</th>
			<th>排序</th>
			<th>操作</th>
		</tr> 
	</thead>
	<tbody class="layui-form">
		<c:forEach items="${list}" var="permissionTree">
			<tr style="font-weight: bold; color:#123252; background-color:#fbfbfb;">
				<td style="width:180px;">
					<i class="layui-icon titleIcon">&#xe625;</i>
					<c:if test="${permissionTree.permissionMark.permission.icon != null && permissionTree.permissionMark.permission.icon != '' }">
						<i class="layui-icon titleIcon">${permissionTree.permissionMark.permission.icon }</i>
					</c:if>${permissionTree.permissionMark.permission.name }
				</td>
				<td><x:substring maxLength="20" text="${permissionTree.permissionMark.permission.description }"></x:substring> </td>
				<td>${permissionTree.permissionMark.permission.percode }</td>
				<td class="numeric">${permissionTree.permissionMark.permission.url }</td>
				<td style="width:70px">
					<input id="${permissionTree.permissionMark.permission.id }" type="checkbox" value="1" name="menu" lay-skin="switch" lay-text="开启|关闭" <c:if test="${permissionTree.permissionMark.permission.menu == 1}">checked</c:if>>
				</td>
				<td style="width:80px; cursor: pointer;" onclick="updateRank(${permissionTree.permissionMark.permission.id }, ${permissionTree.permissionMark.permission.rank }, '${permissionTree.permissionMark.permission.name }');">
						${permissionTree.permissionMark.permission.rank }
				</td>
				<td style="width:138px;">
					<botton class="layui-btn layui-btn-sm" onclick="permission(${permissionTree.permissionMark.permission.id },0);" style="margin-left: 3px;"><i class="layui-icon">&#xe642;</i></botton>
					<botton class="layui-btn layui-btn-sm" onclick="deletePermission('${permissionTree.permissionMark.permission.name }', ${permissionTree.permissionMark.permission.id });" style="margin-left: 3px;"><i class="layui-icon">&#xe640;</i></botton>
					<botton class="layui-btn layui-btn-sm" onclick="permission(0,${permissionTree.permissionMark.permission.id });" style="margin-left: 3px;"><i class="layui-icon">&#xe654;</i></botton>
				</td>
			</tr>
			
			<!--二级菜单权限 -->
			<c:forEach items="${permissionTree.list }" var="permissionMark">
				<tr>
					<td style="padding-left:41px;">&nbsp;&nbsp;&nbsp;&nbsp;${permissionMark.permission.name }</td>
					<td>${permissionMark.permission.description }</td>
					<td>${permissionMark.permission.percode }</td>
					<td class="numeric">${permissionMark.permission.url }</td>
					<td>
						<input id="${permissionMark.permission.id}" type="checkbox" value="1" name="menu" lay-skin="switch" lay-text="开启|关闭" <c:if test="${permissionMark.permission.menu == 1}">checked</c:if>>
					</td>
					<td style="width:80px; cursor: pointer;" onclick="updateRank(${permissionMark.permission.id }, ${permissionMark.permission.rank }, '${permissionMark.permission.name }');">
							${permissionMark.permission.rank }
					</td>
					<td>
						<botton class="layui-btn layui-btn-sm" onclick="permission(${permissionMark.permission.id },0);" style="margin-left: 3px;"><i class="layui-icon">&#xe642;</i></botton>
						<botton class="layui-btn layui-btn-sm" onclick="deletePermission('${permissionMark.permission.name }', ${permissionMark.permission.id });" style="margin-left: 3px;"><i class="layui-icon">&#xe640;</i></botton>
					</td>
				</tr>
			</c:forEach>
		</c:forEach>
	</tbody>
</table>
<!-- 通用分页跳转 -->
<jsp:include page="../../common/page.jsp"></jsp:include>

<div style="padding: 20px;color: gray;">
	<div>操作按钮提示:</div>
	<div><i class="layui-icon">&#xe642;</i> &nbsp;：编辑操作，进行修改</div>
	<div><i class="layui-icon">&#xe640;</i> &nbsp;：删除操作，删除某项</div>
	<div><i class="layui-icon">&#xe654;</i> &nbsp;：新增操作，可以在某个顶级资源分类下进行新增其下级的资源</div>
</div>

<script type="text/javascript">
//鼠标跟随提示
$(function(){
	//最右上方的添加顶级资源
	var add_first_permission_aTag_index = 0;
	$("#add_first_permission_aTag").hover(function(){
		add_first_permission_aTag_index = layer.tips('添加顶级资源，而下方的&nbsp;<i class="layui-icon">&#xe654;</i>&nbsp;只是添加某个顶级资源下的子资源 ', '#add_first_permission_aTag', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['310px' , 'auto']
		});
	},function(){
		layer.close(add_first_permission_aTag_index);
	})
});

//根据资源的id删除
function deletePermission(name, permissionId){
	layer.confirm("您确定要删除\""+name+"\"吗?", {icon: 3, title:'提示'}, function(index){
		parent.msg.loading("删除中");	//显示“操作中”的等待提示
		$.post('deletePermission.do?id='+permissionId, function(data){
			parent.msg.close();	//关闭“操作中”的等待提示
			if(data.result == '1'){
				parent.msg.success('删除成功');
				window.location.reload();	//刷新当前页
			 }else if(data.result == '0'){
				 parent.msg.failure(data.info);
			 }else{
				 parent.msg.failure();
			 }
		});
		layer.close(index);
	});
	
}

/**
 * 添加/修改资源
 * id 要修改的资源id， permission.id ，若是<1 则是添加
 * parentId 要添加的资源的父id，修改时此参数无效。若是0则是添加顶级资源
 */
function permission(id, parentId){
	var url = id<1 ? 'addPermission.do?parentId='+parentId : 'editPermission.do?id='+id;
	console.log(url);
	layer.open({
		type: 2, 
		title: id<1 ? '添加资源':'修改资源', 
		area: ['460px', '469px'],
		shadeClose: true, //开启遮罩关闭
		content: url
	});
}

/**
 * 设置当前资源是否是菜单
 * permissionid 要修改的资源id
 * menu 1使用，0不使用
 */
function editPermissionMenu(permissionid, menu){
	msg.loading('修改中');
	wm.post('/admin/role/editPermissionMenu.do', {permissionid:permissionid, menu:menu}, function(data){
		msg.close();
		msg.success('修改成功');
	});
}

layui.use('form', function(){
	var form = layui.form;

	form.on('switch', function(data){
		console.log(data);
		console.log(data.elem.id);
		console.log(data.elem.checked);
		//updateUse(data.elem.checked? '1':'0');	//将改动同步到服务器，进行保存
		editPermissionMenu(data.elem.id, data.elem.checked?1:0);
	});

	//美化是否启用的开关控件
	$(".layui-form-switch").css("marginTop","-2px");
});

/**
 * 更改某项的排序
 * @param id 要修改的 某项的id编号，要改那一项
 * @param rank 当前的排序序号，默认值
 * @param name 资源名字，仅仅只是提示作用
 */
function updateRank(id,rank,name){
	layer.prompt({title: '请输入排序数字，数字越小越靠前', formType: 3, value: ''+rank}, function(text, index){
		parent.msg.loading("保存中");	//显示“操作中”的等待提示
		$.post('/admin/role/savePermissionRank.do?permissionid='+id+'&rank='+text, function(data){
			parent.msg.close();	//关闭“操作中”的等待提示
			if(data.result == '1'){
				//由最外层发起提示框
				parent.msg.success('操作成功');
				//刷新当前页
				window.location.reload();
			}else if(data.result == '0'){
				parent.msg.failure(data.info);
			}else{
				parent.msg.failure('操作失败');
			}
		});

	});
}
</script>
<jsp:include page="../../common/foot.jsp"></jsp:include>