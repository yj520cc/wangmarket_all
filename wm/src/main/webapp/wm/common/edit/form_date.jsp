<%@page import="com.xnx3.StringUtil"%>
<% /* 
	edit.jsp编辑页面中，form表单的input日期选择，点击input，即可选择日期，然后将选择的日期赋予input中
	共有三个参数：
	iw_name：必填，输入框的name，即 <input name="" 这里的name的值
	iw_type：选填，时间选择框的类型，可输入年、年月、年月日等
			
*/ %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%

//<input name="" 这里的name的值
String wm_name = request.getParameter("wm_name");

//request的值
String wm_name_value = request.getParameter("wm_value");
//对其进行防XSS过滤
wm_name_value = StringUtil.filterXss(wm_name_value);
if(wm_name_value == null){
	wm_name_value = "";
}

//时间选择的类型，可以进行年、年月、年月日等选择
String wm_type = request.getParameter("wm_type");
if(wm_type == null || wm_type.equals("")){
	wm_type = "date";
}
%>

<!-- 存储10位时间戳，表单需要提交的时间 -->
<input type="hidden" name="<%=wm_name %>" id="<%=wm_name %>_input" class="layui-input" value="<%=wm_name_value%>" />

<!-- 显示文字阅读的时间 -->
<input type="text" name="<%=wm_name %>_date_show" id="<%=wm_name %>_input_date_show" class="layui-input" value="" />
<script>
//文字阅读时间进行赋值
if(document.getElementById('<%=wm_name %>_input').value != null && document.getElementById('<%=wm_name %>_input').value.length > 0){
	//有值，那么赋予文字阅读时间
	if('<%=wm_type %>' == 'year'){
		document.getElementById('<%=wm_name %>_input_date_show').value = wm.formatTime(document.getElementById('<%=wm_name %>_input').value, 'Y');
	}else if('<%=wm_type %>' == 'month'){
		document.getElementById('<%=wm_name %>_input_date_show').value = wm.formatTime(document.getElementById('<%=wm_name %>_input').value, 'Y-M');
	}else if('<%=wm_type %>' == 'date'){
		document.getElementById('<%=wm_name %>_input_date_show').value = wm.formatTime(document.getElementById('<%=wm_name %>_input').value, 'Y-M-D');
	}else if('<%=wm_type %>' == 'time'){
		document.getElementById('<%=wm_name %>_input_date_show').value = wm.formatTime(document.getElementById('<%=wm_name %>_input').value, 'h:m:s');
	}else if('<%=wm_type %>' == 'datetime'){
		document.getElementById('<%=wm_name %>_input_date_show').value = wm.formatTime(document.getElementById('<%=wm_name %>_input').value, 'Y-M-D h:m:s');
	}
}

layui.use('laydate', function(){
	layui.laydate.render({
		elem: '#<%=wm_name %>_input_date_show' //指定元素
		,type: '<%=wm_type %>'
		,done: function(value, date, endDate){
			//取出当前选择的时间的时间戳
			var shijianchuo = new Date(date.year+'/'+date.month+'/'+date.date+' '+date.hours+':'+date.minutes+':'+date.seconds).getTime()/1000;
			//赋予值
			document.getElementById('<%=wm_name %>_input').value = shijianchuo;
		}
	});
});
</script>