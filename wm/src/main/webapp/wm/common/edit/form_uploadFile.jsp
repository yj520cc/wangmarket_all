<%@page import="com.xnx3.StringUtil"%>
<% /* 
	edit.jsp编辑页面中，form表单的input文件上传，点击input右侧上传按钮，即可将文件上传，然后将上传后文件路径赋予input中
	共有三个参数：
	iw_name：必填，输入框的name，即 <input name="" 这里的name的值
	wm_api_url：必填，上传文件请求的api接口
	wm_value：选填，input输入框默认值
			
*/ %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%

//<input name="" 这里的name的值
String wm_name = request.getParameter("wm_name");

//上传图片的api接口网址，传入如 /admin/common/uploadImage.jspn
String wm_api_url = request.getParameter("wm_api_url");

//request的值
String wm_name_value = request.getParameter("wm_value");
//对其进行防XSS过滤
wm_name_value = StringUtil.filterXss(wm_name_value);
if(wm_name_value == null){
	wm_name_value = "";
}
%>

<input type="text" name="<%=wm_name %>" id="<%=wm_name %>_input" placeholder="点击右侧上传图片" class="layui-input" value="<%=wm_name_value%>" style="padding-right: 60px;">
<button type="button" class="layui-btn" id="<%=wm_name %>_uploadImagesButton" style="float: right;margin-top: -38px;">
	<i class="layui-icon layui-icon-upload"></i>
</button>
<input class="layui-upload-file" type="file" name="file">
<script>
//拉取上传规则，如允许上传后缀、最大上传大小
var wmUploadFileRule = {};
wm.post('/uploadFileRule.json', {}, function(data){
	if(data.result == 0){
		msg.failure(data.info);
		return;
	}
	
	//赋予全局参数
	wmUploadFileRule = data;
	
	layui.use('upload', function(){
		//上传图片,封面图
		layui.upload.render({
			elem: "#<%=wm_name %>_uploadImagesButton" //绑定元素
			,url: '<%=wm_api_url %>?token='+wm.token.get() //上传接口
			,field: 'file'
			,accept: 'file'
			,size: wmUploadFileRule.maxFileSizeKB
			,exts:wmUploadFileRule.allowUploadSuffix	//可上传的文件后缀
			,done: function(res){
				//上传完毕回调
				msg.close();
				if(res.result == 1){
					try{
						document.getElementById("<%=wm_name %>_input").value = res.url;
					}catch(err){
						console.log(err);
					}
					msg.success("上传成功");
				}else{
					msg.failure(res.info);
				}
			}
			,error: function(index, upload){
				//请求异常回调
				msg.close();
				msg.failure('操作异常');
			}
			,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
				msg.loading('上传中');
			}
		});
	});
	
});

</script>