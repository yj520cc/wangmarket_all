<%@page import="com.xnx3.j2ee.Global"%>
<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
//标题
String title = request.getParameter("title");
if(title == null ){
	title = SystemUtil.get("SITE_NAME");
}else{
	title=title+"_"+SystemUtil.get("SITE_NAME");
}

//关键字
String keywords = request.getParameter("keywords");
if(keywords == null ){
	keywords = SystemUtil.get("SITE_KEYWORDS");
}

//描述
String description = request.getParameter("description");
if(description == null ){
	description = SystemUtil.get("SITE_DESCRIPTION");
}
%><!DOCTYPE html>
<html lang="en" style="font-size:16px;">
<head>
<meta charset="utf-8">
<title><%=title %></title>
<meta name="keywords" content="<%=keywords %>" />
<meta name="description" content="<%=description %>" />

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="管雷鸣">
<!-- wm/common/head -->
<!-- layer 、 layui -->
<link rel="stylesheet" href="<%=SystemUtil.get("STATIC_RESOURCE_PATH") %>module/layui/css/layui.css?v=<%=Global.VERSION %>">
<script src="<%=SystemUtil.get("STATIC_RESOURCE_PATH") %>module/layui/layui.js?v=<%=Global.VERSION %>"></script>
<script src="<%=SystemUtil.get("STATIC_RESOURCE_PATH") %>module/msg/msg.js?v=<%=Global.VERSION %>"></script>
<script src="<%=SystemUtil.get("STATIC_RESOURCE_PATH") %>module/request/request.js?v=<%=Global.VERSION %>"></script>
<!-- 商城及接口相关，要废弃 -->
<!-- <script src="//res.zvo.cn/shop/shop.js"></script>-->
<script>
//加载 layer 模块
layui.use('layer', function(){
	layer = layui.layer;
});
</script>

<script src="<%=SystemUtil.get("STATIC_RESOURCE_PATH") %>module/js/jquery-2.1.4.js"></script>

<!-- order by 列表的排序 -->
<script src="<%=SystemUtil.get("STATIC_RESOURCE_PATH") %>module/js/iw.js?v=<%=Global.VERSION %>"></script>

<script src="<%=SystemUtil.get("STATIC_RESOURCE_PATH") %>module/wm/wm.js?v=<%=Global.VERSION %>"></script>

<!-- vue -->
<script src="<%=SystemUtil.get("STATIC_RESOURCE_PATH") %>module/vue/vue-3.1.4.js"></script>
<style>

/*列表页头部form搜索框*/
.toubu_xnx3_search_form{
	padding-top:10px;
	padding-bottom: 10px;
}
/*列表页头部搜索，form里面的搜索按钮*/
.iw_list_search_submit{
	margin-left:22px;
}
/* 列表页，数据列表的table */
.iw_table{
	margin:0px;
}
/* 详情页，table列表详情展示，这里是描述，名字的td */
.iw_table_td_view_name{
	width:150px;
}

/* edit.jsp 编辑页面，输入框下面的灰色小字的说明 */
.layui-form-item .explain{
	font-size: 11px;
	color: gray;
	padding-top: 2px;
	text-align: left;
}
</style>
<script type="text/javascript">
/**
 * 获取网址的get参数。
 * @param name get参数名
 * @returns value
 */
function getUrlParams(name){
	console.log('getUrlParams 已废弃，请使用 wm.getUrlParams(name)');
	return wm.getUrlParams(name);
}

function formatTime(number,format) {
	console.log('formatTime 已废弃，请使用 wm.formatTime(number,format)');
	return wm.formatTime(number,format);
}

function post(api, data, func) {
	console.log('post 已废弃，请使用 wm.post(api, data, func); 或者重写本post方法');
	return wm.post(api, data, func);
}

//如果未登录，那么拦截，弹出提示，并跳转到登录页面
function checkLogin(data){
	if(data.result == '2'){
		//未登录
		msg.info('请先登录', function(){
			window.location.href="/wm/login/login.jsp";
		});
	}
}
</script>
</head>
<body>